//
//  GridDetailViewController.h
//  Picbooster
//
//  Created by George Bafaloukas on 4/4/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GridDetailMenuView.h"
#import "GridDetailActionsMenuView.h"
#import "CustomNavigationBar.h"

@interface GridDetailViewController : UIViewController<UIAlertViewDelegate,GridDetailMenuViewDelegate,CustomNavigationBarDelegate>
{
    NSMutableData *responseData;
}

@property (strong,nonatomic) NSURLConnection* getLikeConnection;
@property (strong,nonatomic) NSURLConnection* setLikeConnection;
@property (nonatomic, strong) NSMutableData *responseData;
@property (strong,nonatomic) UIImageView *imageView;
@property (strong,nonatomic) NSString *imageUrl;
@property (strong,nonatomic) NSString *venueID;
@property (assign) BOOL isBrand;

@property (strong,nonatomic)  GridDetailMenuView* gridDetailMenuView;
@property (strong,nonatomic)  GridDetailActionsMenuView* gridDetailActionsMenuView;
@property (nonatomic, strong) CustomNavigationBar* customNavBar;

-(void)LikePressed;
-(void)AddComment;
-(void)ShowActions;

@end
