//
//  GridFooterView.h
//  Picbooster
//
//  Created by George Bafaloukas on 7/3/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol loadButtonPressed

-(void)loadButtonPressed:(NSString*) button;

@end
@interface GridFooterView : UIView{
    id <loadButtonPressed> delegate;
}
@property (nonatomic,strong) id delegate;
@end