//
//  MenuView.m
//  Picbooster
//
//  Created by Vassilis Dourmas on 04/2/14.
//  Copyright (c) 2013 Vassilis Dourmas. All rights reserved.
//

#import "HomeMenuView.h"

@interface HomeMenuView ()
@property (nonatomic, strong) UIView *mainView;
@end
@implementation HomeMenuView
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame parentView:(UIView*)parentView
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            self.backgroundColor = [UIColor clearColor];
            self.userInteractionEnabled=true;
            self.clipsToBounds = NO;
            CGRect frame1 = self.frame;
            self.frame = CGRectMake(frame1.origin.x, frame1.origin.y, 0, 0);
            
            //UIImageView* buttonView= [[UIImageView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 207.0f)];
            UIImageView* buttonView= [[UIImageView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 126.0f)];
            buttonView.contentMode = UIViewContentModeTopLeft;
            [buttonView setImage :[UIImage imageNamed:@"main screen gradient.png"]];
            
            [self addSubview:buttonView];
            
            buttonView.clipsToBounds = NO;
            CGRect frame2 = buttonView.frame;
            buttonView.frame = CGRectMake(frame2.origin.x, frame2.origin.y, 0, 0);
            
            self.buttonExplore = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            [self.buttonExplore addTarget:self
                                   action:@selector(menuButtonPressed:)
                         forControlEvents:UIControlEventTouchDown];
            self.buttonExplore.tag=1;
            self.buttonExplore.backgroundColor=[UIColor clearColor];
            
            [self.buttonExplore setBackgroundImage:[UIImage imageNamed:@"main screen start button.png"] forState:UIControlStateNormal];
            //[self.buttonExplore setBackgroundImage:[UIImage imageNamed:@"main screen start buttonPRESSED.png"] forState:UIControlStateSelected];
            //self.buttonExplore.frame = CGRectMake((frame1.origin.x+ frame1.size.width/2 -  63/2),
            //                                       frame1.origin.y + frame1.size.height  - 63.0f,
            //                                                63.0f, 63.0f);
            
            self.buttonExplore.frame = CGRectMake((frame1.origin.x+ (frame1.size.width/2 -  63/2)),
                                                  frame1.origin.y + frame1.size.height  - 63.0f,
                                                  63.0f, 63.0f);
            self.buttonExplore.contentMode = UIViewContentModeCenter;
            
            [parentView addSubview:self.buttonExplore];
        }else{
            self.backgroundColor = [UIColor clearColor];
            self.userInteractionEnabled=true;
            self.clipsToBounds = NO;
            CGRect frame1 = self.frame;
            self.frame = CGRectMake(frame1.origin.x, frame1.origin.y, 0, 0);
            
            //UIImageView* buttonView= [[UIImageView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 207.0f)];
            UIImageView* buttonView= [[UIImageView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 126.0f)];
            buttonView.contentMode = UIViewContentModeTopLeft;
            
            [buttonView setImage :[UIImage imageNamed:@"main screen gradient.png"]];
            [self addSubview:buttonView];
            
            buttonView.clipsToBounds = NO;
            CGRect frame2 = buttonView.frame;
            buttonView.frame = CGRectMake(frame2.origin.x, frame2.origin.y, 0, 0);
            
            self.buttonExplore = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.buttonExplore addTarget:self
                        action:@selector(menuButtonPressed:)
              forControlEvents:UIControlEventTouchDown];
            self.buttonExplore.tag=1;
            self.buttonExplore.backgroundColor=[UIColor clearColor];

            [self.buttonExplore setBackgroundImage:[UIImage imageNamed:@"main screen start button.png"] forState:UIControlStateNormal];
            //[self.buttonExplore setBackgroundImage:[UIImage imageNamed:@"main screen start buttonPRESSED.png"] forState:UIControlStateSelected];
            //self.buttonExplore.frame = CGRectMake((frame1.origin.x+ frame1.size.width/2 -  63/2),
            //                                       frame1.origin.y + frame1.size.height  - 63.0f,
             //                                                63.0f, 63.0f);
            
            self.buttonExplore.frame = CGRectMake((frame1.origin.x+ (frame1.size.width/2 -  63/2)),
                                                   frame1.origin.y + frame1.size.height  - 63.0f,
                                                            63.0f, 63.0f);
            self.buttonExplore.contentMode = UIViewContentModeCenter;
            
            [parentView addSubview:self.buttonExplore];
            
        }
    }
    return self;
}
-(void)menuButtonPressed:(id)sender{
    UIButton* buttonPressed = sender;
    if(buttonPressed.tag==1){
        NSLog(@"Push Explore view controller");
        [delegate menuButtonPressed:@"Explore"];
    }
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//	NSLog(@"touchesBegan");
//    //[delegate touchesBegan:touches withEvent:event];
//    
//    UITouch *aTouch = [touches anyObject];
//    CGPoint point = [aTouch locationInView:self];
//    
//    NSLog(@"touchesBegan at :%f",point.y);
//}
//
//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
//{
//	NSLog(@"touchesMoved");
//    UITouch *aTouch = [touches anyObject];
//    CGPoint point = [aTouch locationInView:self];
//    
//    NSLog(@"touchesMoved at :%f",point.y);
//}
//
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
//{
//	NSLog(@"touchesEnded");
//    
//    UITouch *aTouch = [touches anyObject];
//    CGPoint point = [aTouch locationInView:self];
//    
//    NSLog(@"touchesEnded at :%f",point.y);
//}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
