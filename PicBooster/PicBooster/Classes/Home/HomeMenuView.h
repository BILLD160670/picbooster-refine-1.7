//
//  MenuView.h
//  Picbooster
//
//  Created by Vassilis Dourmas on 04/2/14.
//  Copyright (c) 2013 Vassilis Dourmas. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol HomeMenuViewDelegate

-(void)menuButtonPressed:(NSString*) button;
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;

@end


@interface HomeMenuView : UIView{
    id <HomeMenuViewDelegate> delegate;
 
}
@property (nonatomic,strong) id delegate;
@property (nonatomic,strong) UIButton* buttonExplore;

- (id)initWithFrame:(CGRect)frame parentView:(UIView*)parentView;
@end