//
//  MenuView.h
//  Picbooster
//
//  Created by Vassilis Dourmas on 04/2/14.
//  Copyright (c) 2013 Vassilis Dourmas. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol GridDetailMenuViewDelegate

-(void)menuButtonPressed:(NSString*) button;

@end
@interface GridDetailMenuView : UIView{
    id <GridDetailMenuViewDelegate> delegate;
}
@property (strong,nonatomic) NSURLConnection* getLikeConnection;
@property (nonatomic,strong) id delegate;
@property (nonatomic,strong) UILabel* labelLikes;
@property (nonatomic,assign) BOOL like;
@property (nonatomic,assign) UIButton *buttonLike;

-(void)setLikes:(NSString*)likes;
-(NSString*)getLikes;

-(void)setComments:(NSString*)comments;

-(void)setLikePhotoF:(BOOL)like;

@end