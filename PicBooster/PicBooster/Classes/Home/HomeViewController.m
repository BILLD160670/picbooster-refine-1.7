//
//  HomeViewController.m
//  Picbooster
//
//  Created by George Bafaloukas on 4/3/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "HomeViewController.h"
#import "GridDetailViewController.h"
#import "ExploreViewController.h"
#import "SettingsView.h"
#import "AppDelegate.h"
#import "Consts.h"
#import "UIViewController+MJPopupViewController.h"
#import "PopUpViewController.h"
#import "HomeMenuView.h"
#import "CustomNavigationBar.h"
#import "AwardUploadViewController.h"
#import "CommentBoxView.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize gridView,services;
@synthesize responseData;
@synthesize hud;
@synthesize GridCount;
@synthesize pointsConnection;
@synthesize gridConnection;
@synthesize pointsLabel;
@synthesize pointsImageView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}
- (void)populateUserDetailsWithLogin
{
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection,
           NSDictionary<FBGraphUser> *user,
           NSError *error) {
             if (!error) {
                 [self refreshPoints:user.id];
                 [self populateUserDetails];
             }
         }];
    }else{
        [FBSession openActiveSessionWithReadPermissions:nil
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session,
           FBSessionState state, NSError *error) {
             [self populateUserDetails];
         }];
    }
}
- (void)populateUserDetails
{
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection,
           NSDictionary<FBGraphUser> *user,
           NSError *error) {
             if (!error) {
                 NSLog(@"user.id: %@ ",user.id);
                 [AppDelegate getAppDelegate].fbUserId =user.id;
                 [self refreshPoints:user.id];
             }
         }];
    }
}
-(void) refreshPoints:(NSString *)fbUserId{
    NSString *urlString= [NSString stringWithFormat:getUserPoints,fbUserId];
    NSMutableURLRequest *pointsRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
    [pointsRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [pointsRequest setTimeoutInterval:20.0];
    self.pointsConnection=[[NSURLConnection alloc] initWithRequest:pointsRequest delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

}


-(HomeMenuView*)CreateNavigationBarAndHomeMenu:(id<CustomNavigationBarDelegate>)delegate
{
    HomeMenuView *myMenuViewIos = nil;
    myMenuViewIos.delegate=self;

    NSString *deviceType = [UIDevice currentDevice].model;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            
            myMenuViewIos = [[HomeMenuView alloc] initWithFrame:CGRectMake(0.0f, self.view.frame.size.height - 126.0f, 768.0f, 126.0f) parentView:self.view];
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 45.0f) displayPoints:true displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
        else
        {
            myMenuViewIos = [[HomeMenuView alloc] initWithFrame:CGRectMake(0.0f, self.view.frame.size.height - 126.0f, 320.0f, 126.0f) parentView:self.view];
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 45) displayPoints:true displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
        
    }
    else
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            myMenuViewIos = [[HomeMenuView alloc] initWithFrame:CGRectMake(0.0f, self.view.frame.size.height - 126.0f, 768.0f, 126.0f) parentView:self.view];
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 65.0f) displayPoints:true displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
        else
        {
            myMenuViewIos = [[HomeMenuView alloc] initWithFrame:CGRectMake(0.0f, self.view.frame.size.height - 126.0f, 320.0f, 126.0f) parentView:self.view];
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 65) displayPoints:true displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
    }
    self.customNavBar.delegate = delegate;
    [self.view addSubview:self.customNavBar];
    myMenuViewIos.delegate = delegate;
    
    return myMenuViewIos;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden= YES;
    
    HomeMenuView *myMenuViewIos = [self CreateNavigationBarAndHomeMenu:self];
    
    self.responseData = [[NSMutableData alloc] init];
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
     GridFooterView *footerView = [[GridFooterView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 40.0f)];
    footerView.delegate=self;
    GridCount=15;
    
    
     NSString *deviceType = [UIDevice currentDevice].model;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        //[[UINavigationBar appearance] setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
       // [[UINavigationBar appearance] setBackgroundColor:[UIColor colorWithRed:236/255.0 green:232/255.0 blue:223/255.0 alpha:1.0]];
       
        if (self.view.frame.size.height>480) {
            self.gridView = [[AQGridView alloc] initWithFrame:CGRectMake(0.0f, 45.0f, 320.0f, 568.0f -45.0f)];
        }else{
            self.gridView = [[AQGridView alloc] initWithFrame:CGRectMake(0.0f, 45.0f, 320.0f, 480.0f-45.0f)];
        }
        
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            //iPad
            self.gridView.frame = CGRectMake(64.0f, 45.0f, 640.0f, 1024.0f - 45.0f);
            //myMenuViewIos.frame=CGRectMake(0.0f, self.view.frame.size.height - 207.0f, 768.0f, 207.0f);
        }
        
        self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        self.gridView.autoresizesSubviews = YES;
        self.gridView.delegate = self;
        self.gridView.dataSource = self;
        [self.view addSubview:gridView];
        [self.view addSubview:myMenuViewIos];
        [self.view bringSubviewToFront:myMenuViewIos.buttonExplore];

    }else{
        //self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:236/255.0 green:232/255.0 blue:223/255.0 alpha:1.0];
        
        int h = self.view.frame.size.height;
        if (self.view.frame.size.height>480) {
            self.gridView = [[AQGridView alloc] initWithFrame:CGRectMake(0.0f, 65.0f, 320.0f, 568.0f -65.0f)];
        }else{
            self.gridView = [[AQGridView alloc] initWithFrame:CGRectMake(0.0f, 65.0f, 320.0f, 480.0f-65.0f)];
        }
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            //iPad
            self.gridView.frame = CGRectMake(64.0f, 65.0f, 640.0f, 1024.0f - 65.0f);
           // myMenuViewIos.frame=CGRectMake(0.0f, self.view.frame.size.height - 207.0f, 768.0f, 207.0f);
        }
      
       
        self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        self.gridView.autoresizesSubviews = YES;
        self.gridView.delegate = self;
        self.gridView.dataSource = self;
        [self.view addSubview:gridView];
        [self.view addSubview:myMenuViewIos];
        [self.view bringSubviewToFront:myMenuViewIos.buttonExplore];
    }
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"settingsBG.png"]]];
    [self populateUserDetailsWithLogin];
    
    [self loadData];
    
}


-(void)loadData
{
    //grid request
    NSMutableURLRequest *gridRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:getGrid]] ;
    [gridRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [gridRequest setTimeoutInterval:20.0];
    self.gridConnection=[[NSURLConnection alloc] initWithRequest:gridRequest delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

}

-(void)viewWillAppear:(BOOL)animated
{
    SettingsView *mySettingsView = [AppDelegate getAppDelegate].settingsView;
    if(mySettingsView.settingsIsVisible)
        [mySettingsView hideSettings];

    [self.customNavBar refreshPoints];
    
    if([AppDelegate getAppDelegate].refreshHome)
    {
        [self loadData];
        [AppDelegate getAppDelegate].refreshHome = NO;
    }
}



- (CGSize) portraitGridCellSizeForGridView: (AQGridView *) aGridView
{
    return ( CGSizeMake(160.0, 160) );
}

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    return GridCount;
}

- (AQGridViewCell *) gridView: (AQGridView *) aGridView cellForItemAtIndex: (NSUInteger) index
{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    static NSString * PlainCellIdentifier = @"PlainCellIdentifier";
    NSDictionary *gridCell = [self.services objectAtIndex:index];
    GridViewCell * cell = (GridViewCell *)[aGridView dequeueReusableCellWithIdentifier:@"PlainCellIdentifier"];
    if ( cell == nil )
    {
        cell = [[GridViewCell alloc] initWithFrame: CGRectMake(0.0, 0.0, 160, 160)
                                   reuseIdentifier: PlainCellIdentifier];
    }
    cell.backgroundColor=[UIColor clearColor];
    cell.tag = index;
    //show placeholder image (or throbber) to handle slow loading of new images and replication effect
    [cell.imageViewRotator setImage:[UIImage imageNamed:@"rotator.png"]];
    cell.imageView.hidden = YES;
    cell.imageViewRotator.hidden = NO;
    /*
     "lazy load" images using GCD
     store each image in disk for offline load or next launch of the app
     store images in an NSCache object for fast loading
     */
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        NSString *urlString=[NSString stringWithFormat:APIUrl,[gridCell valueForKey:@"hash"]];
        //use of caching delegate method
        UIImage *cellImage=[delegate returnCahedImageWithName:urlString];
        if (cellImage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cell.tag == index) {
                    cell.imageView.image = cellImage;
                    cell.imageView.hidden = NO;
                    cell.imageViewRotator.hidden = YES;
                    [cell setNeedsLayout];
                }
            });
        }
    });
    if (index == GridCount-1) {
        [self performSelector:@selector(loadMore) withObject:nil afterDelay:2];
    }
    
    return cell;
}

-(void)gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)index
{
    
//    AwardUploadViewController* aw = [[AwardUploadViewController alloc]init];
//    [self.navigationController pushViewController:aw animated:NO];
//    return;
    
    
//    CommentBoxView *commentView=[[CommentBoxView alloc] init];
//    commentView.delegate=self;
//    //commentView.finalImage = photo.image;
//    //commentView.passwordTextField.text = self.comment;
//    [self presentPopupViewControllerNoPop:commentView animationType:MJPopupViewAnimationFade];

    
    if([[AppDelegate getFbUserId] isEqualToString:@""])
    {
        return;
    }
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSDictionary *gridCell = [services objectAtIndex:index];
    GridDetailViewController *detail=[[GridDetailViewController alloc] init];
    NSString *urlString=[NSString stringWithFormat:APIUrl,[gridCell valueForKey:@"hash"]];
    NSString *venueID=[gridCell valueForKey:@"venue_id"];
    UIImage *detailImage=[delegate returnCahedImageWithName:urlString];
    detail.imageUrl=[gridCell valueForKey:@"hash"];
    detail.venueID=venueID;
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        detail.imageView = [[UIImageView alloc] initWithImage:[detailImage scaleToSize:CGSizeMake(600.0f, 600.0f)]];
        detail.imageView.frame = CGRectMake(10.0, 30.0, 600.0f, 600.0f);
    }else{
        detail.imageView = [[UIImageView alloc] initWithImage:[detailImage scaleToSize:CGSizeMake(300.0f, 300.0f)]];
        detail.imageView.frame = CGRectMake(10.0, 30.0, 300.0f, 300.0f);
    }
    [self.navigationController pushViewController:detail animated:NO];
}


//HomeMenuViewDelegate
-(void)menuButtonPressed:(NSString *)button{
    if([button isEqualToString:@"Explore"]){
        [self exploreClicked];
    }
}

//CustomNavigationBarDelegate
-(void)barButtonPressed:(NSString *)button{
    if([button isEqualToString:@"Settings"]){
        [self settingsClicked];
    }
}


-(void)exploreClicked{
    if([[AppDelegate getFbUserId] isEqualToString:@""])
    {
        return;
    }
    ExploreViewController *explore=[[ExploreViewController alloc] init];
    [self.navigationController pushViewController:explore animated:YES];
}

-(void)settingsClicked{
    
    SettingsView *mySettingsView = [AppDelegate getAppDelegate].settingsView;
    if(mySettingsView.settingsIsVisible)
    {
        [mySettingsView hideSettings];
    }
    else{
    
       NSString *deviceType = [UIDevice currentDevice].model;
       CGFloat finalHeight  = 330;
       if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
          finalHeight = 330;
       }
       [self.view addSubview:mySettingsView];
       [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         mySettingsView.frame = CGRectMake(0.0f , mySettingsView.frame.origin.y , mySettingsView.frame.size.width , finalHeight);
                         mySettingsView.settingsIsVisible = true;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
     }
}


-(void)loadMore{
    if (self.services.count>GridCount+15) {
        GridCount = GridCount+15;
    }else {
        GridCount = self.services.count;
    }
    [self.gridView reloadData];
}

//NSURLConnection methods following
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self.hud hide:YES];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Connection Error!"
												   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
	[alert show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *myResponseString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    SBJsonParser *json = [[SBJsonParser alloc] init] ;
    if ([connection isEqual:self.gridConnection]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        self.services = [json objectWithString:myResponseString];
        [self.hud hide:YES];
        [self.gridView reloadData];
        self.gridConnection=nil;
    }else if ([connection isEqual:self.pointsConnection]){
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSDictionary *points =[json objectWithString:myResponseString];
        [AppDelegate getAppDelegate].currentUserPointsString =[points valueForKey:@"points"];
        [self.customNavBar refreshPoints];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [[delegate imageCache] removeAllObjects];
}

@end
