//
//  HomeViewController.h
//  Picbooster
//
//  Created by George Bafaloukas on 4/3/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQGridView.h"
#import "GridViewCell.h"
#import "SBJson.h"
#import "MBProgressHUD.h"
#import "UIImage+Scale.h"
#import "GridFooterView.h"
#import "CustomNavigationbar.h"

@interface HomeViewController : UIViewController <AQGridViewDelegate, AQGridViewDataSource,MBProgressHUDDelegate,loadButtonPressed,CustomNavigationBarDelegate,UITabBarControllerDelegate>{
    NSURLConnection* gridConnection;
    NSURLConnection* pointsConnection;
    NSMutableData *responseData;
    MBProgressHUD *hud;
    UILabel *pointsLabel;
    UIImageView  *pointsImage;
}
@property (strong,nonatomic) MBProgressHUD *hud;
@property (strong,nonatomic) NSURLConnection* gridConnection;
@property (strong,nonatomic) NSURLConnection* pointsConnection;
@property (nonatomic, strong) AQGridView * gridView;
@property (nonatomic, strong) NSArray* services;
@property (nonatomic, strong) NSMutableData *responseData;
@property int GridCount;
@property (nonatomic, strong) UILabel *pointsLabel;
@property (nonatomic, strong) UIImageView  *pointsImageView;

@property (nonatomic, strong) CustomNavigationBar* customNavBar;


-(void)loadMore;
-(void)refreshClicked;
-(void)exploreClicked;
@end
