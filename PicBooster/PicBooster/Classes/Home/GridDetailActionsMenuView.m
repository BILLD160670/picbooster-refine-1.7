//
//  MenuView.m
//  Picbooster
//
//  Created by Vassilis Dourmas on 04/2/14.
//  Copyright (c) 2013 Vassilis Dourmas. All rights reserved.
//

#import "GridDetailActionsMenuView.h"

@interface GridDetailActionsMenuView ()
@property (nonatomic, strong) UIView *mainView;
@end
@implementation GridDetailActionsMenuView
@synthesize mainView;
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        int scale =1;
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            scale = 2;
        }
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width , self.frame.size.height)];
            mainView.backgroundColor = [UIColor clearColor];
            
            float viewHeight = self.frame.size.height;
            float viewWidth = self.frame.size.width;
            float buttonPadding = 1 * scale;
            float buttonHeight = (viewHeight - (buttonPadding*3)) / 3;
            float buttonWidth = 230 * scale;
            float buttonOriginX = (viewWidth -buttonWidth)/2;
            
            
            UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
            [button1 addTarget:self
                        action:@selector(menuButtonPressed:)
              forControlEvents:UIControlEventTouchDown];
            button1.tag=1;
            [button1 setBackgroundImage:[UIImage imageNamed:@"go to venue.png"] forState:UIControlStateNormal];
            //button1.backgroundColor=[UIColor lightGrayColor];
            //button1.titleLabel.textColor=[UIColor blueColor];
            //[button1 setTitle:@"goto venue " forState:UIControlStateNormal];
            //[button1 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
            //[button1.titleLabel setFont:[UIFont fontWithName:@"Opificio" size:14.0f]];
            
            button1.frame = CGRectMake(buttonOriginX, 0.0f, buttonWidth, buttonHeight);
            
            
            UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
            [button2 addTarget:self
                        action:@selector(menuButtonPressed:)
              forControlEvents:UIControlEventTouchDown];
            button2.tag=2;
            [button2 setBackgroundImage:[UIImage imageNamed:@"report photo.png"] forState:UIControlStateNormal];
//            button2.backgroundColor=[UIColor lightGrayColor];
//            button2.titleLabel.textColor=[UIColor blueColor];
//            [button2 setTitle:@"report photo" forState:UIControlStateNormal];
//            [button2 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//            [button2.titleLabel setFont:[UIFont fontWithName:@"Opificio" size:14.0f]];
            button2.frame = CGRectMake(buttonOriginX, button1.frame.origin.y + buttonHeight + buttonPadding, buttonWidth, buttonHeight);
            
            //462x59
            UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
            [button3 addTarget:self
                        action:@selector(menuButtonPressed:)
              forControlEvents:UIControlEventTouchDown];
            button3.tag=3;
            [button3 setBackgroundImage:[UIImage imageNamed:@"cancel button.png"] forState:UIControlStateNormal];
//            button3.backgroundColor=[UIColor lightGrayColor];
//            button3.titleLabel.textColor=[UIColor blueColor];
//            [button3 setTitle:@"cancel" forState:UIControlStateNormal];
//            [button3 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
//            [button3.titleLabel setFont:[UIFont fontWithName:@"Opificio-Bold" size:14.0f]];
            button3.frame = CGRectMake(buttonOriginX, button2.frame.origin.y + buttonHeight + buttonPadding, buttonWidth, buttonHeight);
            
            [mainView addSubview:button1];
            [mainView addSubview:button2];
            [mainView addSubview:button3];
        [self addSubview:mainView];
    }
    return self;
}

-(void)menuButtonPressed:(id)sender{
    UIButton* buttonPressed = sender;
    if(buttonPressed.tag==1){
        [delegate actionsMenuButtonPressed:@"GoToVenue"];
    }else if (buttonPressed.tag==2){
        [delegate actionsMenuButtonPressed:@"ReportPhoto"];
    }else if (buttonPressed.tag==3){
       [delegate actionsMenuButtonPressed:@"CancelActions"];
    }
}




/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
