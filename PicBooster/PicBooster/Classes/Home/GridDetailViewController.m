//
//  GridDetailViewController.m
//  Picbooster
//
//  Created by George Bafaloukas on 4/4/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//
//#import <QuartzCore/CATransaction.h>
#import "GridDetailViewController.h"
#import "Consts.h"
#import "VenueBrandViewController.h"
#import "AppDelegate.h"
#import "GridDetailMenuView.h"
#import "SettingsView.h"
#import "AwardUploadViewController.h"

@interface GridDetailViewController ()

@end

@implementation GridDetailViewController
@synthesize imageView;
@synthesize imageUrl;
@synthesize venueID;
@synthesize isBrand;
@synthesize gridDetailMenuView;
@synthesize gridDetailActionsMenuView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        responseData = [[NSMutableData alloc]init];
    }
    return self;
}


-(void)createCustomNavigationBar:(id<CustomNavigationBarDelegate>)delegate
{
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 45.0f) displayPoints:false displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 45) displayPoints:false displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
    }
    else
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 65.0f) displayPoints:false displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 65) displayPoints:false displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
    }
    
    self.customNavBar.delegate = self;
    [self.view addSubview:self.customNavBar];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBar.tintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"menuTileNew.png"]];
    NSLog(@"Photo has venue_id %@",self.venueID);
    self.navigationController.navigationBarHidden=YES;
    
    [self createCustomNavigationBar:self];
    //#17181a
    self.view.backgroundColor = [UIColor colorWithRed:23.0f/257.0f green:24.0f/257.0f blue:26.0f/257.0f alpha:1.0f];
    
    float    viewOrigin = self.view.frame.origin.y;
    float    viewHeight = self.view.frame.size.height;
    float    viewWidth = self.view.frame.size.width;
    CGSize   imageViewSize      = CGSizeMake(320,  320);
    float    menuHeight  = 50;
    float    menuWidth  = viewWidth;
    float    actionsMenuHeight  = 93;
    float    actionsMenuWidth  = viewWidth;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        imageViewSize      = CGSizeMake(768.0f,  768.0f);
        actionsMenuHeight  = 186;
        menuHeight = 100;
        //self.imageView.frame = CGRectMake(85.0, 40.0, 590.0f, 508.0f);
        //self.gridDetailMenuView = [[GridDetailMenuView alloc]initWithFrame:CGRectMake(0,550, 768, 100)];
    }else{
        if(viewHeight <= 480)
        {
            imageViewSize      = CGSizeMake(320,  320);
            //actionsMenuHeight  = 138;
        }
    }
    CGPoint  imageViewOrigin    = CGPointMake((viewWidth - imageViewSize.width)/2, 65);
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        imageViewOrigin    = CGPointMake((viewWidth - imageViewSize.width)/2, 45);
    }
    
    CGPoint  gridDetailMenuOrigin = CGPointMake(0,imageViewOrigin.y +  imageViewSize.height+10);
    CGPoint  gridDetailActionsMenuOrigin = CGPointMake(0,viewHeight - actionsMenuHeight);

    self.imageView.frame = CGRectMake(imageViewOrigin.x, imageViewOrigin.y, imageViewSize.width, imageViewSize.height);
    self.gridDetailMenuView = [[GridDetailMenuView alloc]initWithFrame:CGRectMake(gridDetailMenuOrigin.x,
                                                                                  gridDetailMenuOrigin.y,
                                                                                  viewWidth,menuHeight)];
    self.gridDetailActionsMenuView = [[GridDetailActionsMenuView alloc] initWithFrame:CGRectMake(gridDetailActionsMenuOrigin.x,
                                                                                                 gridDetailActionsMenuOrigin.y,
                                                                                                 actionsMenuWidth,actionsMenuHeight)];
    self.gridDetailActionsMenuView.hidden = true;

    
    self.gridDetailMenuView.delegate = self;
    self.gridDetailActionsMenuView.delegate = self;
    
    [self.view addSubview:self.imageView];
    [self.view addSubview:self.gridDetailMenuView];
    [self.view addSubview:self.gridDetailActionsMenuView];
    
    [self getLikes];
}


-(void)viewDidAppear:(BOOL)animated
{
//    AwardUploadViewController* aViewController = [[AwardUploadViewController alloc]init:self];
//    // [self.navigationController presentPopupViewController:aViewController animationType:MJPopupViewAnimationFade];
//    [self presentPopupViewControllerNoPop:aViewController animationType:MJPopupViewAnimationFade];
}


-(void)getLikes
{
    //getGridDetailRequest getLike
    self.responseData = [[NSMutableData alloc] init];
    NSString* getGridDetailString = [NSString stringWithFormat:getLikePhoto,[AppDelegate getFbUserId],self.imageUrl];
    NSMutableURLRequest *gridDetailRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:getGridDetailString]] ;
    [gridDetailRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [gridDetailRequest setTimeoutInterval:100.0];
    
    self.getLikeConnection=[[NSURLConnection alloc] initWithRequest:gridDetailRequest delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    SettingsView *mySettingsView = [AppDelegate getAppDelegate].settingsView;
    if(mySettingsView.settingsIsVisible)
        [mySettingsView hideSettings];
    
    [self.customNavBar refreshPoints];
}


//NSURLConnection methods following
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    if(connection==self.getLikeConnection || connection==self.setLikeConnection)
	   [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	if(connection==self.getLikeConnection || connection==self.setLikeConnection)
        [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Connection Error!"
												   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
	[alert show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *myResponseString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
   SBJsonParser *json = [[SBJsonParser alloc] init] ;
   [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
   if(connection==self.getLikeConnection)
   {
       NSArray *likes =[json objectWithString:myResponseString];
       NSNumber* iLike = [likes objectAtIndex:0];
       NSNumber* Likes = [likes objectAtIndex:1];
       
       [gridDetailMenuView setLikePhotoF:[iLike intValue]==1?YES:NO];
       [gridDetailMenuView setLikes:[NSString stringWithFormat:@"%d",[Likes intValue]]];
   }
    else if(connection==self.setLikeConnection)
    {
        NSString *resp =myResponseString;
        if([resp isEqualToString:@"OK"])
        {
            int likes = [[gridDetailMenuView getLikes] intValue];
            [gridDetailMenuView setLikes:[NSString stringWithFormat:@"%d",likes+1]];
            [gridDetailMenuView setLikePhotoF :YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Error sending like request!"
                                                           delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
        [alert show];
        }
    }
}



//customNavigationBar actions
-(void)barButtonPressed:(NSString *)button{
    if([button isEqualToString:@"Back"]){
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if([button isEqualToString:@"Settings"]){
        [self settingsClicked];
    }
}

-(void)settingsClicked{
   
    SettingsView *mySettingsView = [AppDelegate getAppDelegate].settingsView;
    if(mySettingsView.settingsIsVisible)
    {
        [mySettingsView hideSettings];
        
    }
    else{
       [self hideActions];
    
       NSString *deviceType = [UIDevice currentDevice].model;
       CGFloat finalHeight  = 330;
       if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
           finalHeight = 330;
       }
       [self.view addSubview:mySettingsView];
    
       [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationCurveLinear
                     animations:^{
                         mySettingsView.frame = CGRectMake(mySettingsView.frame.origin.x , mySettingsView.frame.origin.y , mySettingsView.frame.size.width , finalHeight);
                         
                         mySettingsView.settingsIsVisible = true;
                         
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
      }
    
}



//menuBar actions
-(void)menuButtonPressed:(NSString *)button{
    if ([button isEqualToString:@"Like"]) {
        [self likePressed];
    }else if([button isEqualToString:@"Actions"]){
        [self performSelector:@selector(showActions) withObject:nil ];
    }
}

-(void)likePressed
{
    if(gridDetailMenuView.like)
    {
        return;
    }
    NSString* setLike = [NSString stringWithFormat:setLikePhoto,[AppDelegate getFbUserId],self.imageUrl];
   
    NSMutableURLRequest *gridDetailRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:setLike]] ;
    [gridDetailRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [gridDetailRequest setTimeoutInterval:100.0];
    
    self.setLikeConnection = [[NSURLConnection alloc] initWithRequest:gridDetailRequest delegate:self];
}

-(void)showActions
{

   // CGRect rectInv = CGRectMake(self.gridDetailMenuView.frame.origin.x,
   //                             self.gridDetailMenuView.frame.origin.y,
    ///                            100, 30);
    
    SettingsView *mySettingsView = [AppDelegate getAppDelegate].settingsView;
    if(mySettingsView.settingsIsVisible)
        [mySettingsView hideSettings];
    
    self.gridDetailMenuView.hidden = true;
    self.gridDetailActionsMenuView.hidden = false;

    return;
//    [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveLinear
//                     animations:^{
//                         CGRect rectInv = CGRectMake(self.gridDetailMenuView.frame.origin.x,
//                                                     self.gridDetailMenuView.frame.origin.y,
//                                                     10, 10);
//                         self.gridDetailMenuView.frame = rectInv;
//                     }
//                     completion:^(BOOL finished){
//                         
//                     }];

}


-(void)hideActions
{
    self.gridDetailMenuView.hidden = false;
    self.gridDetailActionsMenuView.hidden = true;
}


//actionMenuBar actions
-(void)actionsMenuButtonPressed:(NSString *)button{
    if ([button isEqualToString:@"CancelActions"]) {
        [self hideActions];
    }
    else if ([button isEqualToString:@"GoToVenue"]) {
        [self goToVenue];
    }
    else if ([button isEqualToString:@"ReportPhoto"]) {
        [self reportPhoto];
    }
}


-(void)goToVenue{
    
   VenueBrandViewController *detail=[[VenueBrandViewController alloc] init];
   detail.storeId=self.venueID;
   detail.isBrand = self.isBrand;
        [self.navigationController pushViewController:detail animated:YES];
    
}

-(void)reportPhoto{
    NSString *imgURL = [NSString stringWithFormat:@"%@",self.imageUrl];
    
    NSMutableURLRequest *fbRequest = [[NSMutableURLRequest alloc] init];
    NSString *fbURL =[NSString stringWithFormat:report,imgURL,@"Objective_Content"];
    [fbRequest setURL:[NSURL URLWithString:fbURL]];
    NSLog(@"FB Responce: %@ ",fbURL);
    // now lets make the connection to the web
    NSData *returnDataFB = [NSURLConnection sendSynchronousRequest:fbRequest returningResponse:nil error:nil];
    NSString *returnStringFB =[[NSString alloc] initWithData:returnDataFB encoding:NSUTF8StringEncoding];
    
    NSLog(@"FB Responce: %@ ",returnStringFB);
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Report Photo"
                              message:@"Are you sure you want to report this photo?"
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"OK", nil];
    alertView.tag=1;
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1) {
        if (buttonIndex == 0){
            //cancel clicked ...do your action
        }else if (buttonIndex == 1){
            //reset clicked
            NSString *imgURL = [NSString stringWithFormat:@"%@",self.imageUrl];
            
            NSMutableURLRequest *fbRequest = [[NSMutableURLRequest alloc] init];
            NSString *fbURL =[NSString stringWithFormat:report,imgURL,@"Objective_Content"];
            [fbRequest setURL:[NSURL URLWithString:fbURL]];
            NSLog(@"FB Responce: %@ ",fbURL);
            // now lets make the connection to the web
            NSData *returnDataFB = [NSURLConnection sendSynchronousRequest:fbRequest returningResponse:nil error:nil];
            NSString *returnStringFB =[[NSString alloc] initWithData:returnDataFB encoding:NSUTF8StringEncoding];
            
            NSLog(@"FB Responce: %@ ",returnStringFB);
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Photo Reported"
                                      message:@""
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            alertView.tag=2;
            [alertView show];
        }
    }
}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [[delegate imageCache] removeAllObjects];

}

@end
