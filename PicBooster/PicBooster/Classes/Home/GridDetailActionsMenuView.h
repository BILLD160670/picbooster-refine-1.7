//
//  MenuView.h
//  Picbooster
//
//  Created by Vassilis Dourmas on 04/2/14.
//  Copyright (c) 2013 Vassilis Dourmas. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol GridDetailActionsMenuViewDelegate

-(void)actionsMenuButtonPressed:(NSString*) button;

@end
@interface GridDetailActionsMenuView : UIView{
    id <GridDetailActionsMenuViewDelegate> delegate;
}
@property (nonatomic,strong) id delegate;


@end