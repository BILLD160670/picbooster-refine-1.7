//
//  GridFooterView.m
//  Picbooster
//
//  Created by George Bafaloukas on 7/3/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "GridFooterView.h"
@interface GridFooterView ()
@property (nonatomic, strong) UIView *mainView;
@end
@implementation GridFooterView
@synthesize mainView;
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        mainView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 40.0f)];
        mainView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"loadButton.png"]];
        [self addSubview:mainView];
        
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 addTarget:self
                    action:@selector(menuButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        button1.tag=1;
        [button1 setTitle:@"Load More" forState:UIControlStateNormal];
        [button1 setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        button1.backgroundColor = [UIColor clearColor];
        button1.frame = CGRectMake(0.0f, 0.0f, 320.0f, 40.0f);
        
        [mainView addSubview:button1];
        
    }
    return self;
}
-(void)menuButtonPressed:(id)sender{
    [delegate loadButtonPressed:@"loadMore"];
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
