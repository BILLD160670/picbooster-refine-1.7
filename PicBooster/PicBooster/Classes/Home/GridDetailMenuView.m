//
//  MenuView.m
//  Picbooster
//
//  Created by Vassilis Dourmas on 04/2/14.
//  Copyright (c) 2013 Vassilis Dourmas. All rights reserved.
//

#import "GridDetailMenuView.h"

@interface GridDetailMenuView ()
@property (nonatomic, strong) UIView *mainView;
@end
@implementation GridDetailMenuView
@synthesize mainView;
@synthesize delegate;
@synthesize labelLikes;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.like = NO;
    if (self) {
        // Initialization code
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            //iPad
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width , self.frame.size.height)];
            mainView.backgroundColor = [UIColor clearColor];
            
            self.buttonLike = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.buttonLike addTarget:self
                        action:@selector(menuButtonPressed:)
              forControlEvents:UIControlEventTouchDown];
            self.buttonLike.tag=1;
            self.buttonLike.backgroundColor=[UIColor clearColor];
           // [self.buttonLike setBackgroundImage:[UIImage imageNamed:@"like_pressed.png"] forState:UIControlStateNormal];
            self.buttonLike.frame = CGRectMake(20.0f, 0.0f, 35.0f, 44.0f);
            
            
            labelLikes  = [[UILabel alloc]initWithFrame:CGRectMake(70, 20, 100, 30)];
            labelLikes.textColor = [UIColor whiteColor];
            labelLikes.backgroundColor = [UIColor clearColor];
            [labelLikes setFont:[UIFont fontWithName:@"Arial" size:20.0f]];
            [labelLikes setTextAlignment:NSTextAlignmentLeft];
            labelLikes.text = @"";
            
            UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
            [button2 addTarget:self
                        action:@selector(menuButtonPressed:)
              forControlEvents:UIControlEventTouchDown];
            button2.tag=3;
            [button2 setBackgroundImage:[UIImage imageNamed:@"more button@2x.png"] forState:UIControlStateNormal];
            button2.frame = CGRectMake(mainView.frame.size.width - 38.0f-10, mainView.frame.size.height/2 - 24/2, 38.0f, 24.0f);
            button2.backgroundColor = [UIColor clearColor];
            
            
            
            [mainView addSubview:self.buttonLike];
            [mainView addSubview:labelLikes];
            [mainView addSubview:button2];

            
        }else{
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width , self.frame.size.height)];
            mainView.backgroundColor = [UIColor clearColor];
            self.buttonLike = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.buttonLike addTarget:self
                        action:@selector(menuButtonPressed:)
              forControlEvents:UIControlEventTouchDown];
            //[self.buttonLike setBackgroundImage:[UIImage imageNamed:@"like_pressed.png"] forState:UIControlStateNormal];
            
            self.buttonLike.tag=1;
            self.buttonLike.backgroundColor=[UIColor clearColor];
            self.buttonLike.frame = CGRectMake(20.0f, 0.0f, 35.0f, 44.0f);;
            
            
            labelLikes  = [[UILabel alloc]initWithFrame:CGRectMake(70, 20, 100, 30)];
            labelLikes.textColor = [UIColor whiteColor];
            labelLikes.backgroundColor = [UIColor clearColor];
            [labelLikes setFont:[UIFont fontWithName:@"Arial" size:20.0f]];
            [labelLikes setTextAlignment:NSTextAlignmentLeft];
            labelLikes.text = @"";
            
            UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
            [button2 addTarget:self
                        action:@selector(menuButtonPressed:)
              forControlEvents:UIControlEventTouchDown];
            button2.tag=3;
            [button2 setBackgroundImage:[UIImage imageNamed:@"more button@2x.png"] forState:UIControlStateNormal];
            button2.frame = CGRectMake(mainView.frame.size.width - 38.0f-10, mainView.frame.size.height/2 - 24/2, 38.0f, 24.0f);
            button2.backgroundColor = [UIColor clearColor];
            

            
            
            [mainView addSubview:self.buttonLike];
            [mainView addSubview:labelLikes];
            [mainView addSubview:button2];

        }
        
        [self addSubview:mainView];
        
        
    }
    return self;
}

-(void)menuButtonPressed:(id)sender{
    UIButton* buttonPressed = sender;
    if(buttonPressed.tag==1){
        [delegate menuButtonPressed:@"Like"];
    }else if (buttonPressed.tag==3){
        [delegate menuButtonPressed:@"Actions"];
    }
}


-(void)setLikePhotoF:(BOOL)like
{
    self.like  = like ? YES : NO;
    
    if(self.like)
    {
        [self.buttonLike setBackgroundImage:[UIImage imageNamed:@"like_pressed.png"] forState:UIControlStateNormal];
        self.buttonLike.userInteractionEnabled = NO;
    }
    else
    {
        [self.buttonLike setBackgroundImage:[UIImage imageNamed:@"like_unpressed.png"] forState:UIControlStateNormal];
    }
}

-(void)setLikes:(NSString*)likes
{
    self.labelLikes.text = likes;
    
}
-(NSString*)getLikes
{
    return self.labelLikes.text;
}

-(void)setComments:(NSString*)comments
{
     self.labelLikes.text = comments;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
