//
//  UserProfile.h
//  Picbooster
//
//  Created by Vassilis Dourmas on 2/13/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomNavigationBar.h"

@interface UserProfile : UIViewController<CustomNavigationBarDelegate>


@property (strong,nonatomic) NSURLConnection* photoCheckins;
@property (strong,nonatomic) NSURLConnection* userLevelConnection;
@property (strong,nonatomic) MBProgressHUD *hud;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) CustomNavigationBar* customNavBar;

@property (nonatomic,strong) UIImageView *rocketsBack;
@property (nonatomic,strong) UIImageView *rocketView;

@property (nonatomic,strong) UIButton *nextRocket;
@property (nonatomic,strong) UIButton *prevRocket;

@property (nonatomic,strong) UIImageView *nextRocketImageView;
@property (nonatomic,strong) UIImageView *prevRocketImageView;

@property (nonatomic ,assign) int rocket;

@property (nonatomic, strong) UILabel *labelCoins;
@property (nonatomic, strong) UILabel *labelPhotoCheckins;
@end
