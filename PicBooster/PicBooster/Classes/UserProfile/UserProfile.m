//
//  UserProfile.m
//  Picbooster
//
//  Created by Vassilis Dourmas on 2/13/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import "UserProfile.h"
#import "Consts.h"
#import "SBJson.h"
#import "AppDelegate.h"

@interface UserProfile ()

@end

@implementation UserProfile

@synthesize userLevelConnection;
@synthesize hud;
@synthesize responseData;
@synthesize customNavBar;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.rocket=0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.responseData = [[NSMutableData alloc]init];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view from its nib.
    
    [self createCustomNavigationBar];
    
    int offsetY = 65;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        offsetY = 45;
    }
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
   
        UIView* blueBackView = [[UIView alloc]initWithFrame:CGRectMake(0, offsetY, 320, 315)];
        blueBackView.backgroundColor = [UIColor colorWithRed:42.0f/255.f green:112.0f/255.0f blue:177.0f/255.0f alpha:1.0f];
        [self.view addSubview:blueBackView];
        
        //174x175
        //    UIImageView *photoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(((320/2)-87)/2, 0, 87, 87)];
        //    [photoImageView setImage:[UIImage imageNamed:@"photo frame.png"]];
        //    [blueBackView addSubview:photoImageView];
        
        //1x204
        //    UIImageView *lineImageView = [[UIImageView alloc]initWithFrame:CGRectMake((320-2)/2, 0, 1, 104)];
        //    [lineImageView setImage:[UIImage imageNamed:@"photoline.png"]];
        //    [blueBackView  addSubview:lineImageView];
        
        //270x107
        UIImageView *photoCheckins = [[UIImageView alloc]initWithFrame:CGRectMake(20,20,135,53)];
        [photoCheckins setImage:[UIImage imageNamed:@"photocheckincounter.png"]];
        [blueBackView addSubview:photoCheckins];
        
        self.labelPhotoCheckins =  [[UILabel alloc]initWithFrame:CGRectMake(0, 8, 70, 25)];
        self.labelPhotoCheckins.textColor = [UIColor whiteColor];
        self.labelPhotoCheckins.font = [UIFont boldSystemFontOfSize:18];
        self.labelPhotoCheckins.textAlignment = NSTextAlignmentRight;
        self.labelPhotoCheckins.backgroundColor = [UIColor clearColor];
        [photoCheckins addSubview:self.labelPhotoCheckins];
        
        
        UIImageView *picCoins = [[UIImageView alloc]initWithFrame:CGRectMake(320-135-20,20,135,53)];
        [picCoins setImage:[UIImage imageNamed:@"Picoins counter.png"]];
        [blueBackView addSubview:picCoins];
        
        self.labelCoins =  [[UILabel alloc]initWithFrame:CGRectMake(0, 8, 85, 25)];
        self.labelCoins.textColor = [UIColor whiteColor];
        self.labelCoins.font = [UIFont boldSystemFontOfSize:18];
        self.labelCoins.textAlignment = NSTextAlignmentRight;
        self.labelCoins.backgroundColor = [UIColor clearColor];
        [picCoins addSubview:self.labelCoins];
        
        
        
        //640x1
        UIImageView *horizLine = [[UIImageView alloc]initWithFrame:CGRectMake(0, 90, 320, 1)];
        [horizLine setImage:[UIImage imageNamed:@"horizline.png"]];
        [blueBackView addSubview:horizLine];
        
        //618x325
        self.rocketsBack = [[UIImageView alloc]initWithFrame:CGRectMake(5, 115, 309, 162)];
        [self.rocketsBack setImage:[UIImage imageNamed:@"rocketsback.png"]];
        [blueBackView addSubview:self.rocketsBack];
        [self.rocketsBack setUserInteractionEnabled:YES];
        
        ///162x206
        self.rocketView = [[UIImageView alloc]initWithFrame:CGRectMake((309-81)/2,(162-103)/2, 81, 103)];
        [self.rocketView setImage:[UIImage imageNamed:@"atlas.png"]];
        [self.rocketsBack addSubview:self.rocketView];
        
        //34x37
        UIImageView *nRocketImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.rocketsBack.frame.size.width - 17-5, (self.rocketsBack.frame.size.height - 18)/2, 17, 18)];
        [nRocketImageView setImage:[UIImage imageNamed:@"next rocket"]];
        self.nextRocketImageView = nRocketImageView;
        [self.rocketsBack addSubview:nRocketImageView];
        
        UIButton *nRocket = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        nRocket.backgroundColor = [UIColor clearColor];
        nRocket.frame = CGRectMake(self.rocketsBack.frame.size.width - 17-15, (self.rocketsBack.frame.size.height - 18)/2  - 10, 37, 38);;
        [nRocket addTarget:self action:@selector(nextRocketPressed) forControlEvents:UIControlEventTouchUpInside];
        self.nextRocket = nRocket;
        [self.rocketsBack addSubview:nRocket];
        
        //34x37
        UIImageView *pRocketImageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, (self.rocketsBack.frame.size.height - 18)/2, 17, 18)];
        [pRocketImageView setImage:[UIImage imageNamed:@"previous rocket"]];
        self.prevRocketImageView = pRocketImageView;
        [self.rocketsBack addSubview:pRocketImageView];
        
        UIButton *pRocket = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        pRocket.backgroundColor = [UIColor clearColor];
        pRocket.frame = CGRectMake(0, (self.rocketsBack.frame.size.height - 18)/2 - 10, 37, 38);
        [pRocket addTarget:self action:@selector(prevRocketPressed) forControlEvents:UIControlEventTouchUpInside];
        self.prevRocket = pRocket;
        [self.rocketsBack addSubview:pRocket];
        

        
        UILabel* rocketLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.rocketsBack.frame.size.height-20, 320, 20)];
        rocketLabel.textColor  = [UIColor whiteColor];
        rocketLabel.text = @"Your rocket is Atlas";
        rocketLabel.font = [UIFont systemFontOfSize:12];
        rocketLabel.textAlignment = NSTextAlignmentCenter;
        rocketLabel.backgroundColor = [UIColor clearColor];
        [self.rocketsBack addSubview:rocketLabel];
        
        UILabel *trMission = [[UILabel alloc]initWithFrame:CGRectMake(0, self.rocketsBack.frame.origin.y +  self.rocketsBack.frame.size.height, 320, 40)];
        trMission.textColor  = [UIColor whiteColor];
        trMission.text = @"TRENDING NISSIONS";
        trMission.font = [UIFont systemFontOfSize:14];
        trMission.textAlignment = NSTextAlignmentCenter;
        trMission.backgroundColor = [UIColor clearColor];
        [blueBackView addSubview:trMission];
        
        UITextView* textView = [[UITextView alloc]initWithFrame:CGRectMake(0,offsetY +  trMission.frame.origin.y + trMission.frame.size.height,  320, 100)];
        textView.backgroundColor = [UIColor clearColor];
        textView.textColor = [UIColor blackColor];
        textView.text = @"TODAY'S MISSION IS TO\n GET 1000 POINTS IN THE NEXT DAYS!\n\nARE YOU READY? CAN YOU MAKE IT?\n";
        textView.font = [UIFont systemFontOfSize:14];
        textView.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:textView];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        btn.frame =  CGRectMake(35, textView.frame.origin.y + textView.frame.size.height, 250, 60);
        [btn setTitle:@"CLICK HERE AND ACCEPT THE MISSION NOW!" forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont boldSystemFontOfSize : 14];
        btn.titleLabel.textColor = [UIColor redColor];
        btn.titleLabel.textAlignment = NSTextAlignmentCenter;
        btn.backgroundColor = [UIColor clearColor];
        btn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.view addSubview:btn];
        
        [self getPhotoCheckins:[AppDelegate getAppDelegate].fbUserId];
        
    } else {
    
    UIView* blueBackView = [[UIView alloc]initWithFrame:CGRectMake(0, offsetY, 320, 315)];
    blueBackView.backgroundColor = [UIColor colorWithRed:42.0f/255.f green:112.0f/255.0f blue:177.0f/255.0f alpha:1.0f];
    [self.view addSubview:blueBackView];
    
    //174x175
//    UIImageView *photoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(((320/2)-87)/2, 0, 87, 87)];
//    [photoImageView setImage:[UIImage imageNamed:@"photo frame.png"]];
//    [blueBackView addSubview:photoImageView];
    
    //1x204
//    UIImageView *lineImageView = [[UIImageView alloc]initWithFrame:CGRectMake((320-2)/2, 0, 1, 104)];
//    [lineImageView setImage:[UIImage imageNamed:@"photoline.png"]];
//    [blueBackView  addSubview:lineImageView];
    
    //270x107
        UIImageView *photoCheckins = [[UIImageView alloc]initWithFrame:CGRectMake(20,20,135,53)];
        [photoCheckins setImage:[UIImage imageNamed:@"photocheckincounter.png"]];
        [blueBackView addSubview:photoCheckins];
        
        self.labelPhotoCheckins =  [[UILabel alloc]initWithFrame:CGRectMake(0, 8, 65, 25)];
        self.labelPhotoCheckins.textColor = [UIColor whiteColor];
        self.labelPhotoCheckins.font = [UIFont boldSystemFontOfSize:18];
        self.labelPhotoCheckins.textAlignment = NSTextAlignmentRight;
        self.labelPhotoCheckins.backgroundColor = [UIColor clearColor];
        [photoCheckins addSubview:self.labelPhotoCheckins];

        
        UIImageView *picCoins = [[UIImageView alloc]initWithFrame:CGRectMake(320-135-20,20,135,53)];
    [picCoins setImage:[UIImage imageNamed:@"Picoins counter.png"]];
    [blueBackView addSubview:picCoins];
    
    self.labelCoins =  [[UILabel alloc]initWithFrame:CGRectMake(0, 8, 85, 25)];
    self.labelCoins.textColor = [UIColor whiteColor];
    self.labelCoins.font = [UIFont boldSystemFontOfSize:18];
    self.labelCoins.textAlignment = NSTextAlignmentRight;
    self.labelCoins.backgroundColor = [UIColor clearColor];
    [picCoins addSubview:self.labelCoins];
    

    
    
    //640x1
    UIImageView *horizLine = [[UIImageView alloc]initWithFrame:CGRectMake(0, 90, 320, 1)];
    [horizLine setImage:[UIImage imageNamed:@"horizline.png"]];
    [blueBackView addSubview:horizLine];

    //618x325
    self.rocketsBack = [[UIImageView alloc]initWithFrame:CGRectMake(5, 115, 309, 162)];
    [self.rocketsBack setImage:[UIImage imageNamed:@"rocketsback.png"]];
    [blueBackView addSubview:self.rocketsBack];
    [self.rocketsBack setUserInteractionEnabled:YES];
       
        ///162x206
        self.rocketView = [[UIImageView alloc]initWithFrame:CGRectMake((309-81)/2,(162-103)/2, 81, 103)];
        [self.rocketView setImage:[UIImage imageNamed:@"atlas.png"]];
        [self.rocketsBack addSubview:self.rocketView];

        UIImageView *nRocketImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.rocketsBack.frame.size.width - 17-5, (self.rocketsBack.frame.size.height - 18)/2, 17, 18)];
        [nRocketImageView setImage:[UIImage imageNamed:@"next rocket"]];
        self.nextRocketImageView = nRocketImageView;
        [self.rocketsBack addSubview:nRocketImageView];
        
        UIButton *nRocket = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        nRocket.backgroundColor = [UIColor clearColor];
        nRocket.frame = CGRectMake(self.rocketsBack.frame.size.width - 17-15, (self.rocketsBack.frame.size.height - 18)/2  - 10, 37, 38);;
        [nRocket addTarget:self action:@selector(nextRocketPressed) forControlEvents:UIControlEventTouchUpInside];
        self.nextRocket = nRocket;
        [self.rocketsBack addSubview:nRocket];
        
        //34x37
        UIImageView *pRocketImageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, (self.rocketsBack.frame.size.height - 18)/2, 17, 18)];
        [pRocketImageView setImage:[UIImage imageNamed:@"previous rocket"]];
        self.prevRocketImageView = pRocketImageView;
        [self.rocketsBack addSubview:pRocketImageView];
        
        UIButton *pRocket = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        pRocket.backgroundColor = [UIColor clearColor];
        pRocket.frame = CGRectMake(0, (self.rocketsBack.frame.size.height - 18)/2 - 10, 37, 38);
        [pRocket addTarget:self action:@selector(prevRocketPressed) forControlEvents:UIControlEventTouchUpInside];
        self.prevRocket = pRocket;
        [self.rocketsBack addSubview:pRocket];

        
    UILabel* rocketLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.rocketsBack.frame.size.height-20, 320, 20)];
    rocketLabel.textColor  = [UIColor whiteColor];
    rocketLabel.text = @"Your rocket is Atlas";
    rocketLabel.font = [UIFont systemFontOfSize:12];
    rocketLabel.textAlignment = NSTextAlignmentCenter;
    rocketLabel.backgroundColor = [UIColor clearColor];
    [self.rocketsBack addSubview:rocketLabel];
    
    UILabel *trMission = [[UILabel alloc]initWithFrame:CGRectMake(0, self.rocketsBack.frame.origin.y +  self.rocketsBack.frame.size.height, 320, 40)];
    trMission.textColor  = [UIColor whiteColor];
    trMission.text = @"TRENDING NISSIONS";
    trMission.font = [UIFont systemFontOfSize:14];
    trMission.textAlignment = NSTextAlignmentCenter;
    trMission.backgroundColor = [UIColor clearColor];
    [blueBackView addSubview:trMission];
    
    UITextView* textView = [[UITextView alloc]initWithFrame:CGRectMake(0,offsetY +  trMission.frame.origin.y + trMission.frame.size.height,  320, 100)];
    textView.backgroundColor = [UIColor clearColor];
    textView.textColor = [UIColor blackColor];
    textView.text = @"TODAY'S MISSION IS TO\n GET 1000 POINTS IN THE NEXT DAYS!\n\nARE YOU READY? CAN YOU MAKE IT?\n";
    textView.font = [UIFont systemFontOfSize:14];
    textView.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:textView];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    btn.frame =  CGRectMake(35, textView.frame.origin.y + textView.frame.size.height, 250, 60);
    [btn setTitle:@"CLICK HERE AND ACCEPT THE MISSION NOW!" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont boldSystemFontOfSize : 14];
    btn.titleLabel.textColor = [UIColor redColor];
    btn.titleLabel.textAlignment = NSTextAlignmentCenter;
    btn.backgroundColor = [UIColor clearColor];
    btn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.view addSubview:btn];
    
    [self getPhotoCheckins:[AppDelegate getAppDelegate].fbUserId];
    }
    
    [self updateButtons];
}

- (void)nextRocketPressed
{
    self.rocket += 1;
    UIImage *image = nil;
    
    if(self.rocket == 0)
    {
        image = [UIImage imageNamed:@"atlas.png"];
    }
    else
    {
        image = [UIImage imageNamed:@"locked rocket.png"];
    }
    [self.rocketView setImage:image];
    [self performSelectorOnMainThread:@selector(updateButtons) withObject:nil waitUntilDone:NO];

}

- (void)prevRocketPressed
{
    self.rocket -= 1;
    UIImage *image = nil;
    
    if(self.rocket == 0)
    {
        image = [UIImage imageNamed:@"atlas.png"];
    }
    else
    {
        image = [UIImage imageNamed:@"locked rocket.png"];
    }
    [self.rocketView setImage:image];
    [self performSelectorOnMainThread:@selector(updateButtons) withObject:nil waitUntilDone:NO];
    
}

-(void)updateButtons
{
    if(self.rocket==0)
    {
        self.nextRocketImageView.hidden = NO;
        self.prevRocketImageView.hidden = YES;
        
        self.nextRocket.enabled = YES;
        self.prevRocket.enabled = NO;

    }
    else if(self.rocket ==2)
    {
        self.nextRocketImageView.hidden = YES;
        self.prevRocketImageView.hidden = NO;
        
        self.nextRocket.enabled = NO;
        self.prevRocket.enabled = YES;
    }
    else 
    {
        self.nextRocketImageView.hidden = NO;
        self.prevRocketImageView.hidden = NO;
        
        self.nextRocket.enabled = YES;
        self.prevRocket.enabled = YES;
    }

}

-(void)createCustomNavigationBar
{
    NSString *deviceType = [UIDevice currentDevice].model;
    UIImageView* titleView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 84, 17)];
    [titleView setImage:[UIImage imageNamed:@"my profile tab logo.png"]];
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 45.0f) displayPoints:false displayPicboosterTitleView:false title:@"" displaySettingsButton:false titleImageView:titleView];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 45) displayPoints:false displayPicboosterTitleView:false title:@"" displaySettingsButton:false titleImageView:titleView];
        }
    }
    else
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 65.0f) displayPoints:false displayPicboosterTitleView:false title:@"" displaySettingsButton:false titleImageView:titleView];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 65) displayPoints:false displayPicboosterTitleView:false title:@"" displaySettingsButton:false titleImageView:titleView];
        }
    }
    
    [self.view addSubview:self.customNavBar];
    self.customNavBar.delegate = self;
    
}

//customNavigationBar actions
-(void)barButtonPressed:(NSString *)button{
    if([button isEqualToString:@"Back"]){
        [self.navigationController popViewControllerAnimated:true];
    }
    else if([button isEqualToString:@"Settings"]){
        [self settingsClicked];
    }
}

-(void)settingsClicked{
    //SettingsViewController *settings=[[SettingsViewController alloc] init];
    //[self.navigationController pushViewController:settings animated:YES];
    
    //[self presentViewController:settings animated:YES completion:nil];
    
    SettingsView *mySettingsView = [AppDelegate getAppDelegate].settingsView;
    if(mySettingsView.settingsIsVisible)
        return;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    CGFloat finalHeight  = 330;
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        finalHeight = 330;
    }
    [self.view addSubview:mySettingsView];
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         mySettingsView.frame = CGRectMake(0.0f , mySettingsView.frame.origin.y , mySettingsView.frame.size.width , finalHeight);
                         mySettingsView.settingsIsVisible = true;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
}


-(void) getLevel:(NSString *)fbUserId{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *urlString= [NSString stringWithFormat:getUserLevel,fbUserId];
    NSMutableURLRequest *pointsRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
    [pointsRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [pointsRequest setTimeoutInterval:20.0];
    self.userLevelConnection =[[NSURLConnection alloc] initWithRequest:pointsRequest delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}

-(void) getPhotoCheckins:(NSString *)fbUserId{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *urlString= [NSString stringWithFormat:getUserPhotoCheckins,fbUserId];
    NSMutableURLRequest *pointsRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
    [pointsRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [pointsRequest setTimeoutInterval:20.0];
    self.photoCheckins = [[NSURLConnection alloc] initWithRequest:pointsRequest delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}





//NSURLConnection methods following
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Connection Error!"
												   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
	[alert show];
    
    [self.hud hide:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *myResponseString=[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    SBJsonParser *json = [[SBJsonParser alloc] init] ;
    if ([connection isEqual:self.photoCheckins]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSMutableDictionary* photoCheckins = [json objectWithString:myResponseString];
        NSString* photoCheckinsC =[photoCheckins valueForKey:@"count"];
        self.labelCoins.text  = [NSString stringWithFormat:@"%@",[AppDelegate getAppDelegate].currentUserPointsString];
        self.labelPhotoCheckins.text  = [NSString stringWithFormat:@"%@",photoCheckinsC];
        [self.hud hide:YES];
        [self getLevel:[AppDelegate getAppDelegate].fbUserId];
    }else if ([connection isEqual:self.userLevelConnection]){
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSRange r=[myResponseString rangeOfString:@"["];
        myResponseString = [myResponseString substringFromIndex:r.length];
        int level   =[[myResponseString substringToIndex:myResponseString.length-1] intValue];
        //162x206
        //[self.imgViewRocket setImage:[UIImage imageNamed:[NSString stringWithFormat:@"rocket_%d",level]]];
  //      [self.imgViewRocket setImage:[UIImage imageNamed:[NSString stringWithFormat:@"atlas.png",level]]];
    }
    
    [self.hud hide:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
