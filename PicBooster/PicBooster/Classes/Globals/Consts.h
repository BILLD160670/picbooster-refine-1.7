//
//  Consts.h
//  Picbooster
//
//  Created by George Bafaloukas on 6/10/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


 #define HOME  @"http://www.picbooster.net/"
 
 #define HOMEWithValues  @"http://www.picbooster.net/%@"
 
 #define APIUrl  @"http://www.picbooster.net/api/%@"
 
 #define closestVenues  @"http://www.picbooster.net/api/get_closest_venues.php?lat=%f&lng=%f"
 
 #define report  @"http://www.picbooster.net/api/report.php?url=%@&reason=%@"
 
 #define getGrid  @"http://www.picbooster.net/api/get_grid_large.php"
 #define getLikePhoto  @"http://www.picbooster.net/api/get_like.php?UserId=%@&Url=%@" //new
 #define setLikePhoto  @"http://www.picbooster.net/api/set_like.php?UserId=%@&Url=%@" //new

 #define uploadToPicbooster  @"http://www.picbooster.net/api/upload_image.php"
 
 #define uploadToFacebook  @"http://www.picbooster.net/api/upload_to_facebook.php?token=%@&file=%@&venueid=%@&minima=%@&is_brand=%@"
 
 #define oneVenue  @"http://www.picbooster.net/api/one_venue.php?id=%@"

#define getBrands  @"http://www.picbooster.net/api/getBrands.php?cat_id=%i"
 
#define getBrandCategories  @"http://www.picbooster.net/api/getBrandCategories.php"

#define getUserPoints  @"http://www.picbooster.net/api/getUserPoints.php?fbUserId=%@"
#define getUserPhotoCheckins  @"http://www.picbooster.net/api/userPicCounter.php?user_id=%@"

#define getUserLevel   @"http://www.picbooster.net/api/get_level_user.php?fbUserId=%@"


#define getEmotionCategories @"http://www.picbooster.net/api/getAnimCateg.php"

#define getEmotions  @"http://www.picbooster.net/api/getAnim.php?num=%d&cat_id=%d"

#define searchVenues  @"http://www.picbooster.net/api/search_venues.php?lat=%f&lng=%f"

#define getPhotosOfBrand @"http://www.picbooster.net/api/getAllBrandsPhoto.php?id=%@"

#define getPhotosOfVenue @"http://www.picbooster.net/api/getAllVenuesPhoto.php?id=%@"



/*
#define HOME  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web"

#define HOMEWithValues  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/%@"

#define APIUrl  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/%@"

#define closestVenues  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/get_closest_venues.php?lat=%f&lng=%f"

#define report  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/report.php?url=%@&reason=%@"

#define getGrid  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/get_grid_large.php"
 
 

#define uploadToPicbooster  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/upload_image.php"

#define uploadToFacebook  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/upload_to_facebook.php?token=%@&minima=&file=%@&venueid=%@&minima=%@"

#define oneVenue  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/one_venue.php?id=%@"

#define getBrands  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/getBrands.php?cat_id=%i"

#define getBrandCategories  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/getBrandCategories.php"

#define getUserPoints  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/getUserPoints.php?fbUserId=%@"
 
  #define getGridDetail  @"http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/get_grid_detail.php?imageURL=?"
 
  #define getUserProfile  @""http://192.168.1.87:8888/Picbooster/Picbooster_Web/api/user_profile.php?id=%@"
 
 */



