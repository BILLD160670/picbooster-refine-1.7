//
//  GridViewCell.m
//  Picbooster
//
//  Created by George Bafaloukas on 4/4/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "GridViewCell.h"

@implementation GridViewCell
@synthesize captionLabel,imageView,imageViewRotator;

- (id) initWithFrame: (CGRect) frame reuseIdentifier: (NSString *) aReuseIdentifier
{
    self = [super initWithFrame: frame reuseIdentifier: aReuseIdentifier];
    if ( self)
    {
        UIView* mainView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 160.0f, 160.0f)];
        mainView.backgroundColor=[UIColor clearColor];
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 160.0f, 160.0f)];
        self.imageViewRotator = [[UIImageView alloc] initWithFrame:CGRectMake((160.0f -31.0f)/2, (160.0f - 40.0f)/2, 31.0f, 40.0f)];
        [mainView addSubview:imageView];
        [mainView addSubview:imageViewRotator];
        self.contentView.backgroundColor=[UIColor clearColor];
        [self.contentView addSubview:mainView];
    }
    return self;
}

@end
