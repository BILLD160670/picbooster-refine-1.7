//
//  GridViewCell.h
//  Picbooster
//
//  Created by George Bafaloukas on 4/4/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQGridView.h"
@interface GridViewCell : AQGridViewCell
@property (nonatomic, strong) UIImageView * imageView;
@property (nonatomic, strong) UIImageView * imageViewRotator;
@property (nonatomic, strong) UILabel * captionLabel;
@end