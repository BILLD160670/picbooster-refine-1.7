//
//  PhotoViewController.m
//  Picbooster
//
//  Created by George Bafaloukas on 4/24/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "PhotoViewController.h"
#import "Consts.h"
#import "CommentBoxView.h"
#import "AppDelegate.h"
#import "AwardUploadViewController.h"

@interface PhotoViewController ()

@end

@implementation PhotoViewController
@synthesize photo,originalImage,watermarkImage,venueId;
@synthesize newPromo,promoMessage;
@synthesize isBrand;
@synthesize scrollView;
@synthesize docController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.comment = @"";
    }
    return self;
}

-(void)createCustomNavigationBar:(NSString*)title
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 45.0f) displayPoints:false displayPicboosterTitleView:TRUE title:title displaySettingsButton:YES titleImageView:nil] ;
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 45) displayPoints:false displayPicboosterTitleView:TRUE title:title displaySettingsButton:NO titleImageView:nil];
        }
    }
    else
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 65.0f) displayPoints:false displayPicboosterTitleView:TRUE title:title displaySettingsButton:TRUE titleImageView:nil];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 65) displayPoints:false displayPicboosterTitleView:TRUE title:title displaySettingsButton:TRUE titleImageView:nil];
        }
    }
    
    [self.view addSubview:self.customNavBar];
    self.customNavBar.delegate = self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIImage * backgroundPattern = [UIImage imageNamed:@"backgroundIPHONE4.png"];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:backgroundPattern]];
    
    [self createCustomNavigationBar:@""];
    
    int offsetY = self.customNavBar.frame.size.height;
    int imageViewHeight = 480 - 40 - 28 - 84 - 40-offsetY + 4;
    if(self.view.frame.size.height>480)
    {
        imageViewHeight = 568 - 40 - 28 - 84  - 40-offsetY + 4;
    }
    
    self.photo =[[UIImageView alloc] initWithFrame:CGRectMake(0.0f, offsetY, 320.0f, imageViewHeight)];
    self.photo.image = self.originalImage;
    self.photo.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.photo];
    
    offsetY = self.photo.frame.origin.y + self.photo.frame.size.height;
    self.commentsField  = [[UITextField alloc]initWithFrame:CGRectMake(0, offsetY, 320, 40)];
    self.commentsField.text = @"Add Caption here...";
    self.commentsField.font = [UIFont systemFontOfSize:16.0f];
    [self.view addSubview:self.commentsField];
    UIButton* addCaptionButton = [[UIButton alloc]initWithFrame:CGRectMake(0, offsetY, 320, 40)];
    addCaptionButton.backgroundColor = [UIColor clearColor];
    [addCaptionButton addTarget:self action:@selector(getComment) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.commentsField];
    [self.view addSubview:addCaptionButton];
    

    offsetY = self.commentsField.frame.origin.y + self.commentsField.frame.size.height;
    UIImageView* alsoShareImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, offsetY, 320, 28)];
    alsoShareImageView.image = [UIImage imageNamed:@"alsosharebar.png"];
    [self.view addSubview:alsoShareImageView];
    alsoShareImageView.backgroundColor = [UIColor clearColor];
    
    offsetY = alsoShareImageView.frame.origin.y + alsoShareImageView.frame.size.height -4;
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.photo.frame = CGRectMake(224.0f, 0.0f, 320.0f, 310.0f);
        ShareView *shareView=[[ShareView alloc] initWithFrame:CGRectMake(224.0f, offsetY, 320.0f, 84.0f)];
        shareView.delegate=self;
        [self.view addSubview:shareView];
        offsetY = shareView.frame.origin.y + shareView.frame.size.height;
    }else{
        ShareView *shareView=[[ShareView alloc] initWithFrame:CGRectMake(0.0, offsetY, 320.0f, 84.0f)];
        shareView.delegate=self;
        [self.view addSubview:shareView];
        offsetY = shareView.frame.origin.y + shareView.frame.size.height;
    }
    
    [self.view bringSubviewToFront:alsoShareImageView];
    
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"photocheck in button.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(boostImage) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(0, offsetY, 320, 40)];
    [self.view addSubview:button];
    
    if ([[FBSession activeSession]isOpen]) {
        // Ask for publish_actions permissions in context
        if ([FBSession.activeSession.permissions
             indexOfObject:@"publish_actions"] == NSNotFound) {
            // No permissions found in session, ask for it
            [FBSession.activeSession
             requestNewPublishPermissions:
             [NSArray arrayWithObjects:@"publish_actions",nil]
             defaultAudience:FBSessionDefaultAudienceFriends
             completionHandler:^(FBSession *session, NSError *error) {
                 if (!error) {
                     // If permissions granted, publish the story
                     NSLog(@"Permissions Granted");
                 }else{
                     UIAlertView *alertView = [[UIAlertView alloc]
                                               initWithTitle:@"Error Getting Permissions"
                                               message:error.localizedDescription
                                               delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
                     [alertView show];
                 }
             }];
        } else {
            // If permissions present, publish the story
            NSLog(@"Permissions Present");
        }
    }else{
        [FBSession openActiveSessionWithPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                           defaultAudience:FBSessionDefaultAudienceFriends
                                              allowLoginUI:YES
                                         completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                             if (!error && status == FBSessionStateOpen) {
                                                 NSLog(@"Session Opened with permissions");
                                             }else{
                                                 NSLog(@"error");
                                             }
                                         }];
    
    }
}



void RetinaAwareUIGraphicsBeginImageContext(CGSize size) {
    
    static CGFloat scale = -1.0;
    
    if (scale<0.0) {
        
        UIScreen *screen = [UIScreen mainScreen];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 4.0) {
            
            scale = [screen scale];
            
        }
        
        else {
            
            scale = 0.0;    // mean use old api
            
        }
        
    }
    
    if (scale>0.0) {
        
        UIGraphicsBeginImageContextWithOptions(size, NO, scale);
        
    }
    
    else {
        
        UIGraphicsBeginImageContext(size);
        
    }
    
}
-(id) watermarkImage:(UIImage *)clearImage{
    UIImage *backgroundImage=clearImage;
    UIImage *result;
    if (self.isBrand) {
        RetinaAwareUIGraphicsBeginImageContext(backgroundImage.size);
        backgroundImage=[backgroundImage scaleToSize:CGSizeMake(800.0,800.0)];
        [backgroundImage drawInRect:CGRectMake(0, 0, backgroundImage.size.width, backgroundImage.size.height)];
        [watermarkImage drawInRect:CGRectMake(10.0f, (backgroundImage.size.height - 200.0), 200.0, 200.0)];
        result = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }else{
        RetinaAwareUIGraphicsBeginImageContext(backgroundImage.size);
        backgroundImage=[backgroundImage scaleToSize:CGSizeMake(800.0,800.0)];
        [backgroundImage drawInRect:CGRectMake(0, 0, backgroundImage.size.width, backgroundImage.size.height)];
        [watermarkImage drawInRect:CGRectMake((backgroundImage.size.width - 200.0)-10, (backgroundImage.size.height - 200.0), 200.0, 200.0)];
        result = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    result=[result scaleToSize:CGSizeMake(800.0,800.0)];
    return result;
}

-(void)getComment{
    CommentBoxView *commentView=[[CommentBoxView alloc] init];
    commentView.delegate=self;
    commentView.finalImage = photo.image;
    commentView.passwordTextField.text = self.comment;
    [self presentPopupViewControllerNoPop:commentView animationType:MJPopupViewAnimationFade];
}


-(void)closeAwardForm
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    [self performSelector:@selector(goToHome) withObject:nil afterDelay:1.0f];
}

-(void) goToHome
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


-(void)boostImage{
    [self startUploading:self.comment];
}

-(void)photoCaption:(NSString *)text{
    if ([text isEqualToString:@"CANCEL"]) {
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }else{
        [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        self.comment =text;
        self.commentsField.text = text;
    }
}


-(void)startUploading:(NSString *)comment{
    
    UIView* transpBlackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
    transpBlackView.backgroundColor = [UIColor clearColor];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        NSString* responce=[self uploadImage:comment];
        SBJsonParser *json = [[SBJsonParser alloc] init] ;
        self.responceJson = [json objectWithString:responce];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if ([[self.responceJson valueForKey:@"status"] isEqualToString:@"ERROR"]) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle:@"Ooooups"
                                          message:@"Something went wrong!"
                                          delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
                [alertView show];
                [self.navigationController popViewControllerAnimated:YES];
            }else if([[self.responceJson valueForKey:@"status"]  isEqualToString:@"SUCCESS"]){
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 int count = [[self.responceJson valueForKey:@"count"] integerValue];
                if (self.newPromo && ![self.promoMessage isEqualToString:@""]) {
//                    PopUpViewController *popUpView=[[PopUpViewController alloc] init];
//                    popUpView.brandImage = self.watermarkImage;
//                    popUpView.message = self.promoMessage;
//                    popUpView.delegate = self;
//                    [self presentPopupViewController:popUpView animationType:MJPopupViewAnimationFade];
                   // [self.navigationController popToRootViewControllerAnimated:YES];
                AwardUploadViewController* aViewController = [[AwardUploadViewController alloc]init:self wifi:[self.responceJson valueForKey:@"wifi"]];
                //[self.navigationController presentPopupViewController:aViewController animationType:MJPopupViewAnimationFade];
                 [self presentPopupViewControllerNoPop:aViewController animationType:MJPopupViewAnimationFade];
                }else if (count == 1 && ![self.promoMessage isEqualToString:@""]) {
                    PopUpViewController *popUpView=[[PopUpViewController alloc] init];
//                    popUpView.message = self.promoMessage;
//                    popUpView.brandImage = self.watermarkImage;
//                    popUpView.delegate = self;
//                    [self presentPopupViewController:popUpView animationType:MJPopupViewAnimationFade];
                    //[self.navigationController popToRootViewControllerAnimated:YES];
                    AwardUploadViewController* aViewController = [[AwardUploadViewController alloc]init:self wifi:[self.responceJson valueForKey:@"wifi"]];
                   // [self.navigationController presentPopupViewController:aViewController animationType:MJPopupViewAnimationFade];
                    [self presentPopupViewControllerNoPop:aViewController animationType:MJPopupViewAnimationFade];

                    
                }else {
//                    UIAlertView *alertView = [[UIAlertView alloc]
//                                              initWithTitle:@"Success"
//                                              message:@"Photo Uploaded!"
//                                              delegate:self
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//                    [alertView show];
//                    [self.navigationController popViewControllerAnimated:YES];
                    
                    //[self.navigationController popToRootViewControllerAnimated:YES];
                     AwardUploadViewController* aViewController = [[AwardUploadViewController alloc]init:self wifi:[self.responceJson valueForKey:@"wifi"]];
                    [self.navigationController presentPopupViewController:aViewController animationType:MJPopupViewAnimationFade];
                    
                }
            }else if ([responce isEqualToString:@"SUCCESS"]){
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle:@"Success"
                                          message:@"Photo Uploaded!"
                                          delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
               // [alertView show];
               // [self.navigationController popViewControllerAnimated:YES];
                //[self.navigationController popToRootViewControllerAnimated:YES];
                AwardUploadViewController* aViewController = [[AwardUploadViewController alloc]init:self wifi:[self.responceJson valueForKey:@"wifi"]];
                [self.navigationController presentPopupViewController:aViewController animationType:MJPopupViewAnimationFade];
            }else{
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle:@"Ooooups"
                                          message:@"Something went wrong!"
                                          delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
                [alertView show];
                [self.navigationController popViewControllerAnimated:YES];
            }
            });
    });
}

-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}
-(void)saveToLibraryAction{
        UIImage* finalImage = [self watermarkImage:photo.image];
        UIImageWriteToSavedPhotosAlbum(finalImage,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
}
-(NSString* )uploadImage:(NSString *)comment{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    UIImage* finalImage = [self watermarkImage:photo.image];
    NSData* fileData = UIImageJPEGRepresentation(finalImage, 0.7);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:uploadToPicbooster]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = [NSString stringWithFormat:@"0xKhTmLbOuNdArY"]; // This is important! //NSURLConnection is very sensitive to format.
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"file\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:fileData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"param2\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"paramstringvalue1"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"param3\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"paramstringvalue2"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    // now lets make the connection to the web
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString =[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSString *imgURL = [NSString stringWithFormat:@"%@",returnString];
    NSString *fbAccessToken = [[[FBSession activeSession] accessTokenData] accessToken];
    NSMutableURLRequest *fbRequest = [[NSMutableURLRequest alloc] init];
    NSString *fbURL =[NSString stringWithFormat:uploadToFacebook,fbAccessToken,imgURL,venueId,comment,isBrand?@"0":@"1ok"];
    NSString *newfbURL = [(NSString *)fbURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    newfbURL= [newfbURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [fbRequest setURL:[NSURL URLWithString:newfbURL]];
    // now lets make the connection to the web
    NSError *error;
    NSData *returnDataFB = [NSURLConnection sendSynchronousRequest:fbRequest returningResponse:nil error:&error];
    NSString *returnStringFB =[[NSString alloc] initWithData:returnDataFB encoding:NSUTF8StringEncoding];
    return returnStringFB;
}

-(void)shareButtonPressed:(NSString *)button{
    NSLog(@"Button name:%@",button);
    if ([button isEqualToString:@"Facebook"]) {
        [self fbTapped];
    }else if ([button isEqualToString:@"Twitter"]){
        [self tweetTapped];
    }else if([button isEqualToString:@"Mail"]){
        [self mailTapped];
    }else if([button isEqualToString:@"Instagram"]){
        [self instaTapped];
    }
}


//customNavigationBar actions
-(void)barButtonPressed:(NSString *)button{
    if([button isEqualToString:@"Back"]){
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
        if([button isEqualToString:@"Settings"]){
         [self settingsClicked];
    }
}





-(void)settingsClicked{
    
    SettingsView *mySettingsView = [AppDelegate getAppDelegate].settingsView;
    if(mySettingsView.settingsIsVisible)
    {
        [mySettingsView hideSettings];
    }
    else{
        
        NSString *deviceType = [UIDevice currentDevice].model;
        CGFloat finalHeight  = 330;
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            finalHeight = 330;
        }
        [self.view addSubview:mySettingsView];
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationOptionTransitionNone
                         animations:^{
                             mySettingsView.frame = CGRectMake(0.0f , mySettingsView.frame.origin.y , mySettingsView.frame.size.width , finalHeight);
                             mySettingsView.settingsIsVisible = true;
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                         }];
    }
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)fbTapped{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Hey!"
                              message:@"Did you know that when you boost your photo with Picbooster you automatically share it to Facebook as well!"
                              delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
    [alertView show];

}
-(void)tweetTapped{
    UIImage* finalImage = [self watermarkImage:photo.image];
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        //[tweetSheet setInitialText:@"I've boosted my photo with Picbooster!"];
        [tweetSheet setInitialText:self.comment];
        [tweetSheet addImage:finalImage];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
        
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
//        
//        if([self loginToTwitter])
//        {
//            
//        }
//        else
//        {
//            
//        }
    }
}

-(bool)loginToTwitter
{
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierTwitter];
    
    __block bool gr = false;
    [account requestAccessToAccountsWithType:accountType options:nil
                                  completion:^(BOOL granted, NSError *error)
    {
        if (granted == YES)
        {
            // Get account and communicate with Twitter API
            
            NSArray *arrayOfAccounts = [account
                                        accountsWithAccountType:accountType];
            
            if ([arrayOfAccounts count] > 0)
            {
                ACAccount *twitterAccount = [arrayOfAccounts lastObject];
                
                NSDictionary *message = @{@"status": self.comment};
                
                NSURL *requestURL = [NSURL
                                     URLWithString:@"http://api.twitter.com/1/statuses/update.json"];
                
                SLRequest *postRequest = [SLRequest
                                          requestForServiceType:SLServiceTypeTwitter
                                          requestMethod:SLRequestMethodPOST
                                          URL:requestURL parameters:message];
            }
            gr  = true;
        }
        else
        {
            gr  = false;
        }
    }];
    
    return gr;
}


-(void)mailTapped{
    UIImage* finalImage = [self watermarkImage:photo.image];
    if ([MFMailComposeViewController canSendMail])
    {
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:@"I've boosted my photo with Picbooster"];
        NSData *imageData = UIImagePNGRepresentation(finalImage);
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"image"];
        //NSString *emailBody = @"Mail sent from picbooster!";
        NSString *emailBody = self.comment;
        [mailer setMessageBody:emailBody isHTML:NO];
        
        [self presentViewController:mailer animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}
-(void) instaTapped{
    NSLog(@"Instagram");
    UIImage* finalImage = [self watermarkImage:photo.image];
    NSString *imagePath = [NSString stringWithFormat:@"%@/image.igo",[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
    [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
    [UIImagePNGRepresentation(finalImage) writeToFile:imagePath atomically:YES];
    docController= [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:imagePath]];
    docController.delegate = self;
    docController.UTI = @"com.instagram.exclusivegram";
    //docController.annotation = [NSDictionary dictionaryWithObject:@"I've boosted my photo with Picbooster" forKey:@"InstagramCaption"];
    docController.annotation = [NSDictionary dictionaryWithObject:self.comment forKey:@"InstagramCaption"];
    [docController presentOpenInMenuFromRect:self.view.frame inView:self.view animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [[delegate imageCache] removeAllObjects];
}

@end
