//
//  PhotoViewController.h
//  Picbooster
//
//  Created by George Bafaloukas on 4/24/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+FiltrrCompositions.h"
#import "MBProgressHUD.h"
#import <FacebookSDK/FacebookSDK.h>
#import "UIImage+Scale.h"
#import "SBJson.h"
#import "UIViewController+MJPopupViewController.h"
#import "PopUpViewController.h"
#import "CommentBoxView.h"
#import "ShareView.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "CustomNavigationBar.h"
#import "AwardUploadViewController.h"

@interface PhotoViewController : UIViewController <MBProgressHUDDelegate,MJSecondPopupDelegate,UIScrollViewDelegate,CommentBoxDelegate,shareButtonPressed,UIDocumentInteractionControllerDelegate,MFMailComposeViewControllerDelegate,AwardUploadDelegate>{
    UIImageView *photo;
    UIImage *watermarkImage;
    NSString *venueId;
}
@property (nonatomic, strong) CustomNavigationBar* customNavBar;

@property NSString *comment;
@property NSString *venueId;
@property UIImage *watermarkImage;
@property (strong,nonatomic) UIImage *originalImage;
@property UIImageView *photo;
@property (strong,nonatomic)  UITextField* commentsField;
@property (nonatomic) BOOL newPromo;
@property (strong,nonatomic) NSString *promoMessage;
@property (strong,nonatomic)  NSDictionary *responceJson;
@property (nonatomic) BOOL isBrand;
@property (nonatomic) BOOL isEmotion;
@property (strong,nonatomic) UIScrollView *scrollView;
@property (strong,nonatomic) UIDocumentInteractionController* docController;

@end
