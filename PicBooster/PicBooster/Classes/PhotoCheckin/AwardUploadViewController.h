//
//  AwardUploadViewController.h
//  Picbooster
//
//  Created by Vassilis Dourmas on 3/1/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AwardUploadDelegate <NSObject>
-(void)closeAwardForm;
@end

@interface AwardUploadViewController : UIViewController

@property (strong,nonatomic)  id<AwardUploadDelegate> delegate;

@property(strong,nonatomic) UIImageView* backgroundView;
@property (strong,nonatomic)  NSMutableData *responseData;
@property (strong,nonatomic) NSURLConnection* pointsConnection;

@property(nonatomic,strong) UIImageView* topImageView;
@property(nonatomic,strong) UILabel* plus_coins;
@property(nonatomic,strong) UILabel* user_coins;

@property(nonatomic,strong) UIImageView* bottomImageView;
@property(nonatomic,strong) UILabel* offer_coins;
@property(nonatomic,strong) UILabel* wifi_access;
@property(nonatomic,strong) NSString* wifi;

-(id)init:(id<AwardUploadDelegate>) delegate wifi:(NSString*)wifi;
@end
