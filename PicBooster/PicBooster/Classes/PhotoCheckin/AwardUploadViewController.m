//
//  AwardUploadViewController.m
//  Picbooster
//
//  Created by Vassilis Dourmas on 3/1/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import "AwardUploadViewController.h"
#import "AppDelegate.h"
#import "Consts.h"

@interface AwardUploadViewController ()

@end

@implementation AwardUploadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)init:(id<AwardUploadDelegate>) delegate wifi:(NSString*)wifi
{
    self=[super init];
    if(self)
    {
        self.delegate = delegate;
        self.wifi = wifi;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.responseData = [[NSMutableData alloc]init];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        self.backgroundView = [[UIImageView alloc]initWithFrame:self.view.frame];
        self.backgroundView.image = [UIImage imageNamed:@"woohoo screen.png"];
        self.backgroundView.userInteractionEnabled = YES;
        self.backgroundView.contentMode = UIViewContentModeTop;
        [self.view addSubview:self.backgroundView];
        
        self.plus_coins = [[UILabel alloc]initWithFrame:CGRectMake(150, 20, 50, 32)];
        self.plus_coins.backgroundColor = [UIColor clearColor];
        self.plus_coins.textColor = [UIColor whiteColor];
        [self.plus_coins setTextAlignment:NSTextAlignmentCenter];
        self.plus_coins.font = [UIFont boldSystemFontOfSize:25.0f];
        [self.backgroundView addSubview:self.plus_coins];

        
        self.user_coins = [[UILabel alloc]initWithFrame:CGRectMake(331, 20, 185, 32)];
        self.user_coins.backgroundColor = [UIColor clearColor];
        self.user_coins.textColor = [UIColor whiteColor];
        [self.user_coins setTextAlignment:NSTextAlignmentCenter];
        [self.backgroundView addSubview:self.user_coins];
        
        UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(560, 10, 70, 70);
        backButton.backgroundColor = [UIColor clearColor];
        [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [self.backgroundView addSubview:backButton];
        
        self.offer_coins= [[UILabel alloc]initWithFrame:CGRectMake(380, 485, 100, 40)];
        self.offer_coins.textColor = [UIColor whiteColor];
        self.offer_coins.font = [UIFont boldSystemFontOfSize:30];
        self.offer_coins.backgroundColor = [UIColor clearColor];
        [self.backgroundView addSubview:self.offer_coins];
        
        self.wifi_access= [[UILabel alloc]initWithFrame:CGRectMake(350, 590, 310, 40)];
        [self.backgroundView addSubview:self.wifi_access];
        self.wifi_access.textColor = [UIColor whiteColor];
        self.wifi_access.backgroundColor = [UIColor clearColor];
        self.wifi_access.font = [UIFont systemFontOfSize:30];
        [self.wifi_access setTextAlignment:NSTextAlignmentLeft];
        [self refreshPoints:[AppDelegate getFbUserId]];
        
        [self.view addSubview:self.topImageView];
        [self.view addSubview:self.bottomImageView];
    }
    else{
      self.backgroundView = [[UIImageView alloc]initWithFrame:self.view.frame];
      self.backgroundView.image = [UIImage imageNamed:@"woohoo screen.png"];
      self.backgroundView.userInteractionEnabled = YES;
      self.backgroundView.contentMode = UIViewContentModeScaleAspectFit;
      [self.view addSubview:self.backgroundView];
    
        
      self.plus_coins = [[UILabel alloc]initWithFrame:CGRectMake(15, 58, 50, 32)];
      if(self.view.frame.size.height > 480)
            self.plus_coins.frame = CGRectMake(15, 102, 50, 32);
      self.plus_coins.backgroundColor = [UIColor clearColor];
      self.plus_coins.textColor = [UIColor whiteColor];
      [self.plus_coins setTextAlignment:NSTextAlignmentCenter];
      self.plus_coins.font = [UIFont boldSystemFontOfSize:16.0f];
      [self.backgroundView addSubview:self.plus_coins];

      self.user_coins = [[UILabel alloc]initWithFrame:CGRectMake(154, 58, 100, 32)];
      if(self.view.frame.size.height > 480)
            self.user_coins.frame = CGRectMake(154, 102, 100, 32);
      self.user_coins.backgroundColor = [UIColor clearColor];
      self.user_coins.textColor = [UIColor whiteColor];
      [self.user_coins setTextAlignment:NSTextAlignmentLeft];
      [self.backgroundView addSubview:self.user_coins];
    
      UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
      backButton.frame = CGRectMake(280, 55, 30, 30);
      if(self.view.frame.size.height > 480)
            backButton.frame = CGRectMake(280, 100, 30, 30);

      backButton.backgroundColor = [UIColor clearColor];
      [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
      [self.backgroundView addSubview:backButton];
    
      self.offer_coins= [[UILabel alloc]initWithFrame:CGRectMake(160, 328, 40, 30)];
      if(self.view.frame.size.height > 480)
            self.offer_coins.frame = CGRectMake(160, 372, 40, 30);
      [self.backgroundView addSubview:self.offer_coins];
      self.offer_coins.textColor = [UIColor whiteColor];
      self.offer_coins.font = [UIFont boldSystemFontOfSize:18];
      self.offer_coins.backgroundColor = [UIColor clearColor];
    
      self.wifi_access= [[UILabel alloc]initWithFrame:CGRectMake(140, 385, 160, 40)];
      if(self.view.frame.size.height > 480)
         self.wifi_access.frame = CGRectMake(140, 429, 160, 40);
      self.wifi_access.textColor = [UIColor whiteColor];
      self.wifi_access.backgroundColor = [UIColor clearColor];
      [self.backgroundView addSubview:self.wifi_access];

    
      [self refreshPoints:[AppDelegate getFbUserId]];
    
      [self.view addSubview:self.topImageView];
      [self.view addSubview:self.bottomImageView];
    }
    
    self.wifi_access.text = self.wifi;
}

-(void) refreshPoints:(NSString *)fbUserId{
    NSString *urlString= [NSString stringWithFormat:getUserPoints,fbUserId];
    NSMutableURLRequest *pointsRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
    [pointsRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [pointsRequest setTimeoutInterval:20.0];
    self.pointsConnection=[[NSURLConnection alloc] initWithRequest:pointsRequest delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}



-(void)back
{
    [AppDelegate getAppDelegate].refreshHome = YES;
    [self.delegate closeAwardForm];
    
}


//NSURLConnection methods following
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Connection Error!"
												   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
	[alert show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *myResponseString=[[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    SBJsonParser *json = [[SBJsonParser alloc] init] ;
    if ([connection isEqual:self.pointsConnection]){
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSDictionary *points =[json objectWithString:myResponseString];
        [AppDelegate getAppDelegate].currentUserPointsString =[points valueForKey:@"points"];
        //self.user_coins.text = [AppDelegate getAppDelegate].currentUserPointsString;
               NSString* currentUserPointsString = [AppDelegate getAppDelegate].currentUserPointsString;
                NSString* pointsString = @"";
                if ([currentUserPointsString isEqualToString:@"0"] ) {
                    pointsString = @"00000";
                }
                else if ([currentUserPointsString integerValue]<100) {
                    pointsString =[NSString stringWithFormat:@"000%@",currentUserPointsString];
                }
                else if ([currentUserPointsString integerValue]<1000) {
                    pointsString =[NSString stringWithFormat:@"00%@",currentUserPointsString];
                }
                else if ([currentUserPointsString integerValue]<10000) {
                    pointsString =[NSString stringWithFormat:@"0%@",currentUserPointsString];
                }
                [self.user_coins setText:pointsString];
                
                //self.pointsLabel.text = pointsString;
        
              NSString *deviceType = [UIDevice currentDevice].model;
             if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            
                NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:pointsString attributes: @{
                                                                                                                             NSFontAttributeName : [UIFont boldSystemFontOfSize:30.0f],
                                                                                                                             NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                                             NSKernAttributeName : @(4.5f)
                                                                                                                             }];
                
                self.user_coins.text=@"";
                self.user_coins.attributedText =attributedString;
                self.plus_coins.text  = @"10";
                self.offer_coins.text = @"10";
             }
             else
             {
                 NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:pointsString attributes: @{
                                                                                                                              NSFontAttributeName : [UIFont systemFontOfSize:18.0f],
                                                                                                                              NSForegroundColorAttributeName : [UIColor whiteColor],
                                                                                                                              NSKernAttributeName : @(1.0f)
                                                                                                                              }];
                 
                 self.user_coins.text=@"";
                 self.user_coins.attributedText =attributedString;
                 self.plus_coins.text  = @"10";
                 self.offer_coins.text = @"10";
             }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
