//
//  CommentBoxView.m
//  Picbooster
//
//  Created by George Bafaloukas on 6/25/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "CommentBoxView.h"

@implementation CommentBoxView
@synthesize delegate,mainView;
@synthesize passwordTextField;
@synthesize finalImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code
        CGRect passwordTextFieldFrame = CGRectMake(0,0,100.0f,100.0f);
        passwordTextField = [[UITextView alloc] initWithFrame:passwordTextFieldFrame];
        
        //passwordTextField.placeholder = @"Comment";
        passwordTextField.backgroundColor = [UIColor clearColor];
        passwordTextField.textColor = [UIColor blackColor];
        passwordTextField.font = [UIFont systemFontOfSize:14.0f];
        //passwordTextField.borderStyle = UITextBorderStyleRoundedRect;
        //passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        passwordTextField.returnKeyType = UIReturnKeyDone;
        passwordTextField.textAlignment = NSTextAlignmentLeft;
        //passwordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        passwordTextField.tag = 2;
        passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        passwordTextField.delegate = self;
        passwordTextField.editable=YES;

    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 500.0f)];
    bgImage.image = [UIImage imageNamed:@"add caption screen-withOK.png"];
    // center the image as it becomes smaller than the size of the screen
    CGSize boundsSize = self.view.bounds.size;
    CGRect frameToCenter = bgImage.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = ((boundsSize.height - frameToCenter.size.height) / 2);//- 80.0f;
    else
        frameToCenter.origin.y = 0;
    
    bgImage.frame = frameToCenter;
    [self.view addSubview:bgImage];
    
    CGRect passwordTextFieldFrame = CGRectMake(bgImage.frame.origin.x + 10.0f,bgImage.frame.origin.y +55.0f, 180.0f, 105.0f);
    self.passwordTextField.frame = passwordTextFieldFrame;
    
    
    [self.view addSubview:passwordTextField];
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 addTarget:self
                action:@selector(aMethod:)
      forControlEvents:UIControlEventTouchDown];
    button1.tag=1;
    //[button1 setTitle:@"Post" forState:UIControlStateNormal];
    [button1 setBackgroundColor:[UIColor clearColor]];
    button1.frame = CGRectMake(250,0,100,70);
    [self.view addSubview:button1];
//
//    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button2 addTarget:self
//                action:@selector(aMethod:)
//      forControlEvents:UIControlEventTouchDown];
//    button2.tag=2;
//    //[button2 setTitle:@"Cancel" forState:UIControlStateNormal];
//    [button1 setBackgroundColor:[UIColor clearColor]];
//    button2.frame = CGRectMake(bgImage.frame.origin.x ,bgImage.frame.origin.y , 65.0f, 30.0f);
//    [self.view addSubview:button2];
//    
    UIImageView *myImage = [[UIImageView alloc] initWithFrame:CGRectMake(bgImage.frame.origin.x + 197.0f,bgImage.frame.origin.y + 47.0f, 117.0f, 117.0f)];
    
    myImage.image = self.finalImage;
    
    [self.view addSubview:myImage];
    
}
-(void)aMethod:(id)sender{
    UIButton *btn = (UIButton*)sender;
    NSLog(@"%i",btn.tag);
    if (btn.tag == 1) {
        [[self delegate] photoCaption:passwordTextField.text];
    }else if(btn.tag == 2) {
        [[self delegate] photoCaption:@"CANCEL"];
    }
}
- (BOOL)textViewShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    textField.backgroundColor = [UIColor colorWithRed:220.0f/255.0f green:220.0f/255.0f blue:220.0f/255.0f alpha:1.0f];
    return YES;
}
- (void)textViewDidBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing");
     textField.backgroundColor = [UIColor clearColor];
}
- (BOOL)textViewShouldEndEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldEndEditing");
    textField.backgroundColor = [UIColor clearColor];
    return YES;
}
- (void)textViewDidEndEditing:(UITextField *)textField{
    NSLog(@"textFieldDidEndEditing");
     textField.backgroundColor = [UIColor clearColor];
}
- (BOOL)textViewShouldClear:(UITextField *)textField{
    NSLog(@"textFieldShouldClear:");
     textField.backgroundColor = [UIColor clearColor];
    return YES;
}
- (BOOL)textViewShouldReturn:(UITextField *)textField{
    NSLog(@"textFieldShouldReturn:");
    if (textField.tag == 1) {
        passwordTextField = (UITextView *)[self.view viewWithTag:2];
        [passwordTextField becomeFirstResponder];
    }
    else {
        [textField resignFirstResponder];
    }
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end
