//
//  AwardUploadView.h
//  Picbooster
//
//  Created by Vassilis Dourmas on 3/1/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AwardUploadView : UIView

@property (strong,nonatomic)  NSMutableData *responseData;
@property (strong,nonatomic) NSURLConnection* pointsConnection;

@property(nonatomic,strong) UIImageView* topImageView;
@property(nonatomic,strong) UILabel* user_coins;

@property(nonatomic,strong) UIImageView* bottomImageView;
@property(nonatomic,strong) UILabel* offer_coins;
@property(nonatomic,strong) UILabel* wifi_access;

@end
