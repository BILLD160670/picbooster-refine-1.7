//
//  ShareView.h
//  Picbooster
//
//  Created by George Bafaloukas on 10/29/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol shareButtonPressed

-(void)shareButtonPressed:(NSString*) button;

@end
@interface ShareView : UIView{
    id <shareButtonPressed> delegate;
}
@property (nonatomic,strong) id delegate;
@end
