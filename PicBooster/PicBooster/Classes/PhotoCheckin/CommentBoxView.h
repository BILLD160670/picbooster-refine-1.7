//
//  CommentBoxView.h
//  Picbooster
//
//  Created by George Bafaloukas on 6/25/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CommentBoxDelegate <NSObject>
-(void)photoCaption:(NSString *)text;
@end
@interface CommentBoxView : UIViewController <UITextViewDelegate> {
    UIView *mainView;
    id <CommentBoxDelegate> delegate;
    UITextView *passwordTextField;
}
@property (nonatomic,strong) UIImage *finalImage;
@property (nonatomic,strong) UITextView *passwordTextField;
@property (nonatomic,strong) id delegate;
@property (strong,nonatomic) UIView *mainView;
@end