//
//  AwardUploadView.m
//  Picbooster
//
//  Created by Vassilis Dourmas on 3/1/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import "AwardUploadView.h"
#import "Consts.h"
#import "AppDelegate.h"

@implementation AwardUploadView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.responseData = [[NSData alloc]init];
        
        self.topImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320,240)];
        [self addSubview:self.topImageView];
        self.user_coins = [[UILabel alloc]initWithFrame:CGRectMake(20, 100, 100, 40)];
        [self.topImageView addSubview:self.user_coins];
        
        self.bottomImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320,240)];
        self.offer_coins= [[UILabel alloc]initWithFrame:CGRectMake(80, 100, 100, 40)];
        [self.bottomImageView addSubview:self.offer_coins];
        
        self.wifi_access= [[UILabel alloc]initWithFrame:CGRectMake(180, 100, 100, 40)];
        [self.bottomImageView addSubview:self.wifi_access];
        
        [self refreshPoints:[AppDelegate getFbUserId]];
    }
    return self;
}

-(void) refreshPoints:(NSString *)fbUserId{
    NSString *urlString= [NSString stringWithFormat:getUserPoints,fbUserId];
    NSMutableURLRequest *pointsRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
    [pointsRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [pointsRequest setTimeoutInterval:20.0];
    self.pointsConnection=[[NSURLConnection alloc] initWithRequest:pointsRequest delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}


//NSURLConnection methods following
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Connection Error!"
												   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
	[alert show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *myResponseString=[[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    SBJsonParser *json = [[SBJsonParser alloc] init] ;
    if ([connection isEqual:self.pointsConnection]){
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSDictionary *points =[json objectWithString:myResponseString];
        [AppDelegate getAppDelegate].currentUserPointsString =[points valueForKey:@"points"];
        self.user_coins.text = [AppDelegate getAppDelegate].currentUserPointsString;
    }
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
