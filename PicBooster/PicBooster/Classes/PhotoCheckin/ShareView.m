//
//  ShareView.m
//  Picbooster
//
//  Created by George Bafaloukas on 10/29/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "ShareView.h"
@interface ShareView ()
@property (nonatomic, strong) UIView *mainView;
@end
@implementation ShareView
@synthesize mainView;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 addTarget:self
                    action:@selector(menuButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        [button1 setBackgroundImage:[UIImage imageNamed:@"facebookbuttonpressed.png"] forState:UIControlStateNormal];
        [button1 setBackgroundImage:[UIImage imageNamed:@"facebookbuttonpressed.png"] forState:UIControlStateSelected];
        button1.tag=1;
        button1.backgroundColor=[UIColor clearColor];
        UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button2 addTarget:self
                    action:@selector(menuButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        [button2 setBackgroundImage:[UIImage imageNamed:@"emailbutton.png"] forState:UIControlStateNormal];
        [button2 setBackgroundImage:[UIImage imageNamed:@"emailbutton.png"] forState:UIControlStateSelected];
        button2.tag=2;
        button2.backgroundColor=[UIColor clearColor];
        
        UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button3 addTarget:self
                    action:@selector(menuButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        [button3 setBackgroundImage:[UIImage imageNamed:@"instagrambutton.png"] forState:UIControlStateNormal];
        [button3 setBackgroundImage:[UIImage imageNamed:@"instagrambutton.png"] forState:UIControlStateSelected];
        button3.tag=3;
        button3.backgroundColor=[UIColor clearColor];
        
        UIButton *button4 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button4 addTarget:self
                    action:@selector(menuButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        button4.tag=4;
        [button4 setBackgroundImage:[UIImage imageNamed:@"twitterbutton.png"] forState:UIControlStateNormal];
        [button4 setBackgroundImage:[UIImage imageNamed:@"twitterbutton.png"] forState:UIControlStateSelected];
        button4.backgroundColor=[UIColor clearColor];

    
        button1.frame = CGRectMake(0.0f, 0.0f, 160.0f, 42.0f);
        button2.frame = CGRectMake(0.0f, 42.0f, 160.0f, 42.0f);
        button3.frame = CGRectMake(160.0f, 0.0f, 160.0f, 42.0f);
        button4.frame = CGRectMake(160.0f, 42.0f, 160.0f, 42.0f);
        
        [self addSubview:button1];
        [self addSubview:button2];
        [self addSubview:button3];
        [self addSubview:button4];
        
    }
    return self;
}
-(void)menuButtonPressed:(id)sender{
    UIButton* buttonPressed = sender;
    if(buttonPressed.tag==1){
        NSLog(@"Push Explore view controller");
        [delegate shareButtonPressed:@"Facebook"];
    }else if (buttonPressed.tag==2){
        NSLog(@"Refresh Grid View");
        [delegate shareButtonPressed:@"Mail"];
    }else if (buttonPressed.tag==3){
        NSLog(@"Open settings view");
        [delegate shareButtonPressed:@"Instagram"];
    }else if (buttonPressed.tag==4){
        NSLog(@"Open settings view");
        [delegate shareButtonPressed:@"Twitter"];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
