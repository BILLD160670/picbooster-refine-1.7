//
//  AppDelegate.m
//  Picbooster
//
//  Created by George Bafaloukas on 4/3/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "AppDelegate.h"
#import "Consts.h"

@implementation AppDelegate

@synthesize homeViewController;
@synthesize navController;
@synthesize loginViewController;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize imageCache;
@synthesize settingsView;

@synthesize selectedFunction;
@synthesize selectedLevel;



+(AppDelegate*)getAppDelegate
{
    return ((AppDelegate*)([UIApplication sharedApplication].delegate));
}


+(NSString*)getFbUserId
{
    return ((AppDelegate*)([UIApplication sharedApplication].delegate)).fbUserId;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.homeViewController = [[HomeViewController alloc] init];
    
    self.loginViewController =[[LoginViewController alloc] init];
    
    self.imageCache = [[NSCache alloc] init];
    self.fbUserId = @"";
    self.currentUserPointsString = @"0";
    
    NSString *deviceType = [UIDevice currentDevice].model;
    CGFloat width  = 320.0f;
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        width = 768;
    }
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
       self.settingsView = [[SettingsView alloc]initWithFrame:CGRectMake(0.0f, 45.0f, width, 0.0f)];
    }
    else
    {
        self.settingsView = [[SettingsView alloc]initWithFrame:CGRectMake(0.0f, 65.0f, width, 0.0f)];
    }
    
    
    // See if we have a valid token for the current state.
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
        // To-do, show logged in view
         self.navController = [[UINavigationController alloc] initWithRootViewController:self.homeViewController];
    } else {
        // No, display the login page.
         self.navController = [[UINavigationController alloc] initWithRootViewController:self.loginViewController];
    }

    [[UIBarButtonItem appearance] setTintColor:[UIColor grayColor]];
    
    self.window.rootViewController = self.navController;
    
    [self.window makeKeyAndVisible];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    [self listFileAtPath:documentsDirectory];
    
    // Let the device know we want to receive push notifications
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
    
    selectedFunction = 0;
    selectedLevel = 0;
    
    return YES;
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"Did register for remote notifications: %@", deviceToken);
    NSString *str = [NSString
					 stringWithFormat:@"%@",deviceToken];
    // NSLog(str);
	NSString *Newdev=str;
	Newdev = [Newdev stringByReplacingOccurrencesOfString:@"<" withString:@""];
	Newdev = [Newdev stringByReplacingOccurrencesOfString:@">" withString:@""];
	Newdev = [Newdev stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *ulrString=[NSString stringWithFormat:@"http://www.picbooster.net/api/getDeviceId.php?device_id=%@",Newdev];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSError *error = nil;
                NSHTTPURLResponse *response = nil;
                NSURLRequest *request = [NSURLRequest
                                         requestWithURL:[NSURL URLWithString:ulrString]
                                         cachePolicy:NSURLRequestReloadIgnoringCacheData
                                         timeoutInterval:20.0];
                NSData *conn = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
                NSLog(@"%@",conn);

            });
    });
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}
- (BOOL)isPhotoForDelete:(NSDate *)checkEndDate
{
    NSDate* enddate = checkEndDate;
    NSDate* currentdate = [NSDate date];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates < -864000) {
        return YES;
    }else{
        return NO;
    }
}
-(NSArray *)listFileAtPath:(NSString *)path
{
    //-----> LIST ALL FILES <-----//
    NSLog(@"LISTING ALL FILES FOUND");
    
    int count;
    
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:NULL];
    for (count = 0; count < (int)[directoryContent count]; count++)
    {
        NSLog(@"File %d: %@", (count + 1), [directoryContent objectAtIndex:count]);
        NSDictionary *fileProperties = [[NSFileManager defaultManager] attributesOfItemAtPath:[NSString stringWithFormat:@"%@/%@",path,[directoryContent objectAtIndex:count]] error:NULL];
       
        if ([self isPhotoForDelete:[fileProperties valueForKey:@"NSFileCreationDate"]]) {
            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@",path,[directoryContent objectAtIndex:count]] error:NULL];
        }
    }
    return directoryContent;
}
- (int) indexOf:(NSString *)text inString:(NSString *)orig {
    NSRange range = [orig rangeOfString:text];
    if ( range.length > 0 ) {
        return range.location;
    } else {
        return -1;
    }
}

-(UIImage *)returnCahedImageWithName:(NSString *)url{
    UIImage *myImage;
    if (![url isEqualToString:HOME] && [url isKindOfClass:[NSString class]]) {
        NSString *imageLocal=[url substringFromIndex:37];
        
        int index = [self indexOf:@"/" inString:imageLocal];
        
        if (index!=-1) {
            imageLocal=[imageLocal substringFromIndex:index+1];
        }
        while ([self indexOf:@"/" inString:imageLocal]!=-1) {
            index = [self indexOf:@"/" inString:imageLocal];
            imageLocal=[imageLocal substringFromIndex:index+1];
        }
        
        UIImage *cellImage=[self.imageCache objectForKey:imageLocal];
        if (cellImage) {
            myImage = cellImage;
        }else{
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            //
            //Read the image from the disk
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[documentsDirectory stringByAppendingPathComponent:imageLocal]];
            NSLog(@"local image url: %@",imageLocal);
            //checking if we have alocal copy of the offer image. If we do we read it from the local filesystem.
            //If we dont we read it from the web and we create a local copy in order to have it localy for next time.
            if(fileExists==NO){
                NSLog(@"WEB");
                //Read the image from the web
                NSData *receivedData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                myImage = [[UIImage alloc] initWithData:receivedData] ;
                // Do the write
                NSLog(@"local image url: %@",imageLocal);
                documentsDirectory = [documentsDirectory stringByAppendingPathComponent:imageLocal];
                if (myImage) {
                    [receivedData writeToFile:documentsDirectory atomically:YES];
                    [self.imageCache setObject:myImage forKey:imageLocal];
                }
            }else{
                NSLog(@"LOCAL");
                myImage = [[UIImage alloc] initWithContentsOfFile:[documentsDirectory stringByAppendingPathComponent:imageLocal]];
                if (myImage) {
                    [self.imageCache setObject:myImage forKey:imageLocal];
                }
            
            }
        }
    }
    return myImage;
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Picbooster" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Picbooster.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
