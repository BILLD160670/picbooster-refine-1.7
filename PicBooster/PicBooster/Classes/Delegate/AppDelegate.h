//
//  AppDelegate.h
//  Picbooster
//
//  Created by George Bafaloukas on 4/3/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "LoginViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "SettingsView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    HomeViewController * homeViewController;
    LoginViewController * loginViewController;
    UINavigationController *navController;
    NSCache *imageCache;
    
    int selectedFunction;//0:venues,1:brands,2:emotions
    int selectedLevel;//venues have 1 level,brands have 2 levels,emotions have 2 levels
    
    
}
@property (strong, nonatomic) NSCache *imageCache;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) HomeViewController * homeViewController;
@property (strong, nonatomic) LoginViewController * loginViewController;
@property (nonatomic, strong) UINavigationController *navController;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(nonatomic,strong) SettingsView* settingsView;

@property(nonatomic, assign) int selectedFunction;
@property(nonatomic, assign) int selectedLevel;

@property (strong, nonatomic) NSString* fbUserId;
@property (nonatomic, strong) NSString* currentUserPointsString;

@property (nonatomic,assign) BOOL refreshHome;

+(AppDelegate*)getAppDelegate;
+(NSString*)getFbUserId;
-(void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

-(UIImage *)returnCahedImageWithName:(NSString *)url;

@end
