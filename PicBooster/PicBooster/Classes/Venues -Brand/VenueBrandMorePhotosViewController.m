//
//  BrandVenueMorePhotosViewController.m
//  Picbooster
//
//  Created by Vassilis Dourmas on 3/3/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import "VenueBrandMorePhotosViewController.h"
#import "Consts.h"
#import "AQGridView.h"
#import "AppDelegate.h"
#import "GridDetailViewController.h"

@interface VenueBrandMorePhotosViewController ()

@end

@implementation VenueBrandMorePhotosViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
  
    [self createCustomNavigationBar:self];
    self.responseData = [[NSMutableData alloc]init];
    self.view.backgroundColor=[UIColor whiteColor];

    NSString *deviceType = [UIDevice currentDevice].model;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        
    if (self.view.frame.size.height>480) {
      self.gridView = [[AQGridView alloc] initWithFrame:CGRectMake(0.0f, 45.0f, 320.0f, 568.0f -45.0f)];
    }else{
            self.gridView = [[AQGridView alloc] initWithFrame:CGRectMake(0.0f, 45.0f, 320.0f, 480.0f-45.0f)];
         }
         if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            //iPad
            self.gridView.frame = CGRectMake(64.0f, 45.0f, 640.0f, 1024.0f - 45.0f);
            //myMenuViewIos.frame=CGRectMake(0.0f, self.view.frame.size.height - 207.0f, 768.0f, 207.0f);
        }
        
    }
    else
    {
        if (self.view.frame.size.height>480) {
            self.gridView = [[AQGridView alloc] initWithFrame:CGRectMake(0.0f, 65.0f, 320.0f, 568.0f -45.0f)];
        }else{
            self.gridView = [[AQGridView alloc] initWithFrame:CGRectMake(0.0f, 65.0f, 320.0f, 480.0f-45.0f)];
        }
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            //iPad
            self.gridView.frame = CGRectMake(64.0f, 65.0f, 640.0f, 1024.0f - 65.0f);
            // myMenuViewIos.frame=CGRectMake(0.0f, self.view.frame.size.height - 207.0f, 768.0f, 207.0f);
        }

    }
    
    
    self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    self.gridView.autoresizesSubviews = YES;
    self.gridView.delegate = self;
    self.gridView.dataSource = self;
    [self.view addSubview:self.gridView];
    self.GridCount=15;
    
    //grid request
    NSMutableURLRequest *gridRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat: getPhotosOfVenue,self.venueID]]] ;
    [gridRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [gridRequest setTimeoutInterval:20.0];
    self.gridConnection=[[NSURLConnection alloc] initWithRequest:gridRequest delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    


}

-(void)createCustomNavigationBar:(id<CustomNavigationBarDelegate>)delegate

{
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 45.0f) displayPoints:false displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 45) displayPoints:false displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
    }
    else
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 65.0f) displayPoints:false displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 65) displayPoints:false displayPicboosterTitleView:true title:nil displaySettingsButton:true titleImageView:nil];
        }
    }
    
    self.customNavBar.delegate = self;
    [self.view addSubview:self.customNavBar];
}


//customNavigationBar actions
-(void)barButtonPressed:(NSString *)button{
    if([button isEqualToString:@"Back"]){
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if([button isEqualToString:@"Settings"]){
        [self settingsClicked];
    }
}


-(void)settingsClicked{
    
    SettingsView *mySettingsView = [AppDelegate getAppDelegate].settingsView;
    if(mySettingsView.settingsIsVisible)
    {
        [mySettingsView hideSettings];
    }
    else{
        
        NSString *deviceType = [UIDevice currentDevice].model;
        CGFloat finalHeight  = 330;
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            finalHeight = 330;
        }
        [self.view addSubview:mySettingsView];
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: UIViewAnimationOptionTransitionNone
                         animations:^{
                             mySettingsView.frame = CGRectMake(0.0f , mySettingsView.frame.origin.y , mySettingsView.frame.size.width , finalHeight);
                             mySettingsView.settingsIsVisible = true;
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                         }];
    }
}

- (CGSize) portraitGridCellSizeForGridView: (AQGridView *) aGridView
{
    return ( CGSizeMake(160.0, 160) );
}

- (NSUInteger) numberOfItemsInGridView: (AQGridView *) aGridView
{
    return self.GridCount;
}

- (AQGridViewCell *) gridView: (AQGridView *) aGridView cellForItemAtIndex: (NSUInteger) index
{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    static NSString * PlainCellIdentifier = @"PlainCellIdentifier";
    NSString *gridCell = [self.services objectAtIndex:index];
    GridViewCell * cell = (GridViewCell *)[aGridView dequeueReusableCellWithIdentifier:@"PlainCellIdentifier"];
    if ( cell == nil )
    {
        cell = [[GridViewCell alloc] initWithFrame: CGRectMake(0.0, 0.0, 160, 160)
                                   reuseIdentifier: PlainCellIdentifier];
    }
    cell.backgroundColor=[UIColor clearColor];
    cell.tag = index;
    //show placeholder image (or throbber) to handle slow loading of new images and replication effect
    [cell.imageViewRotator setImage:[UIImage imageNamed:@"rotator.png"]];
    cell.imageView.hidden = YES;
    cell.imageViewRotator.hidden = NO;
    /*
     "lazy load" images using GCD
     store each image in disk for offline load or next launch of the app
     store images in an NSCache object for fast loading
     */
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        NSString *urlString=[NSString stringWithFormat:APIUrl,gridCell];
        //use of caching delegate method
        NSLog(urlString);
        UIImage *cellImage=[delegate returnCahedImageWithName:urlString];
        if (cellImage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cell.tag == index) {
                    cell.imageView.image = cellImage;
                    cell.imageView.hidden = NO;
                    cell.imageViewRotator.hidden = YES;
                    [cell setNeedsLayout];
                }
            });
        }
    });
    if (index == self.GridCount-1) {
        [self performSelector:@selector(loadMore) withObject:nil afterDelay:2];
    }
    
    return cell;
}

//-(void)gridView:(AQGridView *)gridView didSelectItemAtIndex:(NSUInteger)index
//{
//    if([[AppDelegate getFbUserId] isEqualToString:@""])
//    {
//        return;
//    }
//    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
//    NSDictionary *gridCell = [self.services objectAtIndex:index];
//    GridDetailViewController *detail=[[GridDetailViewController alloc] init];
//    NSString *urlString=[NSString stringWithFormat:APIUrl,[gridCell valueForKey:@"hash"]];
//    NSString *venueID=[gridCell valueForKey:@"venue_id"];
//    UIImage *detailImage=[delegate returnCahedImageWithName:urlString];
//    detail.imageUrl=[gridCell valueForKey:@"hash"];
//    detail.venueID=venueID;
//    NSString *deviceType = [UIDevice currentDevice].model;
//    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
//        detail.imageView = [[UIImageView alloc] initWithImage:[detailImage scaleToSize:CGSizeMake(600.0f, 600.0f)]];
//        detail.imageView.frame = CGRectMake(10.0, 30.0, 600.0f, 600.0f);
//    }else{
//        detail.imageView = [[UIImageView alloc] initWithImage:[detailImage scaleToSize:CGSizeMake(300.0f, 300.0f)]];
//        detail.imageView.frame = CGRectMake(10.0, 30.0, 300.0f, 300.0f);
//    }
//    [self.navigationController pushViewController:detail animated:NO];
//}


-(void)loadMore{
    if (self.services.count>self.GridCount+15) {
        self.GridCount = self.GridCount+15;
    }else {
        self.GridCount = self.services.count;
    }
    [self.gridView reloadData];
}


//NSURLConnection methods following
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [self.hud hide:YES];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Connection Error!"
												   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
	[alert show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSString *myResponseString=[[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    SBJsonParser *json = [[SBJsonParser alloc] init] ;
    if ([connection isEqual:self.gridConnection]) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        self.services = [json objectWithString:myResponseString];
        [self.hud hide:YES];
        [self.gridView reloadData];
        self.gridConnection=nil;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
