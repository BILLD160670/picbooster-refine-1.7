//
//  GridFooterView.h
//  Picbooster
//
//  Created by Vassilis Dourmas on 16/02/14.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol VenuesBrandsSelectionDelegate

-(void)selectButtonPressed:(NSString*) button;

@end
@interface VenuesBrandsSelectionMenuView : UIView{
    id <VenuesBrandsSelectionDelegate> delegate;
}
@property (nonatomic,strong) id delegate;
@property (nonatomic,strong) UIButton* buttonVenues;
@property (nonatomic,strong) UIButton* buttonBrands;
@property (nonatomic,strong) UIButton* buttonEmotions;



-(void)selectVenues;
-(void)selectBrands;
-(void)selectEmotions;
@end