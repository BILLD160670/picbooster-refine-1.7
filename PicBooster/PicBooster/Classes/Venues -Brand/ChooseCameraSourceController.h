//
//  AwardUploadViewController.h
//  Picbooster
//
//  Created by Vassilis Dourmas on 3/1/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "PopUpViewController.h"
#import "AFPhotoEditorController.h"
#import "AFPhotoEditorCustomization.h"

@protocol ChooseCameraSourceDelegate <NSObject>
-(void)closeChooseCameraSourceForm;
@end

@interface ChooseCameraSourceController : UIViewController<AFPhotoEditorControllerDelegate,UIImagePickerControllerDelegate>

@property (strong,nonatomic)  id<ChooseCameraSourceDelegate> delegate;

@property(strong,nonatomic) UIImageView* backgroundView;

@property(nonatomic,strong) UIButton *closeButton;
@property(nonatomic,strong) UIButton *cameraButton;
@property(nonatomic,strong) UIButton *libraryButton;
@property (strong,nonatomic) UIPopoverController *popoverController;

@property (strong,nonatomic) NSString *emotionId;

@property (strong,nonatomic) NSString *imageURL;

@property (strong,nonatomic) UIViewController *prevController;

-(id)init:(id<ChooseCameraSourceDelegate>) delegate emotionId:(NSString*)emotionId imageUrl:(NSString *)imageUrl prevController:(UIViewController*) prevController;
@end
