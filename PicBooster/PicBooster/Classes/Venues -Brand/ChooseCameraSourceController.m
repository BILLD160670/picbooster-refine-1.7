//
//  AwardUploadViewController.m
//  Picbooster
//
//  Created by Vassilis Dourmas on 3/1/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import "ChooseCameraSourceController.h"
#import "AppDelegate.h"
#import "Consts.h"
#import "UIImage+Scale.h"
#import "PhotoViewController.h"

@interface ChooseCameraSourceController ()

@end

@implementation ChooseCameraSourceController

@synthesize popoverController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}

-(id)init:(id<ChooseCameraSourceDelegate>) delegate emotionId:(NSString*)emotionId imageUrl:(NSString *)imageUrl prevController:(UIViewController*) prevController
{
    self=[super init];
    if(self)
    {
        self.delegate = delegate;
        self.emotionId = emotionId;
        self.imageURL =imageUrl;
        self.prevController = prevController;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        
        //349x219
        self.backgroundView = [[UIImageView alloc]initWithFrame:CGRectMake((320-175)/2, 100, 175, 110)];
        self.backgroundView.image = [UIImage imageNamed:@"choose photo back.png"];
        self.backgroundView.userInteractionEnabled = YES;
        self.backgroundView.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:self.backgroundView];
        
        self.closeButton = [[UIButton alloc]initWithFrame:CGRectMake(self.backgroundView.frame.origin.x + self.backgroundView.frame.size.width - 20  , self.backgroundView.frame.origin.y-10, 30, 30)];
        self.closeButton.backgroundColor = [UIColor clearColor];
        [self.closeButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.closeButton];
        
        //312x73
        self.cameraButton = [[UIButton alloc]initWithFrame:CGRectMake(self.backgroundView.frame.origin.x +  ((175 - 156)/2),
                                                                      self.backgroundView.frame.origin.y  + 20,
                                                                      156 ,37 )];
        [self.cameraButton setBackgroundImage:[UIImage imageNamed:@"take a photo button.png"] forState:UIControlStateNormal];
        [self.cameraButton addTarget:self action:@selector(cameraPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.cameraButton];
        
        self.libraryButton = [[UIButton alloc]initWithFrame:CGRectMake(self.backgroundView.frame.origin.x + ((175 - 156)/2),
                                                                       self.backgroundView.frame.origin.y  + 25 + 37,
                                                                       156 , 37)];
        [self.libraryButton setBackgroundImage:[UIImage imageNamed:@"choose from library.png"] forState:UIControlStateNormal];
        [self.libraryButton addTarget:self action:@selector(libraryPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:self.libraryButton];

        
    }
    else{
      self.view.backgroundColor = [UIColor clearColor];
        
        
      //349x219
      self.backgroundView = [[UIImageView alloc]initWithFrame:CGRectMake((320-175)/2, 100, 175, 110)];
      self.backgroundView.image = [UIImage imageNamed:@"choose photo back.png"];
      self.backgroundView.userInteractionEnabled = YES;
      self.backgroundView.contentMode = UIViewContentModeScaleAspectFit;
      [self.view addSubview:self.backgroundView];
    
      self.closeButton = [[UIButton alloc]initWithFrame:CGRectMake(self.backgroundView.frame.origin.x + self.backgroundView.frame.size.width - 20  , self.backgroundView.frame.origin.y-10, 30, 30)];
      self.closeButton.backgroundColor = [UIColor clearColor];
      [self.closeButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
      [self.view addSubview:self.closeButton];

        //312x73
      self.cameraButton = [[UIButton alloc]initWithFrame:CGRectMake(self.backgroundView.frame.origin.x +  ((175 - 156)/2),
                                                                    self.backgroundView.frame.origin.y  + 20,
                                                                    156 ,37 )];
      [self.cameraButton setBackgroundImage:[UIImage imageNamed:@"take a photo button.png"] forState:UIControlStateNormal];
      [self.cameraButton addTarget:self action:@selector(cameraPressed) forControlEvents:UIControlEventTouchUpInside];
      [self.view addSubview:self.cameraButton];

      self.libraryButton = [[UIButton alloc]initWithFrame:CGRectMake(self.backgroundView.frame.origin.x + ((175 - 156)/2),
                                                                     self.backgroundView.frame.origin.y  + 25 + 37,
                                                                     156 , 37)];
      [self.libraryButton setBackgroundImage:[UIImage imageNamed:@"choose from library.png"] forState:UIControlStateNormal];
      [self.libraryButton addTarget:self action:@selector(libraryPressed) forControlEvents:UIControlEventTouchUpInside];
      [self.view addSubview:self.libraryButton];

    }
    
}


-(void)cameraPressed
{
    
    NSString *deviceType = [UIDevice currentDevice].model;
    [AFOpenGLManager beginOpenGLLoad];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        
        if ([UIImagePickerController isSourceTypeAvailable:
             UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *imagePicker =
            [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType =
            UIImagePickerControllerSourceTypeCamera;
            imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
            imagePicker.allowsEditing = YES;
            //[self presentViewController:imagePicker animated:YES completion:nil];
            self.popoverController = [[UIPopoverController alloc]
                                      initWithContentViewController:imagePicker];
            
            popoverController.delegate = self;
            
            [self.popoverController presentPopoverFromRect:CGRectMake(224.0f, 0.0f, 320.0f, 112.0f) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Sorry"
                                      message:@"Camera mode not availiable."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
            
        }
        
    }else{
        if ([UIImagePickerController isSourceTypeAvailable:
             UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *imagePicker =
            [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType =
            UIImagePickerControllerSourceTypeCamera;
            imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
            imagePicker.allowsEditing = YES;
            [self presentViewController:imagePicker
                               animated:YES completion:nil];
        }else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Sorry"
                                      message:@"Camera mode not availiable."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
            
        }
    }
}


-(void)libraryPressed
{
    
    
    NSString *deviceType = [UIDevice currentDevice].model;
    UIImagePickerController *imagePicker =
    [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType =
    UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
    imagePicker.allowsEditing = YES;
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:imagePicker];
        popoverController.delegate = self;
        [self.popoverController presentPopoverFromRect:CGRectMake(4.0f, 0.0f, 760.0f, 112.0f) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }else{
        [self presentViewController:imagePicker
                           animated:YES completion:nil];
    }

}


-(void)back
{
     [self.delegate closeChooseCameraSourceForm];
    
}


-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
   // [self.delegate closeChooseCameraSourceForm];
    
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = info[UIImagePickerControllerEditedImage];
        image=[image scaleToSize:CGSizeMake(800.0,800.0)];
        [self.popoverController dismissPopoverAnimated:YES];
        [self displayEditorForImage:image];
        /*
         PhotoViewController *detail=[[PhotoViewController alloc] init];
         detail.photo.image = image;
         detail.originalImage =image;
         if (self.isBrand) {
         detail.isBrand=TRUE;
         }else{
         detail.isBrand=FALSE;
         }
         detail.venueId = [[self.storeDetails valueForKey:@"id"] objectAtIndex:0];
         NSString *urlString=[NSString stringWithFormat:HOMEWithValues,[[self.storeDetails valueForKey:@"actual_image"] objectAtIndex:0]];
         detail.watermarkImage =[delegate returnCahedImageWithName:urlString];
         detail.promoMessage = self.promoMessage;
         detail.newPromo = self.newPromo;
         
         [self.popoverController dismissPopoverAnimated:YES];
         [self.navigationController pushViewController:detail animated:YES];
         */
    }
}

- (void)displayEditorForImage:(UIImage *)imageToEdit

{
    
    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:imageToEdit];
    /*
     kAFEnhance
     kAFEffects
     kAFStickers
     kAFOrientation
     kAFCrop
     kAFAdjustments
     kAFSharpness
     kAFDraw
     kAFText
     kAFRedeye
     kAFWhiten
     kAFBlemish
     kAFMeme
     kAFFrames;
     kAFFocus
     */
    // Set Tool Order
    NSArray * toolOrder = @[kAFEffects, kAFFocus, kAFOrientation, kAFCrop, kAFAdjustments];
    [AFPhotoEditorCustomization setToolOrder:toolOrder];
    
    
    [editorController setDelegate:self];
    
    [self presentViewController:editorController animated:YES completion:nil];
    
}

- (void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image

{
    [self.delegate closeChooseCameraSourceForm];

    //[self performSelectorOnMainThread:@selector(dismishPhotoEditor:) withObject:image waitUntilDone:false];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    //[self performSelectorOnMainThread:@selector(showPhotoView:) withObject:image waitUntilDone:false];
    
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    PhotoViewController *detail=[[PhotoViewController alloc] init];
    detail.originalImage =image;
    detail.isEmotion=YES;
    
    detail.venueId = self.emotionId;
    //
    //detail.watermarkImage =
    detail.watermarkImage = [delegate returnCahedImageWithName:self.imageURL];
    detail.promoMessage = @"";
    detail.newPromo = @"";
    
    [self.popoverController dismissPopoverAnimated:YES];
    [self.prevController.navigationController pushViewController:detail animated:YES];
}

-(void)dismishPhotoEditor:(UIImage*)image
{
    
}

-(void)showPhotoView:(UIImage*)image
{
    
}


//NSURLConnection methods following
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
