//
//  StoreViewController.m
//  Picbooster
//
//  Created by George Bafaloukas on 4/19/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "VenueBrandViewController.h"
#import "AppDelegate.h"
#import "PhotoViewController.h"
#import "UIImage+Scale.h"
#import "Consts.h"
#import "GridDetailViewController.h"
#import "VenueBrandMorePhotosViewController.h"

@interface VenueBrandViewController ()

@end

@implementation VenueBrandViewController
@synthesize mapView;
@synthesize storeId;
@synthesize responseData,hud,storeImages,storeDetails,imagesScrollView,imagesPager,popoverController;
@synthesize promoMessage,newPromo;
@synthesize userAnnotationCoord;
@synthesize pageControlUsed;
@synthesize labelPhotoCheckins;
@synthesize imageViewPhotoCheckins;
@synthesize labelAddress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSString *)stripTags:(NSString *)str
{
    NSMutableString *html = [NSMutableString stringWithCapacity:[str length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:str];
    scanner.charactersToBeSkipped = NULL;
    NSString *tempText = nil;
    
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&tempText];
        
        if (tempText != nil)
            [html appendString:tempText];
        
        [scanner scanUpToString:@">" intoString:NULL];
        
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation] + 1];
        
        tempText = nil;
    }
    
    return html;
}


-(void)createCustomNavigationBar:(NSString*)title
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 45.0f) displayPoints:false displayPicboosterTitleView:false title:@"" displaySettingsButton:false titleImageView:nil];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 45) displayPoints:false displayPicboosterTitleView:false title:@"" displaySettingsButton:false titleImageView:nil];
        }
    }
    else
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 65.0f) displayPoints:false displayPicboosterTitleView:false title:@"" displaySettingsButton:false titleImageView:nil];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 65) displayPoints:false displayPicboosterTitleView:false title:@"" displaySettingsButton:false titleImageView:nil];
        }
    }
    
    [self.view addSubview:self.customNavBar];
    self.customNavBar.delegate = self;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	// Do any additional setup after loading the view.
//    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundIPHONE4.png"]];
//    self.navigationController.navigationBar.tintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"menuTileNew.png"]];
//    self.navigationController.navigationBarHidden   =  YES;
    
    
    self.responseData = [[NSMutableData alloc] init];
    self.storeDetails = [[NSMutableDictionary alloc] init];
    self.storeImages = [[NSArray alloc] init];
//    if (self.isBrand) {
//        self.scrollView =[[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 600.0f)];
//    }else{
//        self.scrollView =[[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 600.0f)];
//    }
//    self.scrollView.backgroundColor =[UIColor whiteColor];
    
    [self createCustomNavigationBar:self.storeName];
    
    int offsetY =65.0f;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        offsetY = 45.0f;
    }
    
    int imagesheight = 180;
    if (self.view.frame.size.height <= 480)
    {
        imagesheight = 150;
    }
    self.imagesScrollView =[[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, offsetY, 320.0f, imagesheight)];

    
    self.imagesScrollView.delegate = self;
    self.imagesScrollView.showsHorizontalScrollIndicator = NO;
    self.imagesScrollView.alwaysBounceHorizontal = YES;
    self.imagesScrollView.alwaysBounceVertical = NO;
    self.imagesScrollView.backgroundColor = [UIColor lightGrayColor];
    self.imagesScrollView.pagingEnabled = YES;
	self.imagesScrollView.clipsToBounds = YES;
	self.imagesScrollView.scrollEnabled = YES;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
//        if (self.isBrand) {
//            self.scrollView.frame = CGRectMake(0.0f, 0.0f, 768.0f, 1024.0f);
//            self.imagesScrollView.frame = CGRectMake(0.0f, offsetY, 768.0f, 250.0f);
//        }else{
//            self.scrollView.frame = CGRectMake(0.0f, 0.0f, 768.0f, 1024.0f);
//            self.imagesScrollView.frame = CGRectMake(0.0f, offsetY, 768.0f, 250.0f);
//        }
    }
    //[self.view addSubview:self.view];
    
    self.imagesPager = [[UIPageControl alloc]initWithFrame:CGRectMake(0, offsetY + imagesheight-20, 320, 20)];
    [self.imagesPager  addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    [self.imagesPager setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:self.imagesScrollView];
    [self.view addSubview:self.imagesPager];
    
    //DCE5E9
    //233 229 220
    self.view.backgroundColor = [UIColor colorWithRed:233.0f/255.0f green:229.0f/255.0f blue:220.0f/255.0f alpha:1.0];
    //self.scrollView.delegate=self;
    self.promoMessage = [[NSString alloc] init];
    
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
    NSString *urlString= [NSString stringWithFormat:oneVenue,storeId];
    NSLog(@"URL:%@",urlString);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setTimeoutInterval:20.0];
    self.venueConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

-(void)viewWillAppear:(BOOL)animated
{
   // self.scrollView.contentOffset = CGPointMake(0, 0);
   // self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

//NSURLConnection methods following
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Connection Error!"
												   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
	[alert show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSString *myResponseString=[[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
	SBJsonParser *json = [[SBJsonParser alloc] init] ;
    
    if(connection== self.venueConnection)
    {
    self.storeDetails = [json objectWithString:myResponseString];
    self.storeImages = [[self.storeDetails valueForKey:@"images"] objectAtIndex:0];
        
    NSString *venueName   =  [[NSString alloc] initWithFormat:@"%@",[[self.storeDetails valueForKey:@"name"] objectAtIndex:0]];
    self.customNavBar.tlabel.text =venueName;
        [self.customNavBar.tlabel performSelectorOnMainThread:@selector(setText:) withObject:venueName waitUntilDone:NO];
        
    NSArray* ar =[json objectWithString:myResponseString];
    NSMutableDictionary* dict = (NSMutableDictionary*)ar[0];
    NSString* checkins = (NSString*)[dict objectForKey:@"total_pics"];
    
    int offsetY = self.imagesScrollView.frame.origin.y + self.imagesScrollView.frame.size.height;

    NSString *deviceType = [UIDevice currentDevice].model;
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0.0f, offsetY, 320.0f, 50.0f)];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        self.mapView.frame = CGRectMake(0.0f, offsetY, 768.0f, 150.0f);
    }
    self.mapView.delegate=self;
    if ([[[self.storeDetails valueForKey:@"new_promo"] objectAtIndex:0] isKindOfClass:[NSString class]]) {
        if ([[[self.storeDetails valueForKey:@"new_promo"] objectAtIndex:0] isEqualToString:@"1"]) {
            self.newPromo=TRUE;
        }else{
            self.newPromo=FALSE;
        }
        self.promoMessage = [[self.storeDetails valueForKey:@"promo_message"] objectAtIndex:0];
    }
    if (self.isBrand) {
       self.mapView.hidden = TRUE;
    }
    [self.view addSubview:self.mapView];
    
    [self.hud hide:YES];
    
    //add 5 latest images to imageScrollView
    int imagesheight = 180;
    int imagewidth = 320;
    if (self.view.frame.size.height <= 480)
    {
        imagesheight = 150;
    }
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        imagesheight = 300;
        imagewidth = 768;
    }
    self.imagesScrollView.contentSize = CGSizeMake(self.storeImages.count *imagewidth, imagesheight);
   
    self.imagesPager.numberOfPages = [self.storeImages count];
    self.imagesPager.currentPage = 0;
    
    int index = 0;
    for (NSDictionary *dic in self.storeImages)
    {
        NSString *urlStringUserImage=[NSString stringWithFormat:APIUrl,[dic valueForKey:@"hash"]];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(index * imagewidth , 0.0f, imagewidth, imagesheight)];
        imageView.userInteractionEnabled = true;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [imageButton addTarget:self
                        action:@selector(imageButtonPressed:)
              forControlEvents:UIControlEventTouchDown];
        imageButton.tag=index;
        imageButton.frame = CGRectMake(index * imagewidth , 0.0f, imagewidth, imagesheight);
        imageButton.backgroundColor=[UIColor clearColor];

        NSLog(@"URL: %@",urlStringUserImage);
        imageView.image = [delegate returnCahedImageWithName:urlStringUserImage];
        
        index++;
        
        [self.imagesScrollView addSubview:imageView];
      //  [self.imagesScrollView addSubview:imageButton];
        
        //125x27
        UIButton* showAll = [UIButton buttonWithType:UIButtonTypeCustom];
        showAll.frame =  CGRectMake(imagewidth - 62 - 3, self.imagesScrollView.frame.origin.y + 3,  62, 13);
        [showAll setImage:[UIImage imageNamed:@"view all photos.png"] forState:UIControlStateNormal];
        [showAll addTarget:self action:@selector(showAll) forControlEvents:UIControlEventTouchUpInside];
        //showAll.backgroundColor = [UIColor redColor];
        [self.view addSubview:showAll];

    }
    
    if(self.storeImages.count==0)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(index * imagewidth , 0.0f, imagewidth, 150.0f)];
        if (self.view.frame.size.height <= 480)
        {
            [imageView setImage:[UIImage imageNamed:@"no image 320x150.png"]];
        }
        else
        {
            [imageView setImage:[UIImage imageNamed:@"no image 320x180.png"]];
            imageView.frame  = CGRectMake(index * imagewidth , 0.0f, imagewidth, 180.0f);
        }
        [self.imagesScrollView setScrollEnabled:false];
        self.imagesPager.hidden = YES;
        [self.imagesScrollView addSubview:imageView];
    }
   
    
    offsetY =self.mapView.frame.origin.y + self.mapView.frame.size.height;
    if(self.isBrand)
    {
        offsetY = self.mapView.frame.origin.y;
    }
    
    
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        
        //638x174
        UIImageView* socialBar = [[UIImageView alloc]initWithFrame:CGRectMake(0, offsetY, 320, 87)];
        [socialBar setImage:[UIImage imageNamed:@"venue screen bar.png"]];
        [self.view addSubview:socialBar];
        
        
        UIImageView * storeImage = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,100.0f, 87.0f)];
        NSString *urlString=[NSString stringWithFormat:HOMEWithValues,[[self.storeDetails valueForKey:@"actual_image"] objectAtIndex:0]];
        storeImage.contentMode = UIViewContentModeScaleAspectFit;
        storeImage.image =[delegate returnCahedImageWithName:urlString];
        [socialBar addSubview:storeImage];
        
        
        //photocheckins
        //243x166
        self.imageViewPhotoCheckins =  [[UIImageView alloc]initWithFrame:CGRectMake(103.0f, 2.0f, 121.0f, 83.0f)];
        [self.imageViewPhotoCheckins setImage:[UIImage imageNamed:@"photocheckin counter.png"]];
        [socialBar addSubview:imageViewPhotoCheckins];
        socialBar.userInteractionEnabled = YES;
        
        self.labelPhotoCheckins =  [[UILabel alloc]initWithFrame:CGRectMake(0.0, 16.0f, 50.0f, 40.0f)];
        self.labelPhotoCheckins.textColor = [UIColor blackColor];
        self.labelPhotoCheckins.backgroundColor = [UIColor clearColor];
        [self.labelPhotoCheckins setTextAlignment:NSTextAlignmentRight];
        [self.labelPhotoCheckins setFont:[UIFont boldSystemFontOfSize:18]];
        self.labelPhotoCheckins.text = checkins;
        [imageViewPhotoCheckins addSubview:labelPhotoCheckins];
        
        //fb
        UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button3 addTarget:self
                    action:@selector(ButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        button3.tag=3;
        [button3 setBackgroundColor:[UIColor clearColor]];
        button3.frame = CGRectMake(225.0f, 0.0f, 45.0f, 42.0f);
        [socialBar addSubview:button3];
        
        
        //ph
        UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button2 addTarget:self
                    action:@selector(ButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        button2.tag=2;
        [button2 setBackgroundColor:[UIColor clearColor]];
        button2.frame = CGRectMake(270.0f, 0.0f, 45.0f, 42.0f);
        [socialBar addSubview:button2];
        
        //tw
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 addTarget:self
                    action:@selector(ButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        button1.tag=1;
        [button1 setBackgroundColor:[UIColor clearColor]];
        button1.frame = CGRectMake(225.0f, 45.0f, 45.0f, 42.0f);
        [socialBar addSubview:button1];
        
        //em
        UIButton *button4 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button4 addTarget:self
                    action:@selector(ButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        button4.tag=4;
        [button4 setBackgroundColor:[UIColor clearColor]];
        button4.frame =  CGRectMake(270.0f, 45.0f, 45.0f, 42.0f);
        [socialBar addSubview:button4];
        
        //store address
        offsetY  = socialBar.frame.origin.y + socialBar.frame.size.height;
        
        UIImageView* horzLine1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, offsetY  , 320, 1)];
        [horzLine1 setImage:[UIImage imageNamed:@"horizlineWhite.png"]];
        [self.view addSubview:horzLine1];
        
        offsetY  = offsetY + 1;
        self.labelAddress =  [[UILabel alloc]initWithFrame:CGRectMake(0, offsetY  , 320, 30)];
        self.labelAddress.textColor = [UIColor blackColor];
        self.labelAddress.backgroundColor = [UIColor clearColor];
        [self.labelAddress setTextAlignment:NSTextAlignmentCenter];
        [self.labelAddress setText:[[self.storeDetails valueForKey:@"address"] objectAtIndex:0]];
        //[self.labelAddress setText:@"Artemidos 14,artemis"];
        [self.labelAddress setFont:[UIFont systemFontOfSize:12]];
        
        [self.view addSubview:labelAddress];
        
        offsetY  = offsetY + 30;
        UIImageView* horzLine2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, offsetY  , 320, 1)];
        [horzLine2 setImage:[UIImage imageNamed:@"horizlineWhite.png"]];
        [self.view addSubview:horzLine2];
        
        offsetY  = offsetY + 1;
        UILabel *companyDetailLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, offsetY, 280.0f, 60.0f)];
        companyDetailLabel.numberOfLines = 0;
        companyDetailLabel.adjustsFontSizeToFitWidth = NO;
        companyDetailLabel.textColor = [UIColor blackColor];
        companyDetailLabel.backgroundColor=[UIColor clearColor];
        if(self.view.frame.size.height <=480 )
            [companyDetailLabel setFont:[UIFont systemFontOfSize:9]];
        else
            [companyDetailLabel setFont:[UIFont systemFontOfSize:10]];
        NSString * s =[[self.storeDetails valueForKey:@"description"] objectAtIndex:0];
      
        
        if ([s isKindOfClass:[NSString class]]) {
            if ([s isEqualToString:@""]) {
                companyDetailLabel.text = [[NSString alloc] initWithFormat:@"It's so simple to take photos. Use your imagination and explore new places around your city..Picbooster is a free new way to explore what's around. Made from fun lovers forfun lover."];
            }else{
                s=[self stripTags:s];
                NSLog(@"%@",s);
                companyDetailLabel.text = [[NSString alloc] initWithFormat:@"%@",s];
            }
        }else{
            companyDetailLabel.text = [[NSString alloc] initWithFormat:@"It's so simple to take photos. Use your imagination and explore new places around your city..Picbooster is a free new way to explore what's around. Made from fun lovers forfun lover."];

        }
        [self.view addSubview:companyDetailLabel];
        
       // self.view.contentSize=CGSizeMake(300,1200);
        
        UIImageView *photoBar =[[UIImageView alloc] initWithFrame:CGRectMake(0.0f, self.view.frame.size.height - 149.0f, 768.0f, 49.0f)];
        CGRect r =photoBar.frame;
        photoBar.contentMode = UIViewContentModeScaleAspectFit;
        [photoBar setImage:[UIImage imageNamed:@"bottombar.png"]];
        photoBar.userInteractionEnabled = YES;
        [self.view addSubview:photoBar];
        
        UIButton *cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [cameraButton addTarget:self
                         action:@selector(cameraButtonPressed)
               forControlEvents:UIControlEventTouchDown];
        cameraButton.frame = CGRectMake(0.0f, 0, 762.0f/2, 49.0f);
        [photoBar addSubview:cameraButton];
        
        UIButton *libraryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [libraryButton addTarget:self
                          action:@selector(libraryButtonPressed)
                forControlEvents:UIControlEventTouchDown];
        libraryButton.frame = CGRectMake(762.0f/2, 0.0f, 762.0f/2, 49.0f);
        [photoBar addSubview:libraryButton];
        
    }else{
        
        //638x174
        UIImageView* socialBar = [[UIImageView alloc]initWithFrame:CGRectMake(0, offsetY, 320, 87)];
        [socialBar setImage:[UIImage imageNamed:@"venue screen bar.png"]];
         [self.view addSubview:socialBar];
        
        
        UIImageView * storeImage = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,100.0f, 87.0f)];
        NSString *urlString=[NSString stringWithFormat:HOMEWithValues,[[self.storeDetails valueForKey:@"actual_image"] objectAtIndex:0]];
        storeImage.contentMode = UIViewContentModeScaleAspectFit;
        storeImage.image =[delegate returnCahedImageWithName:urlString];
        [socialBar addSubview:storeImage];
        
        
        //photocheckins
        //227x83
        self.imageViewPhotoCheckins =  [[UIImageView alloc]initWithFrame:CGRectMake(103.0f, 2.0f, 121.0f, 83.0f)];
        [self.imageViewPhotoCheckins setImage:[UIImage imageNamed:@"photocheckin counter.png"]];
        [socialBar addSubview:imageViewPhotoCheckins];
        socialBar.userInteractionEnabled = YES;
        
        self.labelPhotoCheckins =  [[UILabel alloc]initWithFrame:CGRectMake(0.0, 16.0f, 50.0f, 40.0f)];
        self.labelPhotoCheckins.textColor = [UIColor blackColor];
        self.labelPhotoCheckins.backgroundColor = [UIColor clearColor];
        [self.labelPhotoCheckins setTextAlignment:NSTextAlignmentRight];
        [self.labelPhotoCheckins setFont:[UIFont boldSystemFontOfSize:18]];
        self.labelPhotoCheckins.text = checkins;
        [imageViewPhotoCheckins addSubview:labelPhotoCheckins];

        
        //fb
        UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button3 addTarget:self
                    action:@selector(ButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        button3.tag=3;
        [button3 setBackgroundColor:[UIColor clearColor]];
        button3.frame = CGRectMake(225.0f, 0.0f, 45.0f, 42.0f);
        [socialBar addSubview:button3];
      
        
        //ph
        UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button2 addTarget:self
                    action:@selector(ButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        button2.tag=2;
        [button2 setBackgroundColor:[UIColor clearColor]];
        button2.frame = CGRectMake(270.0f, 0.0f, 45.0f, 42.0f);
        [socialBar addSubview:button2];
        
        //tw
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 addTarget:self
                    action:@selector(ButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        button1.tag=1;
        [button1 setBackgroundColor:[UIColor clearColor]];
        button1.frame = CGRectMake(225.0f, 45.0f, 45.0f, 42.0f);
        [socialBar addSubview:button1];
        
        //em
        UIButton *button4 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button4 addTarget:self
                    action:@selector(ButtonPressed:)
          forControlEvents:UIControlEventTouchDown];
        button4.tag=4;
        [button4 setBackgroundColor:[UIColor clearColor]];
        button4.frame =  CGRectMake(270.0f, 45.0f, 45.0f, 42.0f);
        [socialBar addSubview:button4];
        
        //store address
        offsetY  = socialBar.frame.origin.y + socialBar.frame.size.height;
        
        UIImageView* horzLine1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, offsetY  , 320, 1)];
        [horzLine1 setImage:[UIImage imageNamed:@"horizlineWhite.png"]];
        [self.view addSubview:horzLine1];
        
        offsetY  = offsetY + 1;
        self.labelAddress =  [[UILabel alloc]initWithFrame:CGRectMake(0, offsetY  , 320, 30)];
        self.labelAddress.textColor = [UIColor blackColor];
        self.labelAddress.backgroundColor = [UIColor clearColor];
        [self.labelAddress setTextAlignment:NSTextAlignmentCenter];
        [self.labelAddress setText:[[self.storeDetails valueForKey:@"address"] objectAtIndex:0]];
       // [self.labelAddress setText:@"Artemidos 14,artemis"];
        [self.labelAddress setFont:[UIFont systemFontOfSize:12]];
        
        [self.view addSubview:labelAddress];
        
        offsetY  = offsetY + 30;
        UIImageView* horzLine2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, offsetY  , 320,1)];
        [horzLine2 setImage:[UIImage imageNamed:@"horizlineWhite.png"]];
        [self.view addSubview:horzLine2];
        
      
        offsetY  = offsetY + 1;
        UILabel *companyDetailLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, offsetY, 280.0f, 60.0f)];
        companyDetailLabel.numberOfLines = 0;
        companyDetailLabel.adjustsFontSizeToFitWidth = NO;
        companyDetailLabel.textColor = [UIColor blackColor];
        companyDetailLabel.backgroundColor=[UIColor clearColor];
        if(self.view.frame.size.height <=480 )
           [companyDetailLabel setFont:[UIFont systemFontOfSize:9]];
        else
            [companyDetailLabel setFont:[UIFont systemFontOfSize:10]];
        NSString * s =[[self.storeDetails valueForKey:@"description"] objectAtIndex:0];
        //NSString * s = @"jdhbvsabfvhwbvberilveniutehvbhierubnherugtheaivneruithgveingehbveruiatgnveuaynveu nveutnei4tvehniutvhbeuivtgeihiurvneirubvntelruibveriubveiutgneivniueiurtvbeiruvnteuithenibtbesniiuvteshbieshiueshritbegter";
        
        if ([s isKindOfClass:[NSString class]]) {
            if ([s isEqualToString:@""]) {
                companyDetailLabel.text = [[NSString alloc] initWithFormat:@"It's so simple to take photos. Use your imagination and explore new places around your city..Picbooster is a free new way to explore what's around. Made from fun lovers forfun lover."];
            }else{
                s=[self stripTags:s];
                NSLog(@"%@",s);
                companyDetailLabel.text = [[NSString alloc] initWithFormat:@"%@",s];
            }
        }else{
            companyDetailLabel.text = [[NSString alloc] initWithFormat:@"It's so simple to take photos. Use your imagination and explore new places around your city..Picbooster is a free new way to explore what's around. Made from fun lovers forfun lover."];
        }
        [self.view addSubview:companyDetailLabel];
        
        //self.scrollView.contentSize=CGSizeMake(300,550);
        
        UIImageView *photoBar =[[UIImageView alloc] initWithFrame:CGRectMake(0.0f, self.view.frame.size.height - 49.0f, 320.0f, 49.0f)];
        [photoBar setImage:[UIImage imageNamed:@"bottombar.png"]];
        photoBar.userInteractionEnabled = YES;
        [self.view addSubview:photoBar];
        
        UIButton *cameraButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [cameraButton addTarget:self
                         action:@selector(cameraButtonPressed)
               forControlEvents:UIControlEventTouchDown];
        cameraButton.frame = CGRectMake(0, 0, 160.0f, 49.0f);
        [photoBar addSubview:cameraButton];
        
        UIButton *libraryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [libraryButton addTarget:self
                          action:@selector(libraryButtonPressed)
                forControlEvents:UIControlEventTouchDown];
        libraryButton.frame = CGRectMake(160.0f, 0.0f, 160.0f, 49.0f);
        [photoBar addSubview:libraryButton];
    }
   
    CLLocationCoordinate2D annotationCoord;
    if ([[[self.storeDetails valueForKey:@"lat"] objectAtIndex:0] floatValue] == 0.0) {
        annotationCoord.latitude = self.userAnnotationCoord.coordinate.latitude;
        annotationCoord.longitude = self.userAnnotationCoord.coordinate.longitude;
        
    }else{
        annotationCoord.latitude = [[[self.storeDetails valueForKey:@"lat"] objectAtIndex:0] floatValue];
        annotationCoord.longitude = [[[self.storeDetails valueForKey:@"lng"] objectAtIndex:0] floatValue];
        
    }

    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = annotationCoord;
    annotationPoint.title = [[self.storeDetails valueForKey:@"name"] objectAtIndex:0];
    [mapView addAnnotation:annotationPoint];
    MKCoordinateRegion region =
    MKCoordinateRegionMakeWithDistance (
                                        annotationCoord, 500, 500);
    [mapView setRegion:region animated:NO];
        
//    NSString *urlString= [NSString stringWithFormat:getUserPhotoCheckins,[AppDelegate getFbUserId]];
//    NSLog(@"URL:%@",urlString);
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
//    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
//    [request setTimeoutInterval:20.0];
//    self.photoCheckinsConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    }

    else if(connection == self.photoCheckinsConnection)
    {
        NSMutableDictionary* dict = [json objectWithString:myResponseString];

        NSString* status   = [dict valueForKey:@"status"];
        if([status isEqualToString:@"SUCCESS" ])
        {
            NSString* checkins = [dict valueForKey:@"count"];
            self.labelPhotoCheckins.text =checkins;
            
        }
    }
}

-(void)showAll
{
    VenueBrandMorePhotosViewController* allPhotos = [[VenueBrandMorePhotosViewController alloc]init];
    allPhotos.venueID =[[self.storeDetails valueForKey:@"id"] objectAtIndex:0];
    [self.navigationController pushViewController:allPhotos animated:true];
}


//customNavigationBar actions
-(void)barButtonPressed:(NSString *)button{
    if([button isEqualToString:@"Back"]){
        [self.navigationController popViewControllerAnimated:YES];
    }
}


// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    pageControlUsed = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (pageControlUsed)
    {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.imagesScrollView.frame.size.width;
    int page = floor((self.imagesScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.imagesPager.currentPage = page;
}


- (IBAction)changePage:(id)sender
{
    int page = self.imagesPager.currentPage;
	
    // update the scroll view to the appropriate page
    CGRect frame = self.imagesScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.imagesScrollView scrollRectToVisible:frame animated:YES];
    
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    pageControlUsed = YES;
}



- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString* SFAnnotationIdentifier = @"SFAnnotationIdentifier";
    MKPinAnnotationView* pinView =
    (MKPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:SFAnnotationIdentifier];
    if (!pinView)
    {
        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                        reuseIdentifier:SFAnnotationIdentifier] ;
        annotationView.canShowCallout = YES;
        UIImage *flagImage;
        if ([annotation.title isEqualToString:@"User"]) {
            flagImage = [UIImage imageNamed:@"user pin.png"];
        }else{
            flagImage = [UIImage imageNamed:@"map pin.png"];
        }
        annotationView.image = flagImage;
        annotationView.opaque = NO;
        
        return annotationView;
    }
    else
    {
        pinView.annotation = annotation;
    }
    return pinView;
}
-(void)imageButtonPressed:(id)sender{
    UIButton *pressedButton = sender;
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    NSDictionary *gridCell = [self.storeImages objectAtIndex:pressedButton.tag];
    GridDetailViewController *detail=[[GridDetailViewController alloc] init];
    if (self.isBrand) {
        detail.isBrand =TRUE;
    }
    NSString *urlString=[NSString stringWithFormat:APIUrl,[gridCell valueForKey:@"hash"]];
    NSString *venueID=[gridCell valueForKey:@"venue_id"];
    UIImage *detailImage=[delegate returnCahedImageWithName:urlString];
    detail.imageUrl=[gridCell valueForKey:@"hash"];
    detail.venueID=venueID;
    detail.imageView = [[UIImageView alloc] initWithImage:[detailImage scaleToSize:CGSizeMake(300.0f, 300.0f)]];
    detail.imageView.frame = CGRectMake(10.0, 30.0, 300.0f, 300.0f);
    [self.navigationController pushViewController:detail animated:NO];
}
-(void)libraryButtonPressed{
    NSString *deviceType = [UIDevice currentDevice].model;
    UIImagePickerController *imagePicker =
    [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType =
    UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
    imagePicker.allowsEditing = YES;
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.popoverController = [[UIPopoverController alloc]
                                  initWithContentViewController:imagePicker];
        popoverController.delegate = self;
        [self.popoverController presentPopoverFromRect:CGRectMake(4.0f, 0.0f, 760.0f, 112.0f) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }else{
        [self presentViewController:imagePicker
                           animated:YES completion:nil];
    }
}
-(void)cameraButtonPressed{
    NSString *deviceType = [UIDevice currentDevice].model;
    [AFOpenGLManager beginOpenGLLoad];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
       
        if ([UIImagePickerController isSourceTypeAvailable:
             UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *imagePicker =
            [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType =
            UIImagePickerControllerSourceTypeCamera;
            imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
            imagePicker.allowsEditing = YES;
            //[self presentViewController:imagePicker animated:YES completion:nil];
            self.popoverController = [[UIPopoverController alloc]
                                      initWithContentViewController:imagePicker];
            
            popoverController.delegate = self;
            
            [self.popoverController presentPopoverFromRect:CGRectMake(224.0f, 0.0f, 320.0f, 112.0f) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Sorry"
                                      message:@"Camera mode not availiable."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
            
        }
        
    }else{
        if ([UIImagePickerController isSourceTypeAvailable:
             UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *imagePicker =
            [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.sourceType =
            UIImagePickerControllerSourceTypeCamera;
            imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
            imagePicker.allowsEditing = YES;
            [self presentViewController:imagePicker
                               animated:YES completion:nil];
        }else{
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Sorry"
                                      message:@"Camera mode not availiable."
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
            
        }
    }
}

- (void)photoEditor:(AFPhotoEditorController *)editor finishedWithImage:(UIImage *)image

{
    [self dismissViewControllerAnimated:NO completion:nil];
    // Handle the result image here
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    PhotoViewController *detail=[[PhotoViewController alloc] init];
    detail.originalImage =image;
    if (self.isBrand) {
        detail.isBrand=TRUE;
    }else{
        detail.isBrand=FALSE;
    }
    detail.venueId = [[self.storeDetails valueForKey:@"id"] objectAtIndex:0];
    NSString *urlString=[NSString stringWithFormat:HOMEWithValues,[[self.storeDetails valueForKey:@"actual_image"] objectAtIndex:0]];
    detail.watermarkImage =[delegate returnCahedImageWithName:urlString];
    detail.promoMessage = self.promoMessage;
    detail.newPromo = self.newPromo;
    
    [self.popoverController dismissPopoverAnimated:YES];
    [self.navigationController pushViewController:detail animated:YES];
    
}
- (void)photoEditorCanceled:(AFPhotoEditorController *)editor

{
    
    // Handle cancellation here
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)displayEditorForImage:(UIImage *)imageToEdit

{
    
    AFPhotoEditorController *editorController = [[AFPhotoEditorController alloc] initWithImage:imageToEdit];
    /*
     kAFEnhance
     kAFEffects
     kAFStickers
     kAFOrientation
     kAFCrop
     kAFAdjustments
     kAFSharpness
     kAFDraw
     kAFText
     kAFRedeye
     kAFWhiten
     kAFBlemish
     kAFMeme
     kAFFrames;
     kAFFocus
     */
    // Set Tool Order
    NSArray * toolOrder = @[kAFEffects, kAFFocus, kAFOrientation, kAFCrop, kAFAdjustments];
    [AFPhotoEditorCustomization setToolOrder:toolOrder];

    
    [editorController setDelegate:self];
    
    [self presentViewController:editorController animated:YES completion:nil];
    
}
#pragma mark -
#pragma mark UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
   
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    [self dismissViewControllerAnimated:NO completion:nil];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = info[UIImagePickerControllerEditedImage];
        image=[image scaleToSize:CGSizeMake(800.0,800.0)];
        [self.popoverController dismissPopoverAnimated:YES];
        [self displayEditorForImage:image];
        /*
        PhotoViewController *detail=[[PhotoViewController alloc] init];
        detail.photo.image = image;
        detail.originalImage =image;
        if (self.isBrand) {
            detail.isBrand=TRUE;
        }else{
            detail.isBrand=FALSE;
        }
        detail.venueId = [[self.storeDetails valueForKey:@"id"] objectAtIndex:0];
        NSString *urlString=[NSString stringWithFormat:HOMEWithValues,[[self.storeDetails valueForKey:@"actual_image"] objectAtIndex:0]];
        detail.watermarkImage =[delegate returnCahedImageWithName:urlString];
        detail.promoMessage = self.promoMessage;
        detail.newPromo = self.newPromo;
        
        [self.popoverController dismissPopoverAnimated:YES];
        [self.navigationController pushViewController:detail animated:YES];
         */
    }
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)ButtonPressed:(id)sender{
    UIButton* buttonPressed = sender;
    if (buttonPressed.tag == 1) {
        //twitter
        if (![[[self.storeDetails valueForKey:@"twitter"] objectAtIndex:0] isKindOfClass:[NSString class]]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                            message:@"This venue doesn't have a registered Twitter page!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }else{
            if ([[[self.storeDetails valueForKey:@"twitter"] objectAtIndex:0] isEqualToString:@""]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                                message:@"This venue doesn't have a registered Twitter page!"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }else{
                NSURL *twitterLink = [NSURL URLWithString:[[self.storeDetails valueForKey:@"twitter"] objectAtIndex:0]];
                if (![[UIApplication sharedApplication] openURL:twitterLink]){
                    NSLog(@"%@%@",@"Failed to open url:",[twitterLink description]);
                    twitterLink = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",[[self.storeDetails valueForKey:@"twitter"] objectAtIndex:0]]];
                    if (![[UIApplication sharedApplication] openURL:twitterLink]){
                        NSLog(@"%@%@",@"Failed to open url:",[twitterLink description]);
                    }
                }
            }
        }
        
    }
    if (buttonPressed.tag == 2) {
        if (![[[self.storeDetails valueForKey:@"phone"] objectAtIndex:0] isKindOfClass:[NSString class]]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                            message:@"This venue doesn't have a registered Phone Number!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }else{
            if ([[[self.storeDetails valueForKey:@"phone"] objectAtIndex:0] isEqualToString:@""]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                                message:@"This venue doesn't have a registered Phone Number!"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }else{
                NSString *callString = [NSString stringWithFormat:@"tel:%@",[[self.storeDetails valueForKey:@"phone"] objectAtIndex:0]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callString]];

            }
        }
    }
    if (buttonPressed.tag == 3) {
        //facebook
        if (![[[self.storeDetails valueForKey:@"facebookpage"] objectAtIndex:0] isKindOfClass:[NSString class]]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                            message:@"This venue doesn't have a registered Facebook page!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }else{
            if ([[[self.storeDetails valueForKey:@"facebookpage"] objectAtIndex:0] isEqualToString:@""]) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                                message:@"This venue doesn't have a registered Facebook page!"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }else{
                NSURL *facebookpage = [NSURL URLWithString:[[self.storeDetails valueForKey:@"facebookpage"] objectAtIndex:0]];
                if (![[UIApplication sharedApplication] openURL:facebookpage])
                    NSLog(@"%@%@",@"Failed to open url:",[facebookpage description]);
            }
        }
    }
    if (buttonPressed.tag == 4) {
        //email
        if ([MFMailComposeViewController canSendMail])
        {
            
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            mailer.mailComposeDelegate = self;
            NSArray *recipients = [NSArray arrayWithObjects:[[self.storeDetails valueForKey:@"email"] objectAtIndex:0], nil];
            NSLog(@"Recipients: %@", recipients);
            [mailer setToRecipients:recipients];
            [mailer setSubject:@"Mail from picbooster"];
            [self presentViewController:mailer animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [[delegate imageCache] removeAllObjects];
}

@end
