//
//  ExploreViewController.h
//  Picbooster
//
//  Created by George Bafaloukas on 4/20/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MBProgressHUD.h"
#import "SBJson.h"
#import <CoreLocation/CoreLocation.h>
#import "MapViewController.h"
#import "VenuesBrandsSelectionMenuView.h"
#import "CustomNavigationBar.h"
#import "ChooseCameraSourceController.h"

@interface ExploreViewController : UIViewController <MKMapViewDelegate,UITableViewDataSource,UITableViewDelegate,CLLocationManagerDelegate,UISearchBarDelegate, UISearchDisplayDelegate,VenuesBrandsSelectionDelegate,CustomNavigationBarDelegate , ChooseCameraSourceDelegate>{
    MKMapView *mapView;
    MBProgressHUD *hud;
    NSMutableData *responseData;
    NSMutableArray * services;
    UITableView *myTableView;
    CLLocationManager *_locationManager;
    CLLocation *usersLocation;
    BOOL foundLocation;
    NSMutableArray *allVenues;
    //NSCache *imageCache;
    
   
}


@property (nonatomic, strong) CustomNavigationBar* customNavBar;

@property (strong,nonatomic) CLLocation *usersLocation;
@property (strong,nonatomic) CLLocationManager *_locationManager;
@property (strong,nonatomic) UITableView *myTableView;
@property (strong,nonatomic) MBProgressHUD *hud;
@property (strong, nonatomic) MKMapView *mapView;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSMutableArray * services;
@property (nonatomic, strong) NSMutableArray * filteredServices;

@property(nonatomic, strong) VenuesBrandsSelectionMenuView *selectionBar;
@property(nonatomic, strong) UISearchBar *searchBar;
@property(nonatomic, strong) UISearchDisplayController *strongSearchDisplayController;
@property(nonatomic, copy) NSArray *filteredPersons;
@property(nonatomic, copy) NSString *currentSearchString;
@property(nonatomic, strong) NSMutableArray *allVenues;
//@property (strong,nonatomic) NSCache *imageCache;
@property (strong,nonatomic) UIImageView *bannerImage;
@property(nonatomic, strong) NSMutableArray * categories;

@property(nonatomic,assign)  int selected_cat_id;
@property(nonatomic,assign)  int selected_emotion_cat_id;

-(void)onSelectVenues;
@end
