//
//  GridFooterView.m
//  Picbooster
//
//  Created by Vassilis Dourmas on 16/02/14.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "VenuesBrandsSelectionMenuView.h"
@interface VenuesBrandsSelectionMenuView ()
@property (nonatomic, strong) UIView *mainView;
@end
@implementation VenuesBrandsSelectionMenuView
@synthesize mainView;
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            //iPad
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, self.frame.size.height)];
            mainView.backgroundColor = [UIColor lightGrayColor];
            [self addSubview:mainView];
            
            UIImageView* bacgroundImageView = [[UIImageView alloc]initWithFrame:CGRectMake(4.0f, 4.0f, 243.0f, 32.0f)];
            [bacgroundImageView setImage:[UIImage imageNamed:@"buttons bar.png"]];
            
            self.buttonVenues = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.buttonVenues addTarget:self action:@selector(selectionButtonPressed:)  forControlEvents:UIControlEventTouchDown];
            self.buttonVenues.tag=1;
            self.buttonVenues.frame = CGRectMake(2.0f, 4.0f, 128.0f, 31.0f);
            
            self.buttonBrands = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.buttonBrands addTarget:self action:@selector(selectionButtonPressed:)  forControlEvents:UIControlEventTouchDown];
            self.buttonBrands.tag=2;
            self.buttonBrands.frame = CGRectMake(128.0f, 4.0f, 128.0f, 31.0f);
            
            self.buttonEmotions = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.buttonEmotions addTarget:self action:@selector(selectionButtonPressed:)  forControlEvents:UIControlEventTouchDown];
            [self.buttonEmotions setBackgroundImage:[UIImage imageNamed:@"emotions button.png"] forState:UIControlStateNormal];
            [self.buttonEmotions setBackgroundImage:[UIImage imageNamed:@"emotions pressed button.png"] forState:UIControlStateSelected];
            self.buttonEmotions.tag=3;
            self.buttonEmotions.frame = CGRectMake(260.0f, 4.0f, 60.0f, 31.0f);
            
            [mainView addSubview:bacgroundImageView];
            [mainView addSubview:self.buttonVenues];
            [mainView addSubview:self.buttonBrands];
            [mainView addSubview:self.buttonEmotions];
        }
        else
        {
              mainView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, self.frame.size.height)];
              mainView.backgroundColor = [UIColor lightGrayColor];
              [self addSubview:mainView];
            
              UIImageView* bacgroundImageView = [[UIImageView alloc]initWithFrame:CGRectMake(4.0f, 4.0f, 243.0f, 32.0f)];
              [bacgroundImageView setImage:[UIImage imageNamed:@"buttons bar.png"]];
            
               self.buttonVenues = [UIButton buttonWithType:UIButtonTypeCustom];
               [self.buttonVenues addTarget:self action:@selector(selectionButtonPressed:)  forControlEvents:UIControlEventTouchDown];
                self.buttonVenues.tag=1;
               self.buttonVenues.frame = CGRectMake(2.0f, 4.0f, 128.0f, 31.0f);

               self.buttonBrands = [UIButton buttonWithType:UIButtonTypeCustom];
               [self.buttonBrands addTarget:self action:@selector(selectionButtonPressed:)  forControlEvents:UIControlEventTouchDown];
               self.buttonBrands.tag=2;
               self.buttonBrands.frame = CGRectMake(128.0f, 4.0f, 128.0f, 31.0f);

               self.buttonEmotions = [UIButton buttonWithType:UIButtonTypeCustom];
               [self.buttonEmotions addTarget:self action:@selector(selectionButtonPressed:)  forControlEvents:UIControlEventTouchDown];
               [self.buttonEmotions setBackgroundImage:[UIImage imageNamed:@"emotions button.png"] forState:UIControlStateNormal];
               [self.buttonEmotions setBackgroundImage:[UIImage imageNamed:@"emotions pressed button.png"] forState:UIControlStateSelected];
               self.buttonEmotions.tag=3;
               self.buttonEmotions.frame = CGRectMake(260.0f, 4.0f, 60.0f, 31.0f);

              [mainView addSubview:bacgroundImageView];
              [mainView addSubview:self.buttonVenues];
              [mainView addSubview:self.buttonBrands];
              [mainView addSubview:self.buttonEmotions];
        }
    }
    return self;
}

-(void)selectionButtonPressed:(id)sender{
    UIButton* buttonPressed = sender;
    if(buttonPressed.tag==1){
        NSLog(@"Venues selected");
        [delegate selectButtonPressed:@"Venues"];
    }else if (buttonPressed.tag==2){
        NSLog(@"Brands selected");
        [delegate selectButtonPressed:@"Brands"];
    }else if (buttonPressed.tag==3){
        NSLog(@"Emotions selected");
        [delegate selectButtonPressed:@"Emotions"];
    }
}


-(void)selectVenues
{
    self.buttonVenues.selected = true;
    self.buttonVenues.userInteractionEnabled = false;
    [self.buttonVenues setBackgroundImage:[UIImage imageNamed:@"nearby button.png"] forState:UIControlStateNormal];
    self.buttonVenues.frame = CGRectMake(2.0f, 4.0f, 128.0f, 31.0f);
    
    self.buttonBrands.selected = false;
    self.buttonBrands.userInteractionEnabled = true;
    [self.buttonBrands setBackgroundImage:nil forState:UIControlStateNormal];
    self.buttonBrands.frame = CGRectMake(128.0f, 4.0f, 128.0f, 31.0f);

    self.buttonEmotions.selected = false;
    self.buttonEmotions.userInteractionEnabled = true;
}


-(void)selectBrands
{
    self.buttonVenues.selected = false;
    self.buttonVenues.userInteractionEnabled = true;
    [self.buttonVenues setBackgroundImage:nil forState:UIControlStateNormal];
    self.buttonVenues.frame = CGRectMake(2.0f, 4.0f, 128.0f, 31.0f);
    
    self.buttonBrands.selected = true;
    self.buttonBrands.userInteractionEnabled = false;
    [self.buttonBrands setBackgroundImage:[UIImage imageNamed:@"brands button.png"] forState:UIControlStateNormal];
     self.buttonBrands.frame = CGRectMake(128.0f, 4.0f, 128.0f, 31.0f);
    
    
    self.buttonEmotions.selected = false;
    self.buttonEmotions.userInteractionEnabled = true;
}


-(void)selectEmotions
{
    if(self.buttonBrands.selected)
    {
        self.buttonBrands.frame = CGRectMake( 122.0f, -2.0f, 140.0f, 45.0f);
       [self.buttonBrands setBackgroundImage:[UIImage imageNamed:@"brands when on emotions.png"] forState:UIControlStateNormal];
    }
    else if(self.buttonVenues.selected)
    {
        self.buttonVenues.frame = CGRectMake(0.0f, -2.0f, 140.0f, 45.0f);
        [self.buttonVenues setBackgroundImage:[UIImage imageNamed:@"nearby when on emotions.png"] forState:UIControlStateNormal];
    }
        
    self.buttonVenues.selected = false;
    self.buttonVenues.userInteractionEnabled = true;
    
    
    self.buttonBrands.selected = false;
    self.buttonBrands.userInteractionEnabled = true;
    
    self.buttonEmotions.selected = true;
    self.buttonEmotions.userInteractionEnabled = false;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
