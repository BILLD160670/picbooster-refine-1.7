//
//  BrandVenueMorePhotosViewController.h
//  Picbooster
//
//  Created by Vassilis Dourmas on 3/3/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AQGridView.h"
#import "MBProgressHUD.h"
#import "CustomNavigationBar.h"

@interface VenueBrandMorePhotosViewController : UIViewController<AQGridViewDelegate, AQGridViewDataSource>


@property (strong,nonatomic) MBProgressHUD *hud;
@property (nonatomic, strong) AQGridView * gridView;
@property (strong,nonatomic) NSURLConnection* gridConnection;
@property int GridCount;
@property (nonatomic, strong) NSArray* services;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) CustomNavigationBar* customNavBar;
@property (strong,nonatomic) NSString *venueID;

@end
