//
//  StoreViewController.h
//  Picbooster
//
//  Created by George Bafaloukas on 4/19/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MBProgressHUD.h"
#import "SBJson.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MessageUI/MessageUI.h>
#import "PopUpViewController.h"
#import "AFPhotoEditorController.h"
#import "AFPhotoEditorCustomization.h"
#import "CustomNavigationBar.h"

@interface VenueBrandViewController : UIViewController <MKMapViewDelegate,UIScrollViewDelegate,UIImagePickerControllerDelegate,MFMailComposeViewControllerDelegate,UIPopoverControllerDelegate,MJSecondPopupDelegate,AFPhotoEditorControllerDelegate>{
    MKMapView *mapView;
    NSString *storeId;
    NSMutableData *responseData;
    MBProgressHUD *hud;
    NSDictionary *storeDetails;
    NSArray *storeImages;
    UIScrollView  *imagesScrollView;
    
    UIImageView *divider;
    BOOL newPromo;
    NSString *promoMessage;
    CLLocation *userAnnotationCoord;
    BOOL pageControlUsed;
    
    UIImageView* imageViewPhotoCheckins;
    UILabel* labelAddress;
    
}

@property (strong,nonatomic) NSURLConnection* venueConnection;
@property (strong,nonatomic) NSURLConnection* photoCheckinsConnection;

@property (nonatomic, strong) CustomNavigationBar* customNavBar;
@property (strong,nonatomic) UIPopoverController *popoverController;

@property (strong, nonatomic) UIScrollView *imagesScrollView;
@property (strong, nonatomic) UIPageControl *imagesPager;
@property (strong, nonatomic) UIButton* showAllButton;

@property (strong, nonatomic) NSDictionary *storeDetails;
@property (strong, nonatomic) NSString *storeName;
@property (strong, nonatomic) NSArray *storeImages;
@property (strong, nonatomic) NSString *storeId;
@property (strong, nonatomic) MKMapView *mapView;
@property (nonatomic, strong) NSMutableData *responseData;
@property (strong,nonatomic) MBProgressHUD *hud;
@property (nonatomic,assign) BOOL newPromo;
@property (nonatomic,assign) BOOL isBrand;
@property (nonatomic,assign) BOOL pageControlUsed;
@property (strong,nonatomic) NSString *promoMessage;
@property  CLLocation *userAnnotationCoord;
@property (strong,nonatomic) UILabel* labelPhotoCheckins;
@property (strong,nonatomic) UIImageView* imageViewPhotoCheckins;
@property (strong,nonatomic) UILabel* labelAddress;


@end
