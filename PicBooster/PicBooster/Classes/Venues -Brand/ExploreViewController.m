//
//  ExploreViewController.m
//  Picbooster
//
//  Created by George Bafaloukas on 4/20/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "ExploreViewController.h"
#import "VenueBrandViewController.h"
#import "AppDelegate.h"
#import "Consts.h"
#import "VenuesBrandsSelectionMenuView.h"
#import "ChooseCameraSourceController.h"


@interface ExploreViewController ()

@end

@implementation ExploreViewController
@synthesize mapView;
@synthesize hud;
@synthesize responseData;
@synthesize services;
@synthesize myTableView;
@synthesize _locationManager;
@synthesize usersLocation;
@synthesize searchBar;
@synthesize selectionBar;
@synthesize strongSearchDisplayController;
@synthesize filteredPersons,currentSearchString,filteredServices;
@synthesize allVenues;


@synthesize categories;
@synthesize bannerImage;

//@synthesize imageCache;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated{
     CGRect r = self.view.frame;
    [self._locationManager startUpdatingLocation];
   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"menuTileNew.png"]];
    self.navigationController.navigationBar.hidden  = true;
    
    NSString *deviceType = [UIDevice currentDevice].model;
    self.selectionBar = [[VenuesBrandsSelectionMenuView alloc]initWithFrame:CGRectMake(0.0f, 65.0f, 320.0f, 40.0f)];
    self.selectionBar.backgroundColor  =[UIColor greenColor];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.selectionBar.frame = CGRectMake(262.0f, 65.0f, 320.0f, 40.0f);
    }
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        self.selectionBar.frame = CGRectMake(0.0f, 45.0f, 320.0f, 40.0f);
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            //iPad
            self.selectionBar.frame = CGRectMake(262.0f, 45.0f, 320.0f, 40.0f);
            
        }
    }
    

    self.selectionBar.delegate = self;
    [self.view addSubview:self.selectionBar];
    
    if([AppDelegate getAppDelegate].selectedFunction==0)
        [self onSelectVenues];
    else if([AppDelegate getAppDelegate].selectedFunction==1)
    {
        [self onSelectBrandCategories];
    }
    else if([AppDelegate getAppDelegate].selectedFunction==2)
    {
         [self onSelectVenues];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [AppDelegate getAppDelegate].selectedFunction = 0;
    [AppDelegate getAppDelegate].selectedLevel= 0;
}

#pragma mark - VenuesBrandsSelectionDelegate
-(void)selectButtonPressed:(NSString *)button{
    if([button isEqualToString:@"Venues"])
    {
        [self onSelectVenues];
    }
    else if([button isEqualToString:@"Brands"]){
         [self onSelectBrandCategories];
       
    }
    else if([button isEqualToString:@"Emotions"]){
        [self onSelectEmotionCategories];
    }
}


-(void)createCustomNavigationBar:(NSString*)title
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 45.0f) displayPoints:false displayPicboosterTitleView:false title:title displaySettingsButton:false titleImageView:nil];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 45) displayPoints:false displayPicboosterTitleView:false title:title displaySettingsButton:false titleImageView:nil];
        }
    }
    else
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 65.0f) displayPoints:false displayPicboosterTitleView:false title:title displaySettingsButton:false titleImageView:nil];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 65) displayPoints:false displayPicboosterTitleView:false title:title displaySettingsButton:false titleImageView:nil];
        }
    }
    
    [self.view addSubview:self.customNavBar];
    self.customNavBar.delegate = self;
    
}

-(void)onSelectVenues
{
    [self removeAllSubViews];
    [AppDelegate getAppDelegate].selectedFunction = 0;
    
    [self createCustomNavigationBar:@"select venue"];
    
    [self.selectionBar selectVenues];
    
    self._locationManager = [[CLLocationManager alloc]init];
    self._locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    self._locationManager.delegate = self;
    //self._locationManager.distanceFilter = 100.0f;
    self.usersLocation = [[CLLocation alloc] init];
    [self._locationManager startUpdatingLocation];
    foundLocation=FALSE;
    self.allVenues=[[NSMutableArray alloc] init];
    self.filteredServices =[[NSMutableArray alloc] init];
    self.services =[[NSMutableArray alloc] init];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    //"lat":"37.94855436069405","lng":"23.738073114263898"
    self.responseData=[[NSMutableData alloc]init];
    [self.navigationItem setLeftItemsSupplementBackButton:NO];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    int offsetY = 65.0f;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        offsetY = 45.0f;
    }
    
    offsetY  += self.selectionBar.frame.size.height;
    //mapView
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0.0f, offsetY , 320.0f, 130.0f)];
    
    self.mapView.delegate=self;
    [self.view addSubview:self.mapView];
    
    //searchBar
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0f, offsetY , 320.0f, 44.0f)];
    self.searchBar.placeholder = @"Search Venues";
    self.searchBar.delegate = self;
    self.searchBar.tintColor=[UIColor colorWithRed:84.0f/255.0f green:133.0f/255.0f blue:174.0f/255.0f alpha:1.0f];
    [self.searchBar sizeToFit];

    self.strongSearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.strongSearchDisplayController.searchResultsDataSource = self;
    self.strongSearchDisplayController.searchResultsDelegate = self;
    self.strongSearchDisplayController.delegate = self;
    [self.view addSubview:self.searchBar];
    
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.mapView.frame = CGRectMake(0, offsetY, 768, 200.0f);
        self.searchBar.frame = CGRectMake(0, offsetY, 768, 44.0f);
    }
    
    //tableView
    float mapY = mapView.frame.origin.y + mapView.frame.size.height;
    self.myTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, mapY , 320.0f, self.view.bounds.size.height-mapY) style:UITableViewStylePlain] ;
    self.myTableView.separatorStyle =UITableViewCellSeparatorStyleNone;
    self.myTableView.dataSource = self;
    self.myTableView.delegate = self;
    self.myTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.myTableView.frame = CGRectMake(0, mapY, 768, self.view.bounds.size.height-mapY);
    }
    [self.view addSubview:self.myTableView];
    
    
    UIView *footer =
    [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 50.0f)];
    self.myTableView.tableFooterView = footer;
    self.myTableView.backgroundColor = [UIColor clearColor];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundIPHONE4.png"]]];

}
             
             
             
             
-(void)onSelectBrandCategories
{
    [self removeAllSubViews];
    [AppDelegate getAppDelegate].selectedFunction = 1;
    [AppDelegate getAppDelegate].selectedLevel = 0;
    
    [self createCustomNavigationBar:@"select category"];
    
    [self.selectionBar selectBrands];
    [self.selectionBar setHidden:NO];
    
    self.allVenues=[[NSMutableArray alloc] init];
    self.filteredServices =[[NSMutableArray alloc] init];
    self.services =[[NSMutableArray alloc] init];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.responseData=[[NSMutableData alloc]init];
    
    
    NSString *deviceType = [UIDevice currentDevice].model;
    int offsetY = 65.0f;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        offsetY = 45.0f;
    }
    offsetY  += self.selectionBar.frame.size.height;
    //bannerImage
    self.bannerImage = [[UIImageView alloc]init];
    self.bannerImage.image=[UIImage imageNamed:@"bannerIphone.png"];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.bannerImage.image=[UIImage imageNamed:@"bannerIpad.png"];
        self.bannerImage.frame = CGRectMake(0.0f, offsetY, 768.0f, 269.0f);
        self.myTableView.frame = CGRectMake(4.0f, self.bannerImage.frame.origin.y + self.bannerImage.frame.size.height, 760.0f, self.view.bounds.size.height - 150.0f);
    }else{
        self.bannerImage.frame = CGRectMake(0.0f, offsetY, 320.0f, 130.0f);
    }
    [self.view addSubview:self.bannerImage];
    
    //searchBar
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0f, 65.0f + 40.0f, 320.0f, 44.0f)];
    self.searchBar.delegate=self;
    self.searchBar.backgroundColor = [UIColor clearColor];
    self.searchBar.placeholder = @"Search Categories";
    self.searchBar.tintColor=[UIColor colorWithRed:84.0f/255.0f green:133.0f/255.0f blue:174.0f/255.0f alpha:1.0f];
    [self.searchBar sizeToFit];
    
    for (UIView *subview in self.searchBar.subviews)
    {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")])
        {
            [subview removeFromSuperview];
            break;
        }
    }
    
    self.strongSearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.strongSearchDisplayController.searchResultsDataSource = self;
    self.strongSearchDisplayController.searchResultsDelegate = self;
    self.strongSearchDisplayController.delegate = self;
   // [self.view addSubview:self.searchBar];
    
    
    //tableView
    float mapY = bannerImage.frame.origin.y + bannerImage.frame.size.height;
    self.myTableView = [[UITableView alloc] initWithFrame:CGRectMake(3.5f, mapY , 313.0f, self.view.bounds.size.height-mapY) style:UITableViewStylePlain] ;
    self.myTableView.separatorStyle =UITableViewCellSeparatorStyleNone;
    self.myTableView.dataSource = self;
    self.myTableView.delegate = self;
    UIView *footer =
    [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 50.0f)];
    self.myTableView.tableFooterView = footer;
    self.myTableView.backgroundColor = [UIColor clearColor];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.myTableView.frame = CGRectMake(0, mapY, 768, self.view.bounds.size.height-mapY);
    }
    [self.view addSubview:self.myTableView];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundIPHONE4.png"]]];
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
    NSString *urlString= getBrandCategories;
    NSLog(@"URL:%@",urlString);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setTimeoutInterval:20.0];
    NSURLConnection* conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

}



-(void)onSelectBrands
{
    [self removeAllSubViews];
    [AppDelegate getAppDelegate].selectedFunction = 1;
    [AppDelegate getAppDelegate].selectedLevel = 1;
    
    [self createCustomNavigationBar:@"select brand"];
    
    [self.selectionBar selectBrands];
    [self.selectionBar setHidden:YES];
    
    self.allVenues=[[NSMutableArray alloc] init];
    self.filteredServices =[[NSMutableArray alloc] init];
    self.services =[[NSMutableArray alloc] init];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    //"lat":"37.94855436069405","lng":"23.738073114263898"
    self.responseData=[[NSMutableData alloc]init];
    //self.imageCache = [[NSCache alloc] init];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    int offsetY = 65.0f;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        offsetY = 45.0f;
    }
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0f, offsetY , 320.0f, 44.0f)];
    self.searchBar.placeholder = @"Search Brands";
    self.searchBar.delegate = self;
    self.searchBar.tintColor=[UIColor colorWithRed:84.0f/255.0f green:133.0f/255.0f blue:174.0f/255.0f alpha:1.0f];
    [self.searchBar sizeToFit];

    
    offsetY  += self.selectionBar.frame.size.height;
    //bannerImage
    self.bannerImage = [[UIImageView alloc]init];
    self.bannerImage.image=[UIImage imageNamed:@"bannerIphone.png"];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.bannerImage.image=[UIImage imageNamed:@"bannerIpad.png"];
        self.bannerImage.frame = CGRectMake(0.0f, offsetY, 768.0f, 269.0f);
        self.myTableView.frame = CGRectMake(4.0f, self.bannerImage.frame.origin.y + self.bannerImage.frame.size.height, 760.0f, self.view.bounds.size.height - 150.0f);
    }else{
        self.bannerImage.frame = CGRectMake(0.0f, offsetY, 320.0f, 130.0f);
    }
    [self.view addSubview:self.bannerImage];
    
    
    self.strongSearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.strongSearchDisplayController.searchResultsDataSource = self;
    self.strongSearchDisplayController.searchResultsDelegate = self;
    self.strongSearchDisplayController.delegate = self;
    [self.view addSubview:self.searchBar];

    
    //tableView
    float mapY = bannerImage.frame.origin.y + bannerImage.frame.size.height;
    self.myTableView = [[UITableView alloc] initWithFrame:CGRectMake(3.5f, mapY , 313.0f, self.view.bounds.size.height-mapY) style:UITableViewStylePlain] ;
    self.myTableView.separatorStyle =UITableViewCellSeparatorStyleNone;
    self.myTableView.dataSource = self;
    self.myTableView.delegate = self;
    UIView *footer =
    [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 50.0f)];
    self.myTableView.tableFooterView = footer;
    self.myTableView.backgroundColor = [UIColor clearColor];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.myTableView.frame = CGRectMake(0, mapY, 768, self.view.bounds.size.height-mapY);
    }
    [self.view addSubview:self.myTableView];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundIPHONE4.png"]]];
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
    NSString *urlString= [NSString stringWithFormat:getBrands,self.selected_cat_id];
    NSLog(@"URL:%@",urlString);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setTimeoutInterval:20.0];
    NSURLConnection* conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
  
}


-(void)onSelectEmotionCategories
{
    [self removeAllSubViews];
    [AppDelegate getAppDelegate].selectedFunction = 2;
    [AppDelegate getAppDelegate].selectedLevel = 0;
    
    [self createCustomNavigationBar:@"select category"];
    
    [self.selectionBar selectEmotions];
    
    [self.selectionBar setHidden:NO];
    
    self.allVenues=[[NSMutableArray alloc] init];
    self.filteredServices =[[NSMutableArray alloc] init];
    self.services =[[NSMutableArray alloc] init];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.responseData=[[NSMutableData alloc]init];
    
    
    NSString *deviceType = [UIDevice currentDevice].model;
    int offsetY = 65.0f;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        offsetY = 45.0f;
    }
    offsetY  += self.selectionBar.frame.size.height;
    //bannerImage
    self.bannerImage = [[UIImageView alloc]init];
    self.bannerImage.image=[UIImage imageNamed:@"bannerIphone.png"];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.bannerImage.image=[UIImage imageNamed:@"bannerIpad.png"];
        self.bannerImage.frame = CGRectMake(0.0f, offsetY, 768.0f, 269.0f);
        self.myTableView.frame = CGRectMake(4.0f, self.bannerImage.frame.origin.y + self.bannerImage.frame.size.height, 760.0f, self.view.bounds.size.height - 150.0f);
    }else{
        self.bannerImage.frame = CGRectMake(0.0f, offsetY, 320.0f, 130.0f);
    }
    [self.view addSubview:self.bannerImage];
    
    //searchBar
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0f, 65.0f + 40.0f, 320.0f, 44.0f)];
    self.searchBar.delegate=self;
    self.searchBar.backgroundColor = [UIColor clearColor];
    self.searchBar.placeholder = @"Search Categories";
    self.searchBar.tintColor=[UIColor colorWithRed:84.0f/255.0f green:133.0f/255.0f blue:174.0f/255.0f alpha:1.0f];
    [self.searchBar sizeToFit];
    
    for (UIView *subview in self.searchBar.subviews)
    {
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")])
        {
            [subview removeFromSuperview];
            break;
        }
    }
    
    self.strongSearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.strongSearchDisplayController.searchResultsDataSource = self;
    self.strongSearchDisplayController.searchResultsDelegate = self;
    self.strongSearchDisplayController.delegate = self;
    // [self.view addSubview:self.searchBar];
    
    
    //tableView
    float mapY = bannerImage.frame.origin.y + bannerImage.frame.size.height;
    self.myTableView = [[UITableView alloc] initWithFrame:CGRectMake(3.5f, mapY , 313.0f, self.view.bounds.size.height-mapY) style:UITableViewStylePlain] ;
    self.myTableView.separatorStyle =UITableViewCellSeparatorStyleNone;
    self.myTableView.dataSource = self;
    self.myTableView.delegate = self;
    UIView *footer =
    [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 50.0f)];
    self.myTableView.tableFooterView = footer;
    self.myTableView.backgroundColor = [UIColor clearColor];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.myTableView.frame = CGRectMake(0, mapY, 768, self.view.bounds.size.height-mapY);
    }
    [self.view addSubview:self.myTableView];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundIPHONE4.png"]]];
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
    NSString *urlString= getEmotionCategories;
    NSLog(@"URL:%@",urlString);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setTimeoutInterval:20.0];
    NSURLConnection* conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    
}





-(void)onSelectEmotions
{
    [self removeAllSubViews];
    [AppDelegate getAppDelegate].selectedFunction = 2;
    [AppDelegate getAppDelegate].selectedLevel = 1;
    
    [self createCustomNavigationBar:@"select brand"];
    
    [self.selectionBar selectBrands];
    [self.selectionBar setHidden:YES];
    
    self.allVenues=[[NSMutableArray alloc] init];
    self.filteredServices =[[NSMutableArray alloc] init];
    self.services =[[NSMutableArray alloc] init];
	// Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    //"lat":"37.94855436069405","lng":"23.738073114263898"
    self.responseData=[[NSMutableData alloc]init];
    //self.imageCache = [[NSCache alloc] init];
    
    NSString *deviceType = [UIDevice currentDevice].model;
    int offsetY = 65.0f;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        offsetY = 45.0f;
    }
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0f, offsetY , 320.0f, 44.0f)];
    self.searchBar.placeholder = @"Search Emotions";
    self.searchBar.delegate = self;
    self.searchBar.tintColor=[UIColor colorWithRed:84.0f/255.0f green:133.0f/255.0f blue:174.0f/255.0f alpha:1.0f];
    [self.searchBar sizeToFit];

    
    offsetY  += self.selectionBar.frame.size.height;
    //bannerImage
    self.bannerImage = [[UIImageView alloc]init];
    self.bannerImage.image=[UIImage imageNamed:@"bannerIphone.png"];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.bannerImage.image=[UIImage imageNamed:@"bannerIpad.png"];
        self.bannerImage.frame = CGRectMake(0.0f, offsetY, 768.0f, 269.0f);
        self.myTableView.frame = CGRectMake(4.0f, self.bannerImage.frame.origin.y + self.bannerImage.frame.size.height, 760.0f, self.view.bounds.size.height - 150.0f);
    }else{
        self.bannerImage.frame = CGRectMake(0.0f, offsetY, 320.0f, 130.0f);
    }
    [self.view addSubview:self.bannerImage];
    
    
    self.strongSearchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.strongSearchDisplayController.searchResultsDataSource = self;
    self.strongSearchDisplayController.searchResultsDelegate = self;
    self.strongSearchDisplayController.delegate = self;
    [self.view addSubview:self.searchBar];
    
    
    //tableView
    float mapY = bannerImage.frame.origin.y + bannerImage.frame.size.height;
    self.myTableView = [[UITableView alloc] initWithFrame:CGRectMake(3.5f, mapY , 313.0f, self.view.bounds.size.height-mapY) style:UITableViewStylePlain] ;
    self.myTableView.separatorStyle =UITableViewCellSeparatorStyleNone;
    self.myTableView.dataSource = self;
    self.myTableView.delegate = self;
    UIView *footer =
    [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 50.0f)];
    self.myTableView.tableFooterView = footer;
    self.myTableView.backgroundColor = [UIColor clearColor];
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.myTableView.frame = CGRectMake(0, mapY, 768, self.view.bounds.size.height-mapY);
    }
    [self.view addSubview:self.myTableView];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundIPHONE4.png"]]];
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
    NSString *urlString= [NSString stringWithFormat:getEmotions,100,self.selected_emotion_cat_id];
    NSLog(@"URL:%@",urlString);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setTimeoutInterval:20.0];
    NSURLConnection* conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
}

-(void)removeAllSubViews
{
    [self.myTableView removeFromSuperview];
    [self.bannerImage removeFromSuperview];
    [self.searchBar removeFromSuperview];
    [self.mapView removeFromSuperview];
    
    self.strongSearchDisplayController.searchResultsDataSource = nil;
    self.strongSearchDisplayController.searchResultsDelegate = nil;
    self.strongSearchDisplayController.delegate = nil;

}


//customNavigationBar actions
-(void)barButtonPressed:(NSString *)button{
    if([button isEqualToString:@"Back"]){
        if([AppDelegate getAppDelegate].selectedFunction==0 || [AppDelegate getAppDelegate].selectedLevel==0)
        {
           [self.navigationController popViewControllerAnimated:true];
        }
        else if([AppDelegate getAppDelegate].selectedFunction==1 && [AppDelegate getAppDelegate].selectedLevel==1)
        {
            [self onSelectBrandCategories];
        }
        else if([AppDelegate getAppDelegate].selectedFunction==2 && [AppDelegate getAppDelegate].selectedLevel==1)
        {
            [self onSelectEmotionCategories];
        }
    }
    
}




- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
        static NSString* SFAnnotationIdentifier = @"SFAnnotationIdentifier";
        MKPinAnnotationView* pinView =
        (MKPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:SFAnnotationIdentifier];
        if (!pinView)
        {
            MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                             reuseIdentifier:SFAnnotationIdentifier] ;
            annotationView.canShowCallout = YES;
            UIImage *flagImage;
            
            if ([annotation.title isEqualToString:@"User"]) {
                flagImage = [UIImage imageNamed:@"user pin.png"];
            }else{
                flagImage = [UIImage imageNamed:@"map pin.png"];
            }
            annotationView.image = flagImage;
            annotationView.opaque = NO;
            
            return annotationView;
        }
        else
        {
            pinView.annotation = annotation;
        }
        return pinView;
}


-(void)fullMap{
    MapViewController * fullMapView=[[MapViewController alloc] init];
    fullMapView.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
    fullMapView.allVenues = self.allVenues;
    fullMapView.usersLocation =self.usersLocation;
    fullMapView.services = self.services;
    [self.navigationController pushViewController:fullMapView animated:YES];
}


-(void)findPlaces:(CLLocation *)userLocation{
    self.usersLocation = userLocation;
   
    NSString *deviceType = [UIDevice currentDevice].model;
    //button fullMap
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button1 addTarget:self
                action:@selector(fullMap)
      forControlEvents:UIControlEventTouchDown];
    button1.tag=1;
    //[button1 setTitle:@"1" forState:UIControlStateNormal];
    [button1 setBackgroundImage:[UIImage imageNamed:@"view_full_screen.png"] forState:UIControlStateNormal];
    button1.frame = CGRectMake(self.mapView.frame.size.width - 22.0f, self.mapView.frame.size.height- 22.0f ,  20.0f, 20.0f);
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
           button1.frame = CGRectMake(288.0f+448.0f, 128.0f, 32.0f, 32.0f);
    }
    [self.mapView addSubview:button1];
    
	
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
    NSString *urlString= [NSString stringWithFormat:closestVenues,userLocation.coordinate.latitude,userLocation.coordinate.longitude];
    NSLog(@"URL:%@",urlString);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setTimeoutInterval:20.0];
    NSURLConnection* conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

#pragma mark - Search Delegate
- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    self.searchBar.frame = CGRectMake(0.0f, 20.0f, 320.0f, 44.0f);
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        self.searchBar.frame = CGRectMake(0.0f, 0.0f, 320.0f, 44.0f);

    }
    self.selectionBar.hidden = true;
    self.filteredPersons = nil;
    self.currentSearchString = @"";
}

- (void) searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    self.searchBar.frame = CGRectMake(0.0f, 65.0f, 320.0f, 44.0f);
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
         self.searchBar.frame = CGRectMake(0.0f, 45.0f, 320.0f, 44.0f);
    }
    self.selectionBar.hidden = false;
    self.filteredPersons = nil;
    self.currentSearchString = nil;
}




- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
//    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.mode = MBProgressHUDModeIndeterminate;
//    hud.labelText = @"Loading";
//    NSString *urlString= [NSString stringWithFormat:closestVenues,userLocation.coordinate.latitude,userLocation.coordinate.longitude];
//    NSLog(@"URL:%@",urlString);
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]] ;
//    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
//    [request setTimeoutInterval:20.0];
//    NSURLConnection* conn=[[NSURLConnection alloc] initWithRequest:request delegate:self];
//    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    if (searchString.length > 0) { // Should always be the case
        NSArray *venuesToSearch = self.allVenues;
        if (self.currentSearchString.length > 0 && [searchString rangeOfString:self.currentSearchString].location == 0) { // If the new search string starts with the last search string, reuse the already filtered array so searching is faster
            venuesToSearch = self.filteredPersons;
        }
        
      self.filteredPersons = [venuesToSearch filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains[cd] %@", searchString]];
       } else {
        self.filteredPersons = self.allVenues;
    }
    
    [self.filteredServices removeAllObjects];
    for (int i = 0; i<self.services.count; i++) {
        NSDictionary *tableCell = [self.services objectAtIndex:i];
        for (int z=0; z<self.filteredPersons.count; z++) {
            if ([[tableCell valueForKey:@"name"] isEqualToString:[self.filteredPersons objectAtIndex:z]]) {
                [self.filteredServices addObject:[self.services objectAtIndex:i]];
            }
        }
    }
    
    self.currentSearchString = searchString;
    
    return YES;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    if (!foundLocation) {
        self.usersLocation = newLocation;
        if (oldLocation!=newLocation) {
            [self._locationManager stopUpdatingLocation];
            foundLocation=TRUE;
            [self findPlaces:newLocation];
        }
        
    }
    
}

- (void)locationManager:(CLLocationManager *)manager
	   didFailWithError:(NSError *)error
{
    int k = 0;
}

//NSURLConnection methods following
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Connection Error!"
												   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
	[alert show];
    
    [self.hud hide:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSString *myResponseString=[[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    
    SBJsonParser *json = [[SBJsonParser alloc] init] ;
    self.services = [json objectWithString:myResponseString];
    if (self.services.count==0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Connection Error!"
                                                       delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
        [alert show];
        [self.hud hide:YES];
    }
        
    if([AppDelegate getAppDelegate].selectedFunction==0)
    {
        NSLog(@"Responce String for Nearby Places %@",myResponseString);

        for (int i=0; i<self.services.count; i++) {
            self.allVenues[i]= [[self.services objectAtIndex:i] valueForKey:@"name"];
            CLLocationCoordinate2D annotationCoord;
            annotationCoord.latitude = [[[self.services objectAtIndex:i] valueForKey:@"lat"] floatValue];
            annotationCoord.longitude = [[[self.services objectAtIndex:i] valueForKey:@"lng"] floatValue];
            MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
            annotationPoint.coordinate = annotationCoord;
            annotationPoint.title=[[self.services objectAtIndex:i] valueForKey:@"name"];
            [mapView addAnnotation:annotationPoint];
       }
       CLLocationCoordinate2D annotationCoord;
       annotationCoord.latitude = self.usersLocation.coordinate.latitude;
       annotationCoord.longitude = self.usersLocation.coordinate.longitude;
       MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
       annotationPoint.coordinate = annotationCoord;
       annotationPoint.title=@"User";
       [mapView addAnnotation:annotationPoint];
       MKCoordinateRegion region =
       MKCoordinateRegionMakeWithDistance (annotationCoord, 500, 500);
       [mapView setRegion:region animated:NO];
       //after succesful decoding of data refresh the table.
       [self.myTableView reloadData];
       [self.hud hide:YES];
    }
    else if ([AppDelegate getAppDelegate].selectedFunction==1)
    {
        NSLog(@"Responce String for Brand Categories %@",myResponseString);
        if([AppDelegate getAppDelegate].selectedLevel==0)
        {
            for (int i=0; i<self.services.count; i++) {
                self.allVenues[i]= [[self.services objectAtIndex:i] valueForKey:@"categoryname1"];
            }
            //[self getBannerImage];
            
            //after succesful decoding of data refresh the table.
            [self.myTableView reloadData];
            
            [self.hud hide:YES];

        }
        else if([AppDelegate getAppDelegate].selectedLevel==1)
        {
            NSLog(@"Responce String for Brands %@",myResponseString);
            for (int i=0; i<self.services.count; i++) {
                self.allVenues[i]= [[self.services objectAtIndex:i] valueForKey:@"name"];
            }
            [self getBannerImage];
            //after succesful decoding of data refresh the table.
            [self.myTableView reloadData];
            [self.hud hide:YES];
        }
    }
    else if ([AppDelegate getAppDelegate].selectedFunction==2)
    {
        NSLog(@"Responce String for Emotion Categories %@",myResponseString);
        if([AppDelegate getAppDelegate].selectedLevel==0)
        {
            for (int i=0; i<self.services.count; i++) {
                self.allVenues[i]= [[self.services objectAtIndex:i] valueForKey:@"categoryname1"];
            }
            
            
            //after succesful decoding of data refresh the table.
            [self.myTableView reloadData];
            
            [self.hud hide:YES];
            
        }
        else if([AppDelegate getAppDelegate].selectedLevel==1)
        {
            NSLog(@"Responce String for Emotions %@",myResponseString);
            for (int i=0; i<self.services.count; i++) {
                self.allVenues[i]= [[self.services objectAtIndex:i] valueForKey:@"name"];
            }
            //[self getBannerImage];
            //after succesful decoding of data refresh the table.
            [self.myTableView reloadData];
            [self.hud hide:YES];
        }
    }

}


- (void) getBannerImage {
    NSDictionary *tableCell = [self.services objectAtIndex:0];
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    /*
     "lazy load" images using GCD
     store each image in disk for offline load or next launch of the app
     store images in an NSCache object for fast loading
     */
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        NSString *deviceType = [UIDevice currentDevice].model;
        NSString *urlString;
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            urlString=[NSString stringWithFormat:HOMEWithValues,[tableCell valueForKey:@"bannerBig"]];
        }else{
            urlString=[NSString stringWithFormat:HOMEWithValues,[tableCell valueForKey:@"bannerSmall"]];
        }
        //use of caching delegate method
        UIImage *cellImage=[delegate returnCahedImageWithName:urlString];
        if (cellImage) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.bannerImage.image = cellImage;
                [self.bannerImage setNeedsLayout];
            });
        }
    });
}

//tableView delegate methods following
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.myTableView) {
        if (self.services.count>20) {
            return 20;
        }else{
            return self.services.count;
        }
    } else {
        return self.filteredServices.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        
    }
    NSDictionary *tableCell;
    if (tableView == self.myTableView) {
      tableCell = [self.services objectAtIndex:indexPath.row];
    }else{
      tableCell = [self.filteredServices objectAtIndex:indexPath.row];
    }
    

    // Configure the cell...
    if([AppDelegate getAppDelegate].selectedFunction==0)
    {
        cell.textLabel.text = [tableCell valueForKey:@"name"];
        cell.accessoryType=UITableViewCellAccessoryNone;
        cell.textLabel.backgroundColor = [UIColor clearColor];
        //cell.imageView.image = [delegate returnCahedImageWithName:urlString];
        cell.tag = indexPath.row;
    
        /*
        "lazy load" images using GCD
        store each image in disk for offline load or next launch of the app
        store images in an NSCache object for fast loading
        */
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
             NSString *urlString=[NSString stringWithFormat:HOMEWithValues,[tableCell valueForKey:@"actual_image"]];
             //use of caching delegate method
             UIImage *cellImage=[delegate returnCahedImageWithName:urlString];
             if (cellImage) {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     if (cell.tag == indexPath.row) {
                        cell.imageView.image = cellImage;
                        [cell setNeedsLayout];
                      }
                 });
             }
         });
    
//         if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
//            UIImage *cellBG = [UIImage imageNamed:@"cellBG.png"];
//            cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
//         }else{
             NSString *deviceType = [UIDevice currentDevice].model;

             if (indexPath.row % 2) {
                 UIImage *cellBG = [UIImage imageNamed:@"grey results block.png"];
                 cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];

             }else{
                 UIImage *cellBG = [UIImage imageNamed:@"white results block.png"];
                 cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
             }
        // }
         return cell;
    }
    else if([AppDelegate getAppDelegate].selectedFunction==1)
    {
        if([AppDelegate getAppDelegate].selectedLevel==0)
        {
            cell.textLabel.text = [tableCell valueForKey:@"categoryname1"];
            cell.accessoryType=UITableViewCellAccessoryNone;
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.tag = indexPath.row;
            
            /*
             "lazy load" images using GCD
             store each image in disk for offline load or next launch of the app
             store images in an NSCache object for fast loading
             */
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(queue, ^(void) {
                NSString *urlString=[NSString stringWithFormat:HOMEWithValues,[tableCell valueForKey:@"icon"]];
                //use of caching delegate method
                UIImage *cellImage=[delegate returnCahedImageWithName:urlString];
                if (cellImage) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (cell.tag == indexPath.row) {
                            cell.imageView.image = cellImage;
                            [cell setNeedsLayout];
                        }
                    });
                }
            });
//            if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
//                UIImage *cellBG = [UIImage imageNamed:@"cellBG.png"];
//                cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
//            }else{
                if (indexPath.row % 2) {
                    UIImage *cellBG = [UIImage imageNamed:@"grey results block.png"];
                    cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
                    
                }else{
                    UIImage *cellBG = [UIImage imageNamed:@"white results block.png"];
                    cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
                }
           // }
            
            return cell;
        }
        else
        {
            cell.textLabel.text = [tableCell valueForKey:@"name"];
            cell.accessoryType=UITableViewCellAccessoryNone;
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.tag = indexPath.row;
            
            /*
             "lazy load" images using GCD
             store each image in disk for offline load or next launch of the app
             store images in an NSCache object for fast loading
             */
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(queue, ^(void) {
                NSString *urlString=[NSString stringWithFormat:HOMEWithValues,[tableCell valueForKey:@"actual_image"]];
                //use of caching delegate method
                UIImage *cellImage=[delegate returnCahedImageWithName:urlString];
                if (cellImage) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (cell.tag == indexPath.row) {
                            cell.imageView.image = cellImage;
                            [cell setNeedsLayout];
                        }
                    });
                }
            });
            
//            if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
//                UIImage *cellBG = [UIImage imageNamed:@"cellBG.png"];
//                cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
//            }else{
                if (indexPath.row % 2) {
                    UIImage *cellBG = [UIImage imageNamed:@"grey results block.png"];
                    cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
                    
                }else{
                    UIImage *cellBG = [UIImage imageNamed:@"white results block.png"];
                    cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
                }
           // }
            
            return cell;
        }
    }
    else if([AppDelegate getAppDelegate].selectedFunction==2)
    {
        if([AppDelegate getAppDelegate].selectedLevel==0)
        {
            cell.textLabel.text = [tableCell valueForKey:@"categoryname1"];
            cell.accessoryType=UITableViewCellAccessoryNone;
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.tag = indexPath.row;
                
            /*
            "lazy load" images using GCD
            store each image in disk for offline load or next launch of the app
            store images in an NSCache object for fast loading
            */
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
            dispatch_async(queue, ^(void) {
            NSString *urlString=[NSString stringWithFormat:HOMEWithValues,[tableCell valueForKey:@"icon"]];
            //use of caching delegate method
            UIImage *cellImage=[delegate returnCahedImageWithName:urlString];
            if (cellImage) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (cell.tag == indexPath.row) {
                        cell.imageView.image = cellImage;
                        [cell setNeedsLayout];
                    }
                   });
                 }
             });
//            if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
//                UIImage *cellBG = [UIImage imageNamed:@"cellBG.png"];
//                cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
//            }else{
                if (indexPath.row % 2) {
                    UIImage *cellBG = [UIImage imageNamed:@"grey results block.png"];
                    cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
                    
                }else{
                    UIImage *cellBG = [UIImage imageNamed:@"white results block.png"];
                    cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
                }
         //   }
            return cell;
            }
            else
            {
                cell.textLabel.text = [tableCell valueForKey:@"name"];
                cell.accessoryType=UITableViewCellAccessoryNone;
                cell.textLabel.backgroundColor = [UIColor clearColor];
                cell.tag = indexPath.row;
                
                /*
                 "lazy load" images using GCD
                 store each image in disk for offline load or next launch of the app
                 store images in an NSCache object for fast loading
                 */
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                dispatch_async(queue, ^(void) {
                    NSString *urlString=[NSString stringWithFormat:HOMEWithValues,[tableCell valueForKey:@"actual_image"]];
                    //use of caching delegate method
                    UIImage *cellImage=[delegate returnCahedImageWithName:urlString];
                    if (cellImage) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (cell.tag == indexPath.row) {
                                cell.imageView.image = cellImage;
                                [cell setNeedsLayout];
                            }
                        });
                    }
                });
//                
//                if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
//                    UIImage *cellBG = [UIImage imageNamed:@"cellBG.png"];
//                    cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
//                }else{
                    if (indexPath.row % 2) {
                        UIImage *cellBG = [UIImage imageNamed:@"grey results block.png"];
                        cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
                        
                    }else{
                        UIImage *cellBG = [UIImage imageNamed:@"white results block.png"];
                        cell.backgroundView= [[UIImageView alloc] initWithImage:cellBG];
                    }
               // }
                
                return cell;
            }
    }
    
    return nil;
}

-(void)closeChooseCameraSourceForm
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
   
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
    NSDictionary *tableCell;
    if (tableView == self.myTableView) {
        tableCell = [self.services objectAtIndex:indexPath.row];
    }else{
        tableCell = [self.filteredServices objectAtIndex:indexPath.row];
    }
    // Configure the cell...
    NSLog(@"%@",tableCell);
    
    if([AppDelegate getAppDelegate].selectedFunction==0)
    {
        VenueBrandViewController *detail=[[VenueBrandViewController alloc] init];
        detail.isBrand =FALSE;
        detail.storeId=[tableCell valueForKey:@"id"];
        detail.storeName=[tableCell valueForKey:@"name"];
        [self.navigationController pushViewController:detail animated:YES];
    }
    else if([AppDelegate getAppDelegate].selectedFunction == 1)
    {
        if([AppDelegate getAppDelegate].selectedLevel == 0)
        {
            self.selected_cat_id = [[tableCell valueForKey:@"cat_id"] intValue];
            [self onSelectBrands];
        }
        else
        {
            VenueBrandViewController *detail=[[VenueBrandViewController alloc] init];
            NSDictionary *tableCell = [self.services objectAtIndex:indexPath.row];
            // Configure the cell...
            NSLog(@"%@",tableCell);
            detail.userAnnotationCoord = self.usersLocation;
            detail.isBrand =TRUE;
            detail.storeId=[tableCell valueForKey:@"id"];
            detail.storeName=[tableCell valueForKey:@"name"];
            [self.navigationController pushViewController:detail animated:YES];
        }
    }
    else if([AppDelegate getAppDelegate].selectedFunction == 2)
    {
        if([AppDelegate getAppDelegate].selectedLevel == 0)
        {
            self.selected_emotion_cat_id = [[tableCell valueForKey:@"cat_id"] intValue];
            [self onSelectEmotions];
        }
        else
        {
            NSDictionary *tableCell = [self.services objectAtIndex:indexPath.row];
            
            NSString *urlString=[NSString stringWithFormat:HOMEWithValues,[tableCell valueForKey:@"actual_image"]];
            
            ChooseCameraSourceController* aViewController = [[ChooseCameraSourceController alloc]init:self emotionId:[tableCell valueForKey:@"id" ] imageUrl:urlString prevController:self];
            //[self.navigationController presentPopupViewController:aViewController animationType:MJPopupViewAnimationFade];
            [self presentPopupViewControllerNoPop:aViewController animationType:MJPopupViewAnimationFade];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [[delegate imageCache] removeAllObjects];
}

@end
