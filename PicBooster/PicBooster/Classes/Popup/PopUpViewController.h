//
//  PopUpViewController.h
//  Picbooster
//
//  Created by George Bafaloukas on 6/22/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+MJPopupViewController.h"
@protocol MJSecondPopupDelegate;

@interface PopUpViewController : UIViewController

@property (nonatomic,strong) NSString *message;
@property (nonatomic,strong) UIImage *brandImage;

@property (assign, nonatomic) id <MJSecondPopupDelegate>delegate;

@end



@protocol MJSecondPopupDelegate<NSObject>
@optional
- (void)cancelButtonClicked;
@end