//
//  PopUpViewController.m
//  Picbooster
//
//  Created by George Bafaloukas on 6/22/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "PopUpViewController.h"


@interface PopUpViewController ()

@end

@implementation PopUpViewController
@synthesize brandImage,message;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 186.0f, 220.0f)];
    bgImage.image = [UIImage imageNamed:@"dialogbox.png"];
    self.view=bgImage;
    
    UIImageView *venueImageView = [[UIImageView alloc] initWithFrame:CGRectMake(20.0f, 40.0f, 50.0f, 50.0f)];
    venueImageView.image = self.brandImage;
    [self.view addSubview:venueImageView];
    if ([self.message isEqualToString:@""]) {
        self.message = @"Συγχαρητήρια! \nΞεκλείδωσες την \nπροσφορά του μαγαζίου";
    }
    UILabel *promoMessageLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 90.0f, 141.0f, 85.0f)];
    promoMessageLabel.text = self.message;
    promoMessageLabel.numberOfLines =0;
    promoMessageLabel.backgroundColor = [UIColor clearColor];
    promoMessageLabel.textColor = [UIColor colorWithRed:84.0f/255.0f green:133.0f/255.0f blue:174.0f/255.0f alpha:1.0f];
    promoMessageLabel.adjustsFontSizeToFitWidth =YES;
    [self.view addSubview:promoMessageLabel];
   
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
