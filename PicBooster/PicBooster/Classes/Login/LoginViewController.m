//
//  LoginViewController.m
//  Picbooster
//
//  Created by George Bafaloukas on 4/17/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden=YES;
    self.navigationController.navigationBar.tintColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"menuTileNew.png"]];
    self.title=@"picbooster";
    UILabel* tlabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0, 180, self.view.frame.size.height)];
    tlabel.text=self.navigationItem.title;
    tlabel.textColor=[UIColor whiteColor];
    tlabel.shadowColor=[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.8f];
    tlabel.shadowOffset =CGSizeMake(0, 	1);
    tlabel.backgroundColor =[UIColor clearColor];
    //tlabel.adjustsFontSizeToFitWidth=YES;
    [tlabel setFont:[UIFont fontWithName:@"Opificio-Bold" size:20.0f]];
    [tlabel setTextAlignment:NSTextAlignmentCenter];
    self.navigationItem.titleView=tlabel;
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 addTarget:self
                action:@selector(loginPressed)
      forControlEvents:UIControlEventTouchDown];
    button1.tag=1;
    [button1 setBackgroundColor:[UIColor clearColor]];
    //[button1 setImage:[UIImage imageNamed:@"facebook button.png"] forState:UIControlStateNormal];
    button1.frame = CGRectMake(100, 140, 122.0f, 56.0f);

    if (self.view.frame.size.height>480) {
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"loginScreenNew-568h.png"]];
        button1.frame = CGRectMake(100, 180, 122.0f, 56.0f);
        //[button1 setImage:[UIImage imageNamed:@"facebook button-568h.png"] forState:UIControlStateNormal];
    }else{
       self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"loginScreenNew.png"]];
    }
    
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
        //iPad
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"loginScreenIpadNew.png"]];
        button1.frame = CGRectMake(310, 370, 170.0f, 60.0f);
    }
    
    
    
    [self.view addSubview:button1];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(![defaults boolForKey:@"hasAcceptedTermsOfUse"]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Do you accept Picboosters's Terms Of Use?" message:@"By logging in using Facebook you are indicating that you have read and agree to the Privacy Policy and Terms of Service."
                                                       delegate:self cancelButtonTitle:@"Deny" otherButtonTitles:@"View", @"Accept", nil];
        [alert show];
        
    }

}
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
        // the user clicked one of the OK/Cancel buttons
        if (buttonIndex == 1)
        {
            NSLog(@"View");
            NSURL *url = [NSURL URLWithString:@"http://www.picbooster.net/index.php?option=com_content&view=article&id=1&Itemid=150"];
            
            if (![[UIApplication sharedApplication] openURL:url]){
                NSLog(@"%@%@",@"Failed to open url:",[url description]);
            }
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            if(![defaults boolForKey:@"hasAcceptedTermsOfUse"]){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Do you accept Rollingrams's Terms Of Use?" message:@"Users are noted that all objectionable content will be removed from Rollingram without notice."
                                                               delegate:self cancelButtonTitle:@"Deny" otherButtonTitles:@"View", @"Accept", nil];
                [alert show];
                
            }
            
        }else if(buttonIndex == 2){
            NSLog(@"Accept");
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:YES forKey:@"hasAcceptedTermsOfUse"];
            [defaults synchronize];
        }else {
            NSLog(@"Deny");
            exit(0);
        }
}
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen: {
            HomeViewController *myHome =[[HomeViewController alloc] init];
            [self.navigationController pushViewController:myHome animated:YES];
            NSLog(@"1");
        }
            break;
        case FBSessionStateClosed:{
            NSLog(@"State closed");
        }
            break;
        case FBSessionStateClosedLoginFailed:{
            NSLog(@"Login Failed: %@",error);
            if(error && error.code == 2) {
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle:@"Permissions error"
                                          message:@"Go to Settings -> Facebook -> Picbooster To fix that!"
                                          delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
                alertView.tag=1;
                [alertView show];
                return;
            }
            
        }
            break;
        default:
            break;
    }
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Login Failed"
                                  message:@"Try again later"
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }    
}

-(void)loginPressed{
    [FBSession renewSystemCredentials:^(ACAccountCredentialRenewResult result, NSError *error) {
        NSLog(@"Result: %d",result);
    }];
    [FBSession openActiveSessionWithReadPermissions:nil
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         // find error for not granted
         [self sessionStateChanged:session state:state error:error];
         
     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
