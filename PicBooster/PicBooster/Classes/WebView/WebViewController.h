//
//  WebViewController.h
//  Picbooster
//
//  Created by George Bafaloukas on 6/28/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomNavigationBar.h"

@interface WebViewController : UIViewController<CustomNavigationBarDelegate>
@property NSString *URL;

@property (atomic,strong) CustomNavigationBar* navBar;


- (id)init:(CustomNavigationBar*)navBar;

@end
