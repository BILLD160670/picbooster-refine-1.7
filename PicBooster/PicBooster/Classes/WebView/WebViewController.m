//
//  WebViewController.m
//  Picbooster
//
//  Created by George Bafaloukas on 6/28/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController
@synthesize URL;


- (id)init:(CustomNavigationBar*)navBar
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.navBar = navBar;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil navBar:(CustomNavigationBar*)navBar
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.navBar = navBar;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    int offsetY = 0;
    if(self.navBar != nil)
    {
        [self.view addSubview:self.navBar];
        offsetY+=self.navBar.frame.size.height;
    }
    // Do any additional setup after loading the view.
    UIWebView *instagramView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0f, offsetY, self.view.bounds.size.width, self.view.bounds.size.height)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:URL]] ;
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setTimeoutInterval:20.0];
    [instagramView loadRequest:request];
    [self.view addSubview:instagramView];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)barButtonPressed:(NSString *)button{
    if([button isEqualToString:@"Back"]){
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
