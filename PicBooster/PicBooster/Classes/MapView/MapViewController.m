//
//  MapViewController.m
//  Picbooster
//
//  Created by George Bafaloukas on 7/28/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "MapViewController.h"
#import "Consts.h"

@interface MapViewController ()

@end

@implementation MapViewController
@synthesize mapView,services,usersLocation,allVenues;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)createCustomNavigationBar:(NSString*)title
{
    NSString *deviceType = [UIDevice currentDevice].model;
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 45.0f) displayPoints:false displayPicboosterTitleView:false title:title displaySettingsButton:false titleImageView:nil];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 45) displayPoints:false displayPicboosterTitleView:false title:title displaySettingsButton:false titleImageView:nil];
        }
    }
    else
    {
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 768.0f, 65.0f) displayPoints:false displayPicboosterTitleView:false title:title displaySettingsButton:false titleImageView:nil];
        }
        else
        {
            self.customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 65) displayPoints:false displayPicboosterTitleView:false title:title displaySettingsButton:false titleImageView:nil];
        }
    }
    
    [self.view addSubview:self.customNavBar];
    self.customNavBar.delegate = self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createCustomNavigationBar:@"Venues Map"];
	// Do any additional setup after loading the view.
    [self.view addSubview:mapView];
    mapView.frame = CGRectMake(0, 65.0f, self.view.frame.size.width, self.view.frame.size.height-  65.0f);
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
         mapView.frame = CGRectMake(0, 45.0f, self.view.frame.size.width, self.view.frame.size.height - 45.0f);
    }
    self.mapView.delegate =self;
    for (int i=0; i<self.services.count; i++) {
        self.allVenues[i]= [[self.services objectAtIndex:i] valueForKey:@"name"];
        CLLocationCoordinate2D annotationCoord;
        annotationCoord.latitude = [[[self.services objectAtIndex:i] valueForKey:@"lat"] floatValue];
        annotationCoord.longitude = [[[self.services objectAtIndex:i] valueForKey:@"lng"] floatValue];
        MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
        annotationPoint.coordinate = annotationCoord;
        annotationPoint.title=[[self.services objectAtIndex:i] valueForKey:@"name"];
        [mapView addAnnotation:annotationPoint];
    }
    CLLocationCoordinate2D annotationCoord;
    
    annotationCoord.latitude = self.usersLocation.coordinate.latitude;
    annotationCoord.longitude = self.usersLocation.coordinate.longitude;
    
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationPoint.coordinate = annotationCoord;
    annotationPoint.title=@"User";
    
    [mapView addAnnotation:annotationPoint];
    
    MKCoordinateRegion region =
    MKCoordinateRegionMakeWithDistance (
                                        annotationCoord, 500, 500);
    [mapView setRegion:region animated:NO];
}
- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString* SFAnnotationIdentifier = @"SFAnnotationIdentifier";
    MKPinAnnotationView* pinView =
    (MKPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:SFAnnotationIdentifier];
    if (!pinView)
    {
        MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                                        reuseIdentifier:SFAnnotationIdentifier] ;
        annotationView.canShowCallout = YES;
        UIImage *flagImage;
        
        if ([annotation.title isEqualToString:@"User"]) {
            flagImage = [UIImage imageNamed:@"user pin.png"];
        }else{
            flagImage = [UIImage imageNamed:@"map pin.png"];
        }
        
        
        
        annotationView.image = flagImage;
        annotationView.opaque = NO;
        
        return annotationView;
    }
    else
    {
        pinView.annotation = annotation;
    }
    return pinView;
}


//customNavigationBar actions
-(void)barButtonPressed:(NSString *)button{
    if([button isEqualToString:@"Back"]){
        [self.navigationController popViewControllerAnimated:true];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
