//
//  MapViewController.h
//  Picbooster
//
//  Created by George Bafaloukas on 7/28/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "CustomNavigationbar.h"

@interface MapViewController : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate>{
    MKMapView *mapView;
}
@property (strong, nonatomic) MKMapView *mapView;
@property (nonatomic, strong) NSMutableArray * services;
@property (strong,nonatomic) CLLocation *usersLocation;
@property(nonatomic, strong) NSMutableArray *allVenues;
@property (nonatomic, strong) CustomNavigationBar* customNavBar;
@end
