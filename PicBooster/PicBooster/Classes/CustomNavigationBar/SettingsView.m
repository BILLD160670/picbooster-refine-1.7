//
//  SettingsViewController.m
//  Picbooster
//
//  Created by George Bafaloukas on 4/22/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import "SettingsView.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "WebViewController.h"
#import "Consts.h"
#import "UserProfile.h"


@implementation SettingsView
@synthesize settingsIsVisible;

- (id)initWithFrame:(CGRect)frame 
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _mainView = [[UIView alloc] initWithFrame:CGRectMake(0,0,
                                                 290.0f,self.frame.size.height)];
        self.mainView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.clipsToBounds = YES;
        self.mainView.backgroundColor= [UIColor blackColor];
        NSString *deviceType = [UIDevice currentDevice].model;
        
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button1 addTarget:self action:@selector(ButtonPressed:) forControlEvents:UIControlEventTouchDown];
        button1.tag=1;
        [button1 setBackgroundImage:[UIImage imageNamed:@"my profile button.png"] forState:UIControlStateNormal];
        [button1 setBackgroundImage:[UIImage imageNamed:@"my profile buttonPRESSED.png"] forState:UIControlStateSelected];
        [button1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [button2 addTarget:self action:@selector(ButtonPressed:) forControlEvents:UIControlEventTouchDown];
        button2.tag=2;
        [button2 setBackgroundImage:[UIImage imageNamed:@"contact button.png"] forState:UIControlStateNormal];
        [button2 setBackgroundImage:[UIImage imageNamed:@"contact buttonPRESSED.png"] forState:UIControlStateSelected];
        [button2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
         UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
         [button3 addTarget:self action:@selector(ButtonPressed:) forControlEvents:UIControlEventTouchDown];
         button3.tag=3;
         [button3 setBackgroundImage:[UIImage imageNamed:@"news button.png"] forState:UIControlStateNormal];
         [button3 setBackgroundImage:[UIImage imageNamed:@"news buttonPRESSED.png"] forState:UIControlStateSelected];
         [button3 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
         UIButton *button4 = [UIButton buttonWithType:UIButtonTypeCustom];
         [button4 addTarget:self action:@selector(ButtonPressed:)  forControlEvents:UIControlEventTouchDown];
          button4.tag=4;
         [button4 setBackgroundImage:[UIImage imageNamed:@"suggest venue button.png"] forState:UIControlStateNormal];
         [button4 setBackgroundImage:[UIImage imageNamed:@"suggest venue buttonPRESSED.png"] forState:UIControlStateSelected];
         [button4 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
         UIButton *button5 = [UIButton buttonWithType:UIButtonTypeCustom];
         [button5 addTarget:self action:@selector(ButtonPressed:)  forControlEvents:UIControlEventTouchDown];
         button5.tag=5;
         [button5 setBackgroundImage:[UIImage imageNamed:@"logout button.png"] forState:UIControlStateNormal];
         [button5 setBackgroundImage:[UIImage imageNamed:@"logout buttonPRESSED.png"] forState:UIControlStateSelected];
         [button5 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
         UIButton *button6 = [UIButton buttonWithType:UIButtonTypeCustom];
         [button6 addTarget:self action:@selector(ButtonPressed:)  forControlEvents:UIControlEventTouchDown];
         button6.tag=6;
         [button6 setBackgroundImage:[UIImage imageNamed:@"close button.png"] forState:UIControlStateNormal];
         [button6 setBackgroundImage:[UIImage imageNamed:@"close buttonPRESSED.png"] forState:UIControlStateSelected];
         [button6 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

         if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
             
             self.mainView.frame = CGRectMake(224, 0, 320.0f,self.frame.size.height);
             
             button1.frame = CGRectMake(0.0f, 0.0f , 320.0f, 55.0f);
             [self.mainView addSubview:button1];
             button2.frame = CGRectMake(0.0f, 55.0f, 320.0f, 55.0f);
             [self.mainView addSubview:button2];
             button3.frame = CGRectMake(0.0f, 110.0f , 320.0f, 55.0f);
             [self.mainView addSubview:button3];
             button4.frame = CGRectMake(0.0f, 165.0f, 320.0f, 55.0f);
             [self.mainView addSubview:button4];
             button5.frame = CGRectMake(0.0f, 220.0f , 320.0f, 55.0f);
             [self.mainView addSubview:button5];
             button6.frame = CGRectMake(0.0f, 275.0f , 320.0f, 55.0f);
             [self.mainView addSubview:button6];
           
         }else{
             button1.frame = CGRectMake(0.0f, 0.0f , 320.0f, 55.0f);
             [self.mainView addSubview:button1];
             button2.frame = CGRectMake(0.0f, 55.0f, 320.0f, 55.0f);
             [self.mainView addSubview:button2];
             button3.frame = CGRectMake(0.0f, 110.0f , 320.0f, 55.0f);
             [self.mainView addSubview:button3];
             button4.frame = CGRectMake(0.0f, 165.0f, 320.0f, 55.0f);
             [self.mainView addSubview:button4];
             button5.frame = CGRectMake(0.0f, 220.0f , 320.0f, 55.0f);
             [self.mainView addSubview:button5];
             button6.frame = CGRectMake(0.0f, 275.0f , 320.0f, 55.0f);
             [self.mainView addSubview:button6];
         }
        
        [self addSubview:self.mainView];
    }
    return self;
}


-(void)ButtonPressed:(id)sender
{
    UIButton* buttonPressed = sender;
    
    [self hideSettings];
    
    UIViewController* rootController =  [AppDelegate getAppDelegate].navController.topViewController;
    UINavigationController* navController =  [AppDelegate getAppDelegate].navController;
    
    if (buttonPressed.tag==1){
        UserProfile *userProfile= [[UserProfile alloc] init];
        [navController pushViewController:userProfile animated:YES];
    }
    else if (buttonPressed.tag==2){
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
            mailer.mailComposeDelegate = self;
            NSArray *reciepietnts=[[NSArray alloc] initWithObjects:@"info@picbooster.com", nil];
            [mailer setToRecipients:reciepietnts];
            [mailer setSubject:@"Contact Picbooster"];
            NSString *emailBody = @"Write your message here!";
            [mailer setMessageBody:emailBody isHTML:NO];
            
            [rootController presentViewController:mailer animated:YES completion:nil];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        

        
    }else if (buttonPressed.tag==3){
        CustomNavigationBar* customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 65) displayPoints:false displayPicboosterTitleView:true title:@"" displaySettingsButton:false titleImageView:nil];
        WebViewController *detail=[[WebViewController alloc] init:customNavBar];
        customNavBar.delegate = detail;
        
        //pass Instagram username to Detail View. A request URL is constructed to load the Instagram WebView
        detail.URL = @"http://picbooster.net/index.php?option=com_content&view=article&id=10&Itemid=170&lang=en";
        [navController pushViewController:detail animated:YES];
    }else if (buttonPressed.tag==4){
        //www.picbooster.com/mobilenews/
        CustomNavigationBar* customNavBar = [[CustomNavigationBar alloc]initWithFrame:CGRectMake(0, 0, 320, 65) displayPoints:false displayPicboosterTitleView:true title:@"" displaySettingsButton:false titleImageView:nil];
        WebViewController *detail=[[WebViewController alloc] init:customNavBar];
        customNavBar.delegate = detail;
        //pass Instagram username to Detail View. A request URL is constructed to load the Instagram WebView
        detail.URL = @"http://picbooster.net/index.php?option=com_content&view=article&id=9&Itemid=170&lang=en";
        [navController pushViewController:detail animated:YES];
    }
    else if(buttonPressed.tag==5){
        [[FBSession activeSession] closeAndClearTokenInformation];
        AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
        LoginViewController *loginViewController = [[LoginViewController alloc] init];
        UINavigationController *navController =[[UINavigationController alloc] initWithRootViewController:loginViewController];
        delegate.window.rootViewController = navController;
     
    }
    else if (buttonPressed.tag==6) {
       [self hideSettings];
    }
}



- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    UIViewController* rootController =  [AppDelegate getAppDelegate].navController.topViewController;
    [rootController dismissViewControllerAnimated:YES completion:nil];
}

-(void)hideSettings
{
    [UIView animateWithDuration:0.5
                          delay:0.1
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         self.frame = CGRectMake(self.frame.origin.x ,self.frame.origin.y , self.frame.size.width  , 0.0f);
                         self.settingsIsVisible=NO;
                         
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];

}


@end
