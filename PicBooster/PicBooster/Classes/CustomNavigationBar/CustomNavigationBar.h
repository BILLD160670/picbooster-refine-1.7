//
//  CustomNavigationBar.h
//  Picbooster
//
//  Created by Vassilis Dourmas on 2/21/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomNavigationBarDelegate

-(void)barButtonPressed:(NSString*) button;

@end

@interface CustomNavigationBar : UIView
{
    id <CustomNavigationBarDelegate> delegate;
}
@property (nonatomic,strong) id delegate;
@property (nonatomic, strong) UILabel *pointsLabel;
@property (nonatomic, assign) BOOL displayPoints;
@property (nonatomic, assign) BOOL displayPicboosterTitleView;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) UIImageView* titleImageView;
@property (nonatomic, assign) bool displaySettingsButton;
@property (nonatomic, strong) UILabel *tlabel;

@property (nonatomic, strong) NSMutableData *responseData;

- (id)initWithFrame:(CGRect)frame displayPoints:(bool)displayPoints displayPicboosterTitleView:(bool)displayPicboosterTitleView title:(NSString*)title displaySettingsButton:(bool)displaySettingsButton titleImageView:(UIImageView *)titleImageView;


-(void) refreshPoints;
@end
