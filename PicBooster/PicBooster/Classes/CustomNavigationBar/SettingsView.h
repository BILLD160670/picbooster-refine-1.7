//
//  SettingsViewController.h
//  Picbooster
//
//  Created by George Bafaloukas on 4/22/13.
//  Copyright (c) 2013 George Bafaloukas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <FacebookSDK/FacebookSDK.h>


@interface SettingsView : UIView <MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) UIView *mainView;
@property (nonatomic,assign) BOOL settingsIsVisible;


-(void)hideSettings;
@end
