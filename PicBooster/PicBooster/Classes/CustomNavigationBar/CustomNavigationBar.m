//
//  CustomNavigationBar.m
//  Picbooster
//
//  Created by Vassilis Dourmas on 2/21/14.
//  Copyright (c) 2014 George Bafaloukas. All rights reserved.
//

#import "CustomNavigationBar.h"
#import "AppDelegate.h"
#import "Consts.h"

@interface CustomNavigationBar ()
@property (nonatomic, strong) UIView *mainView;
@end

@implementation CustomNavigationBar
@synthesize mainView;
@synthesize delegate;
- (id)initWithFrame:(CGRect)frame displayPoints:(bool)displayPoints displayPicboosterTitleView:(bool)displayPicboosterTitleView title:(NSString*)title displaySettingsButton:(bool)displaySettingsButton titleImageView:(UIImageView *)atitleImageView
{
    self = [super initWithFrame:frame];
    self.responseData = [[NSMutableData alloc] init];
    self.displayPoints = displayPoints;
    self.displayPicboosterTitleView = displayPicboosterTitleView;
    self.displaySettingsButton = displaySettingsButton;
    self.title = title;
    self.titleImageView =atitleImageView;
    if (self) {
        // Initialization code
        NSString *deviceType = [UIDevice currentDevice].model;
        if([deviceType isEqualToString:@"iPad Simulator"] || [deviceType isEqualToString:@"iPad"]) {
            //iPad
            int h=self.frame.size.height;
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, self.frame.size.height)];
            mainView.backgroundColor = [UIColor clearColor];
            
            UIImageView* bgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, mainView.frame.size.width, mainView.frame.size.height)];
            [bgView setImage:[UIImage imageNamed:@"main screen top bar.png"]];
            [mainView addSubview:bgView];
            
            //points
            if(self.displayPoints)
            {
                UIImageView* pointsImageView =  [[UIImageView alloc]initWithFrame:CGRectMake(20, 31, 79, 22)];
                if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
                {
                    pointsImageView.frame =CGRectMake(20, 11, 79, 22);
                }
                [pointsImageView setImage:[UIImage imageNamed:@"main page counter.png"]];
                pointsImageView.contentMode = UIViewContentModeScaleAspectFill;
                [mainView addSubview:pointsImageView];
                self.pointsLabel =  [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 22)];
                self.pointsLabel.textColor = [UIColor colorWithRed:64.f/255.0f green:64.f/255.0f blue:64.f/255.0f alpha:1.0f];
                self.pointsLabel.backgroundColor = [UIColor clearColor];
                self.pointsLabel.textAlignment = NSTextAlignmentRight;
                [pointsImageView addSubview:self.pointsLabel];
                [self refreshPoints];
            }
            else
            {
                UIButton* backButton =  [[UIButton alloc]initWithFrame:CGRectMake(25, 26, 32, 32)];
                if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
                {
                    backButton.frame =CGRectMake(25, 6, 32, 32);
                }
                [backButton setImage:[UIImage imageNamed:@"back button.png"] forState:UIControlStateNormal];
                backButton.tag=2;
                backButton.contentMode = UIViewContentModeScaleAspectFill;
                [backButton addTarget:self action:@selector(barButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [mainView addSubview:backButton];
            }
            
            
            if(self.displayPicboosterTitleView)
            {
                 UIImageView* titleImageView = [[UIImageView alloc]initWithFrame:CGRectMake(340, 35, 85, 16)];
                if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
                {
                    titleImageView.frame =CGRectMake(340, 15, 85, 16);
                }
                [titleImageView setImage:[UIImage imageNamed:@"main page logo.png"]];
                titleImageView.contentMode = UIViewContentModeScaleAspectFill;
                [mainView addSubview:titleImageView];
            }
            else if(self.titleImageView)
            {
                self.titleImageView.frame = CGRectMake(340, 35, 85, 16);
                if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
                {
                    self.titleImageView.frame =CGRectMake(340, 15, 85, 16);
                }
                [mainView addSubview:self.titleImageView];
            }
            else
            {
                int xOffsetOfTitle =340;
                if(!self.displayPoints)
                    xOffsetOfTitle =200;
                
                if(self.title)
                {
                    self.tlabel=[[UILabel alloc] initWithFrame:CGRectMake(xOffsetOfTitle, 31, 220, 32)];
                    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
                    {
                        self.tlabel.frame =CGRectMake(xOffsetOfTitle,11, 220, 32);
                    }
                    self.tlabel.text = self.title;
                    self.tlabel.textColor=[UIColor blackColor];
                    //tlabel.shadowColor=[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.8f];
                    //tlabel.shadowOffset =CGSizeMake(0, 	1);
                    self.tlabel.backgroundColor =[UIColor clearColor];
                    //[tlabel sizeToFit];
                    //tlabel.adjustsFontSizeToFitWidth=YES;
                    [self.tlabel setFont:[UIFont fontWithName:@"Opificio" size:18.0f]];
                    [self.tlabel setTextAlignment:NSTextAlignmentCenter];
                    [mainView addSubview:self.tlabel];
                }
            }
            
            //settings button
            if(self.displaySettingsButton)
            {
                UIButton *settingsButton =  [UIButton buttonWithType:UIButtonTypeCustom];
                settingsButton.tag=1;
                [settingsButton setImage:[UIImage imageNamed:@"main page more button.png"] forState:UIControlStateNormal];
                settingsButton.backgroundColor = [UIColor colorWithRed:236/255.0 green:232/255.0 blue:223/255.0 alpha:1.0];
                [settingsButton addTarget:self action:@selector(barButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [settingsButton setFrame:CGRectMake(768-55, 26, 55, 39)];
                if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
                {
                    settingsButton.frame =CGRectMake(768-55, 6, 55, 39);
                }
                [mainView addSubview:settingsButton];
            }
            

            
        }else{
            int h=self.frame.size.height;
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.frame.size.width, self.frame.size.height)];
            mainView.backgroundColor = [UIColor clearColor];
            
            UIImageView* bgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, mainView.frame.size.width, mainView.frame.size.height)];
            [bgView setImage:[UIImage imageNamed:@"main screen top bar.png"]];
            [mainView addSubview:bgView];
            
            //points
            if(self.displayPoints)
            {
               UIImageView* pointsImageView =  [[UIImageView alloc]initWithFrame:CGRectMake(20, 31, 79, 22)];
               if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
               {
                  pointsImageView.frame =CGRectMake(20, 11, 79, 22);
               }
               [pointsImageView setImage:[UIImage imageNamed:@"main page counter.png"]];
               pointsImageView.contentMode = UIViewContentModeScaleAspectFill;
               [mainView addSubview:pointsImageView];
               self.pointsLabel =  [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 60, 22)];
               self.pointsLabel.textColor = [UIColor colorWithRed:64.f/255.0f green:64.f/255.0f blue:64.f/255.0f alpha:1.0f];
               self.pointsLabel.backgroundColor = [UIColor clearColor];
               self.pointsLabel.textAlignment = NSTextAlignmentRight;
               [pointsImageView addSubview:self.pointsLabel];
               [self refreshPoints];
            }
            else
            {
                UIButton* backButton =  [[UIButton alloc]initWithFrame:CGRectMake(25, 26, 32, 32)];
                if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
                {
                    backButton.frame =CGRectMake(25, 6, 32, 32);
                }
                [backButton setImage:[UIImage imageNamed:@"back button.png"] forState:UIControlStateNormal];
                backButton.tag=2;
                backButton.contentMode = UIViewContentModeScaleAspectFill;
                [backButton addTarget:self action:@selector(barButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                [mainView addSubview:backButton];
            }
            
            if(self.displayPicboosterTitleView)
            {
                UIImageView* titleImageView = [[UIImageView alloc]initWithFrame:CGRectMake(125, 35, 85, 16)];
                if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
                {
                    titleImageView.frame =CGRectMake(125, 15, 85, 16);
                }
                [titleImageView setImage:[UIImage imageNamed:@"main page logo.png"]];
                titleImageView.contentMode = UIViewContentModeScaleAspectFill;
                [mainView addSubview:titleImageView];
            }
            else if(self.titleImageView)
            {
                self.titleImageView.frame = CGRectMake(125, 35, 85, 16);
                if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
                {
                    self.titleImageView.frame =CGRectMake(125, 15, 85, 16);
                }
                [mainView addSubview:self.titleImageView];
            }
            else
            {
                int xOffsetOfTitle =125;
                if(!self.displayPoints)
                    xOffsetOfTitle =60;
           
                if(self.title)
                {
                   self.tlabel=[[UILabel alloc] initWithFrame:CGRectMake(xOffsetOfTitle, 31, 220, 32)];
                    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
                    {
                        self.tlabel.frame =CGRectMake(xOffsetOfTitle,11, 220, 32);
                    }
                   self.tlabel.text = self.title;
                   self.tlabel.textColor=[UIColor blackColor];
                   //tlabel.shadowColor=[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.8f];
                   //tlabel.shadowOffset =CGSizeMake(0, 	1);
                   self.tlabel.backgroundColor =[UIColor clearColor];
                   //[tlabel sizeToFit];
                   //tlabel.adjustsFontSizeToFitWidth=YES;
                   [self.tlabel setFont:[UIFont fontWithName:@"Opificio" size:18.0f]];
                   [self.tlabel setTextAlignment:NSTextAlignmentCenter];
                   [mainView addSubview:self.tlabel];
                }
            }
            
            //settings button
            if(self.displaySettingsButton)
            {
               UIButton *settingsButton =  [UIButton buttonWithType:UIButtonTypeCustom];
               settingsButton.tag=1;
               [settingsButton setImage:[UIImage imageNamed:@"main page more button.png"] forState:UIControlStateNormal];
               settingsButton.backgroundColor = [UIColor colorWithRed:236/255.0 green:232/255.0 blue:223/255.0 alpha:1.0];
               [settingsButton addTarget:self action:@selector(barButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
               [settingsButton setFrame:CGRectMake(265, 26, 55, 39)];
                if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
                {
                    settingsButton.frame =CGRectMake(265, 6, 55, 39);
                }
               [mainView addSubview:settingsButton];
            }
           
        }
        [self addSubview:mainView];
        
        
    }
    return self;
}
-(void)barButtonPressed:(id)sender{
    UIButton* buttonPressed = sender;
    if(buttonPressed.tag==1){
        NSLog(@"Settings button pressed");
        [delegate barButtonPressed:@"Settings"];
    }
     if(buttonPressed.tag==2){
         NSLog(@"Settings button pressed");
        [delegate barButtonPressed:@"Back"];    
     }
}

-(void) refreshPoints
{
    if(self.displayPoints)
    {
        NSString* currentUserPointsString = [AppDelegate getAppDelegate].currentUserPointsString;
        NSString* pointsString = @"";
        if ([currentUserPointsString isEqualToString:@"0"] ) {
            pointsString = @"00000";
        }
        else if ([currentUserPointsString integerValue]<100) {
            pointsString =[NSString stringWithFormat:@"000%@",currentUserPointsString];
        }
        else if ([currentUserPointsString integerValue]<1000) {
            pointsString =[NSString stringWithFormat:@"00%@",currentUserPointsString];
        }
        else if ([currentUserPointsString integerValue]<10000) {
            pointsString =[NSString stringWithFormat:@"0%@",currentUserPointsString];
        }
        [self.pointsLabel setText:pointsString];
        
        //self.pointsLabel.text = pointsString;
        
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:pointsString attributes: @{
                                    NSFontAttributeName : [UIFont systemFontOfSize:16.0f],
                                    NSForegroundColorAttributeName : [UIColor blackColor],
                                    NSKernAttributeName : @(2.2f)
           }];
        
        self.pointsLabel.text=@"";
        self.pointsLabel.attributedText =attributedString;
    }
}

//NSURLConnection methods following
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[self.responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
  	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Connection Error!"
												   delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil,nil];
	[alert show];
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
